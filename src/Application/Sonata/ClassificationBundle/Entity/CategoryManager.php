<?php

namespace Application\Sonata\ClassificationBundle\Entity;

use Doctrine\Common\Persistence\ManagerRegistry;
use Sonata\AdminBundle\Datagrid\PagerInterface;
use Sonata\ClassificationBundle\Model\CategoryInterface;
use Sonata\ClassificationBundle\Model\CategoryManagerInterface;
use Sonata\ClassificationBundle\Model\ContextInterface;
use Sonata\ClassificationBundle\Model\ContextManagerInterface;
use Sonata\DatagridBundle\Pager\Doctrine\Pager;
use Sonata\DatagridBundle\ProxyQuery\Doctrine\ProxyQuery;
use Sonata\ClassificationBundle\Entity\CategoryManager as BaseCategoryManager;

class CategoryManager extends BaseCategoryManager implements CategoryManagerInterface
{
    public function getProductCategories()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.context = :context')
			->andWhere('c.enabled = :enabled')
			->andWhere('c.parent > 0')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('context', 'product')
			->setParameter('enabled', true)
		;

		return $queryBuilder->getQuery()->getResult();
	}

    public function getProductCategoriesForNav()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.context = :context')
			->andWhere('c.enabled = :enabled')
			->andWhere('c.showOnNav = true')
			->andWhere('c.parent > 0')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('context', 'product')
			->setParameter('enabled', true)
		;

		return $queryBuilder->getQuery()->getResult();
	}
	
	public function getColorCategories()
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.context = :context')
			->andWhere('c.enabled = :enabled')
			->andWhere('c.showOnNav = :showOnNav')
			->andWhere('c.parent > 0')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('context', 'color')
			->setParameter('enabled', true)
			->setParameter('showOnNav', true)
		;

		return $queryBuilder->getQuery()->getResult();
	}

    public function getBrandCategories()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.context = :context')
			->andWhere('c.enabled = :enabled')
			->andWhere('c.parent > 0')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('context', 'product_brand')
			->setParameter('enabled', true)
		;

		return $queryBuilder->getQuery()->getResult();
    }

    public function getThemeCategories()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.context = :context')
			->andWhere('c.enabled = :enabled')
			->andWhere('c.parent > 0')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('context', 'product_theme')
			->setParameter('enabled', true)
		;

		return $queryBuilder->getQuery()->getResult();
    }

    public function findCategoryByParentSlug($parentSlug, $slug)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
			->join('c.parent', 'p')
		;

		$queryBuilder
			->where('p.slug = :parentSlug')
			->andWhere('c.slug = :categorySlug')
			->andWhere('c.enabled = :enabled')
        ;
        
		$queryBuilder
			->setParameter('parentSlug', $parentSlug)
			->setParameter('categorySlug', $slug)
			->setParameter('enabled', true)
		;

		$queryBuilder
		   ->setFirstResult(0)
		   ->setMaxResults(1)
		;

		return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
