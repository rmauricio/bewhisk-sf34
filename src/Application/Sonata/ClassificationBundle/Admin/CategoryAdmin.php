<?php

namespace Application\Sonata\ClassificationBundle\Admin;

use Sonata\ClassificationBundle\Admin\CategoryAdmin as BaseCategoryAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\ClassificationBundle\Form\Type\CategorySelectorType;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Valid;
use AppBundle\Admin\Traits\BaseAdminTrait;

class CategoryAdmin extends BaseCategoryAdmin
{
    use BaseAdminTrait;

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $context = $this->getSubject()->getContext()->getId();

        $formMapper
            ->with('group_general', ['class' => 'col-md-6'])
                ->add('name')
                ->add('description', TextareaType::class, [
                    'required' => false,
                ])
        ;

        if ($this->hasSubject()) {
            if (null !== $this->getSubject()->getParent() || null === $this->getSubject()->getId()) { // root category cannot have a parent
                $formMapper
                    ->add('parent', CategorySelectorType::class, [
                        'category' => $this->getSubject() ?: null,
                        'model_manager' => $this->getModelManager(),
                        'class' => $this->getClass(),
                        'required' => true,
                        'context' => $this->getSubject()->getContext(),
                    ])
                ;
            }
        }

        $position = $this->hasSubject() && null !== $this->getSubject()->getPosition() ? $this->getSubject()->getPosition() : 0;

        $formMapper
            ->end()
            ->with('group_options', ['class' => 'col-md-6'])
                ->add('showOnNav', CheckboxType::class, [
                    'required' => false,
                    'label' => 'Show on Navigation'
                ])
                ->add('enabled', CheckboxType::class, [
                    'required' => false,
                ])
                ->add('position', IntegerType::class, [
                    'required' => false,
                    'data' => $position,
                ])
            ->end()
        ;

        if (interface_exists(MediaInterface::class)) {
            $formMapper
                ->with('group_general')
                    ->add('media', ModelListType::class, [
                        'required' => false,
                    ], [
                        'link_parameters' => [
                            'provider' => 'sonata.media.provider.image',
                            'context' => 'sonata_category',
                        ],
                    ])
                ->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);

        $context = $this->getPersistentParameter('context');

        $datagridMapper
            ->add('showOnNav', null, ['label' => 'Show on Navigation'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $context = $this->getPersistentParameter('context');

        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('name')
            ->add('context', null, [
                'sortable' => 'context.name',
            ])
            ->add('slug')
            // ->add('description')
            ->add('parent', null, [
                'sortable' => 'parent.name',
            ])
        ;
        
        $listMapper->add('showOnNav', null, ['label' => 'Show on Navigation', 'editable' => true]);

        $listMapper
            ->add('position', null, ['editable' => true])
            ->add('enabled', null, ['editable' => true])
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
                'label' => 'Action',
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $context = $this->getPersistentParameter('context');

        $showMapper
            ->with('General', ['class' => 'col-md-12'])
                ->add('id', null, array('label' => 'ID'))
                ->add('name')
                ->add('context')
                ->add('slug')
                ->add('parent', null, array('required' => false))
                ->add('description')
        ;

        $showMapper->add('showOnNav', null, ['label' => 'Show on Navigation']);

        $showMapper
                ->add('position')
                ->add('enabled')
                ->add('updatedAt', null, array( 'label' => 'Last Update' ))
                ->add('createdAt', null, array( 'label' => 'Date Created' ))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        $context = $this->getPersistentParameter('context');

        $fields = array(
            'ID' => 'id',
            'Name' => 'name',
            'Context' => 'context',
            'Slug' => 'slug',
            'Parent' => 'parent.name',
            'Description' => 'description',
            'Position' => 'position',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );

        if ($context == 'color') {
            $fields = array(
                'ID' => 'id',
                'Name' => 'name',
                'Context' => 'context',
                'Slug' => 'slug',
                'Parent' => 'parent.name',
                'Description' => 'description',
                'Show on Navigation' => 'showOnNav',
                'Position' => 'position',
                'Enabled' => 'enabled',
                'Last Update' => 'updatedAt',
                'Date Created' => 'createdAt',
            );
        }

        return $fields;
    }
}