<?php

namespace Application\Sonata\ClassificationBundle\Admin;

use Sonata\ClassificationBundle\Admin\ContextAdmin as BaseContextAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use \AppBundle\Admin\Traits\BaseAdminTrait;

class ContextAdmin extends BaseContextAdmin
{
    use BaseAdminTrait;

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('id')
            ->add('enabled', null, [
                'editable' => true,
            ])
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
                'label' => 'Action',
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'Code' => 'id',
            'Name' => 'name',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}