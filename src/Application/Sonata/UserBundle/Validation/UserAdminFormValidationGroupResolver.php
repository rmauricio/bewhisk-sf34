<?php

namespace Application\Sonata\UserBundle\Validation;

use Symfony\Component\Form\FormInterface;

class UserAdminFormValidationGroupResolver
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public function __invoke(FormInterface $form)
    {
        $groups = ['Registration'];

        $data = $form->getData();

        if ($data->getId()) {
            $groups[] = 'Profile';
        }

        if ($data->getBillingInformation()->count()) {
            $groups[] = 'BillingInformation';
        }

        if ($data->getShippingInformation()->count()) {
            $groups[] = 'ShippingInformation';
        }

        return $groups;
    }
}