<?php

declare(strict_types=1);

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Admin;

use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Symfony\Component\Form\FormTypeInterface;
use AppBundle\Entity\User\ShippingInformation;
use AppBundle\Entity\User\BillingInformation;
use Application\Sonata\UserBundle\Entity\User;
use \AppBundle\Admin\Traits\BaseAdminTrait;

class UserAdmin extends BaseUserAdmin
{
    use BaseAdminTrait;

    protected $validationGroupResolver;

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = $this->validationGroupResolver;
        $options['constraints'] = new \Symfony\Component\Validator\Constraints\Valid();

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();

        // $instance->setBillingInformation(new BillingInformation());
        // $instance->setShippingInformation(new ShippingInformation());

        return $instance;
    }

    public function getObject($id)
    {
    	$object = parent::getObject($id);

    	/*if (!$object->getBillingInformation()) {
        	$object->setBillingInformation(new BillingInformation());
    	}
    	if (!$object->getShippingInformation()) {
	        $object->setShippingInformation(new ShippingInformation());
	    }*/

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
    	// parent::configureFormFields($formMapper);

        // define group zoning
        $formMapper
            ->tab('User')
                ->with('Profile', ['class' => 'col-md-6'])->end()
                ->with('General', ['class' => 'col-md-6'])->end()
                //->with('Social', ['class' => 'col-md-6'])->end()
                //->with('Shipping Information', ['class' => 'col-md-6'])->end()
                //->with('Billing Information', ['class' => 'col-md-6'])->end()
            ->end()
        ;

        $formMapper
            ->tab('Security')
                ->with('Status', ['class' => 'col-md-4'])->end()
                ->with('Groups', ['class' => 'col-md-4'])->end()
            ->end()
            ->tab('Billing / Shipping Information')
                ->with('Billing', ['class' => 'col-md-6'])->end()
                ->with('Shipping', ['class' => 'col-md-6'])->end()
            ->end()
        ;

        if ($this->isSuperAdmin()) {
            $formMapper
                ->tab('User')
                    ->with('Social', ['class' => 'col-md-6'])->end()
                ->end()
                ->tab('Security')
                    ->with('Keys', ['class' => 'col-md-4'])->end()
                ->end()
            ;
        }

        $formMapper
            ->tab('Security')
                ->with('Roles', ['class' => 'col-md-12'])->end()
            ->end()
        ;

        $now = new \DateTime();

        $genderOptions = [
            // 'choices' => \call_user_func([$this->getUserManager()->getClass(), 'getGenderList']),
            'choices' => array_flip(User::getGenderOptions()),
            'required' => true,
            'translation_domain' => $this->getTranslationDomain(),
        ];

        // NEXT_MAJOR: Remove this when dropping support for SF 2.8
        if (method_exists(FormTypeInterface::class, 'setDefaultOptions')) {
            $genderOptions['choices_as_values'] = true;
        }

        $formMapper
            ->tab('User')
                ->with('General')
                    ->add('username')
                    ->add('email')
                    ->add('plainPassword', TextType::class, [
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    ])
                ->end()
                ->with('Profile')
                    ->add('dateOfBirth', DatePickerType::class, [
	                    'years' => range(1900, $now->format('Y')),
	                    'format' => 'yyyy-MM-dd',
	                    'dp_min_date' => '1-1-1900',
	                    'dp_max_date' => $now->format('c'),
	                    'required' => false,
                    ])
                    ->add('firstname', null, ['required' => true, 'label' => 'First Name'])
                    ->add('middlename', null, ['required' => false, 'label' => 'Middle Name'])
                    ->add('lastname', null, ['required' => true, 'label' => 'Last Name'])
                    // ->add('website', UrlType::class, ['required' => false])
                    ->add('phone', null, ['required' => true])
                    ->add('image', ModelListType::class, array('label' => 'Image', 'required' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'user', 'provider' => 'sonata.media.provider.image')))
                    // ->add('biography', TextType::class, ['required' => false])
                    ->add('gender', ChoiceType::class, $genderOptions)
                    // ->add('locale', LocaleType::class, ['required' => false])
                    // ->add('timezone', TimezoneType::class, ['required' => false])
                ->end()
                /*->with('Social')
                    ->add('facebookUid', null, ['required' => false])
                    ->add('facebookName', null, ['required' => false])
                    ->add('twitterUid', null, ['required' => false])
                    ->add('twitterName', null, ['required' => false])
                    ->add('gplusUid', null, ['required' => false])
                    ->add('gplusName', null, ['required' => false])
                ->end()*/
            ->end()
            ->tab('Security')
                ->with('Status')
                    ->add('enabled', null, ['required' => false])
                ->end()
                ->with('Groups')
                    ->add('groups', ModelType::class, [
                        'required' => false,
                        'expanded' => true,
                        'multiple' => true,
                    ])
                ->end()
                ->with('Roles')
                    ->add('realRoles', SecurityRolesType::class, [
                        'label' => 'form.label_roles',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => false,
                    ])
                ->end()
                ->with('Keys')
                    ->add('token', null, ['required' => false])
                    ->add('twoStepVerificationCode', null, ['required' => false])
                ->end()
            ->end()
            ->tab('Billing / Shipping Information')
                ->with('Billing')
                    /*->add('billingInformation.firstName', null, ['required' => false])
                    ->add('billingInformation.lastName', null, ['required' => false])
                    ->add('billingInformation.address1', TextareaType::class, ['required' => false])
                    ->add('billingInformation.address2', TextareaType::class, ['required' => false])
                    ->add('billingInformation.city', ModelType::class, ['required' => false])
                    ->add('billingInformation.postalCode', null, ['required' => false])
                    ->add('billingInformation.contactNumber', null, ['required' => false])*/
                    ->add('billingInformation', CollectionType::class, array(
                            'label' => false,
                            'by_reference' => false,
                            'constraints' => new \Symfony\Component\Validator\Constraints\Valid(),
                            // 'btn_add' => false,
                            /*'type_options' => array(
                                // Prevents the "Delete" option from being displayed
                                'delete' => true,
                                'delete_options' => array(
                                    // You may otherwise choose to put the field but hide it
                                    // 'type' => HiddenType::class,
                                    // In that case, you need to fill in the options as well
                                    'type_options' => array(
                                        'mapped'   => false,
                                        'required' => false,
                                    )
                                ),
                            )*/
                        ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'standard',
                            // 'sortable' => 'position',
                            'limit' => 1,
                            //'edit' => 'standard',
                            // 'inline' => 'table',
                            // 'sortable' => 'position',
                            // 'link_parameters' => ['context' => $context],
                            // 'allow_delete' => true,
                            'admin_code' => 'app.user.admin.user_billing_information',
                        )
                    )
                ->end()
                ->with('Shipping')
                    /*->add('shippingInformation.firstName', null, ['required' => false])
                    ->add('shippingInformation.lastName', null, ['required' => false])
                    ->add('shippingInformation.address1', TextareaType::class, ['required' => false])
                    ->add('shippingInformation.address2', TextareaType::class, ['required' => false])
                    ->add('shippingInformation.city', ModelType::class, ['required' => false])
                    ->add('shippingInformation.postalCode', null, ['required' => false])
                    ->add('shippingInformation.contactNumber', null, ['required' => false])*/
                    ->add('shippingInformation', CollectionType::class, array(
                            'label' => false,
                            'by_reference' => false,
                            'constraints' => new \Symfony\Component\Validator\Constraints\Valid(),
                            // 'btn_add' => false,
                            /*'type_options' => array(
                                // Prevents the "Delete" option from being displayed
                                'delete' => true,
                                'delete_options' => array(
                                    // You may otherwise choose to put the field but hide it
                                    // 'type' => HiddenType::class,
                                    // In that case, you need to fill in the options as well
                                    'type_options' => array(
                                        'mapped'   => false,
                                        'required' => false,
                                    )
                                ),
                            )*/
                        ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'standard',
                            // 'sortable' => 'position',
                            'limit' => 1,
                            //'edit' => 'standard',
                            // 'inline' => 'table',
                            // 'sortable' => 'position',
                            // 'link_parameters' => ['context' => $context],
                            // 'allow_delete' => true,
                            'admin_code' => 'app.user.admin.user_shipping_information',
                        )
                    )
                ->end()
            ->end()
        ;

        if ($this->isSuperAdmin()) {
            $formMapper
                ->tab('User')
                    ->with('Social')
                        ->add('facebookUid', null, ['required' => false])
                        ->add('facebookName', null, ['required' => false])
                        ->add('twitterUid', null, ['required' => false])
                        ->add('twitterName', null, ['required' => false])
                        ->add('gplusUid', null, ['required' => false])
                        ->add('gplusName', null, ['required' => false])
                    ->end()
                ->end()
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('General')
                ->add('username')
                ->add('email')
            ->end()
            ->with('Profile')
                ->add('dateOfBirth')
                ->add('firstname', null, array('label' => 'First Name'))
                ->add('middlename', null, array('label' => 'Middle Name'))
                ->add('lastname', null, array('label' => 'Last Name'))
                //->add('website')
                //->add('biography')
                ->add('gender', 'choice', array('choices' => User::getGenderOptions()))
                //->add('locale')
                //->add('timezone')
                ->add('image', null, array('label' => 'Image'))
                ->add('phone')
            ->end()
            ->with('Groups')
                ->add('groups')
                ->add('roles', null, array('label' => 'Roles'))
            ->end()
        ;

        if ($this->isSuperAdmin()) {
            $showMapper
                ->with('Social')
                    ->add('facebookUid')
                    ->add('facebookName')
                    ->add('twitterUid')
                    ->add('twitterName')
                    ->add('gplusUid')
                    ->add('gplusName')
                ->end()
            ;
        }

        if ($this->getSubject()->getBillingInformation()->count()) {
            $showMapper
                ->with('Billing Information')
                    ->add('billingInformation', null, array('label' => 'Billing Information', 'admin_code' => 'app.user.admin.user_billing_information', 'template' => 'ApplicationSonataUserBundle:CRUD/User:show_billing_information.html.twig'))
                ->end()
            ;
        }
        
        if ($this->getSubject()->getShippingInformation()->count()) {
            $showMapper
                ->with('Shipping Information')
                    ->add('shippingInformation', null, array('label' => 'Shipping Information', 'admin_code' => 'app.user.admin.user_shipping_information', 'template' => 'ApplicationSonataUserBundle:CRUD/User:show_shipping_information.html.twig'))
                ->end()
            ;
        }

        $showMapper
            ->with('Security')
                ->add('enabled', null, array('label' => 'Enabled'))
            ->end()
        ;

        if ($this->isSuperAdmin()) {
            $showMapper
                ->with('Security')
                    ->add('token')
                    ->add('twoStepVerificationCode')
                ->end()
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('username')
            ->add('lastname', null, array('label' => 'Last Name'))
            ->add('firstname', null, array('label' => 'First Name'))
            ->add('email')
            ->add('enabled', null, ['editable' => true])
            ->add('createdAt', null, array('label' => 'Registration Date'))
        ;
        
        // if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
        /*if ($this->isSuperAdmin()) {
            $listMapper
                ->add('impersonating', 'string', ['template' => '@SonataUser/Admin/Field/impersonating.html.twig'])
            ;
        }*/

        $listMapper
            ->add('_action', null, array(
                'label' => 'Action',
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper): void
    {
        $filterMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('username')
            ->add('lastname', null, array('label' => 'Last Name'))
            ->add('middlename', null, array('label' => 'Middle Name'))
            ->add('firstname', null, array('label' => 'First Name'))
            ->add('email')
            ->add('phone', null, array('label' => 'Phone'))
            ->add('dateOfBirth', null, array('label' => 'Date of Birth'))
            ->add('gender', null, [ 'label' => 'Gender', 'field_type' => ChoiceType::class ], null, [ 'choices' => array_flip( User::getGenderOptions() ) ])
            ->add('enabled', null, array('label' => 'Enabled'))
            ->add('groups')
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Registration Date'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Username' => 'username',
            'First Name' => 'firstname',
            'Middle Name' => 'middlename',
            'Last Name' => 'lastname',
            'E-mail Address' => 'email',
            'Phone' => 'phone',
            'Date of Birth' => 'dateOfBirth',
            'Gender' => 'getGenderLabel',
            'Enabled' => 'enabled',
            'Billing Information (First Name)' => 'billingInfoFirstName',
            'Billing Information (Last Name)' => 'billingInfoLastName',
            'Billing Information (Address 1)' => 'billingInfoAddress1',
            'Billing Information (Address 2)' => 'billingInfoAddress2',
            'Billing Information (Postal Code)' => 'billingInfoPostalCode',
            'Billing Information (City)' => 'billingInfoCity',
            'Billing Information (Region)' => 'billingInfoRegion',
            'Billing Information (Contact Number)' => 'billingInfoContactNumber',
            'Shipping Information (First Name)' => 'shippingInfoFirstName',
            'Shipping Information (Last Name)' => 'shippingInfoLastName',
            'Shipping Information (Address 1)' => 'shippingInfoAddress1',
            'Shipping Information (Address 2)' => 'shippingInfoAddress2',
            'Shipping Information (Postal Code)' => 'shippingInfoPostalCode',
            'Shipping Information (City)' => 'shippingInfoCity',
            'Shipping Information (Region)' => 'shippingInfoRegion',
            'Shipping Information (Contact Number)' => 'shippingInfoContactNumber',
            'Last Update' => 'updatedAt',
            'Registration Date' => 'createdAt',
        );
    }

    public function setValidationGroupResolver($validationGroupResolver)
    {
        $this->validationGroupResolver = $validationGroupResolver;
    }
}
