<?php

namespace Application\Sonata\ArticleBundle\Entity;

use Sonata\ArticleBundle\Entity\AbstractArticle;
use Sonata\ArticleBundle\Model\ArticleInterface;
use Sonata\ArticleBundle\Model\FragmentInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Content\ArticleProduct;

use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\Traits\SortableTrait;

class Article extends AbstractArticle implements SortableInterface
{
    use SortableTrait;
    
    const TYPE_DEFAULT = 'default';
    const TYPE_FAQ = 'faq';
    const TYPE_LOOKBOOK = 'lookbook';
    const TYPE_SPOTTED = 'spotted';
    const TYPE_PRESS_COVERAGE = 'press_coverage';

    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string $type
     */
    protected $type = self::TYPE_DEFAULT;

    /**
     * @var string $body
     */
    protected $body;

    /**
     * @var \Application\Sonata\MediaBundle\Media $image
     */
    protected $image;

    /**
     * @var \Application\Sonata\MediaBundle\Media $thumbnail
     */
    protected $thumbnail;

    /**
     * @var \Application\Sonata\MediaBundle\Gallery $gallery
     */
    protected $gallery;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection $relatedProducts
     */
    protected $relatedProducts;

    public function __construct()
    {
        $this->publicationStartsAt = new \DateTime();
        $this->relatedProducts = new ArrayCollection();
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Article
     */
    public function setType(?string $type = self::TYPE_DEFAULT): Article
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string $type
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set body.
     *
     * @return void
     */
    public function setBody(?string $body): void
    {
        $this->body = $body;
    }

    /**
     * Get body.
     *
     * @return string $body
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * Updates dates before creating/updating entity.
     */
    public function prePersist(): void
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Updates dates before updating entity.
     */
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTime();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        // $metadata->addConstraint(new Assert\Callback([__NAMESPACE__.'\\AbstractArticle', 'validatorPublicationEnds']));
    }

    /**
     * Validation publication ends given by publication starts.
     */
    /*public static function validatorPublicationEnds(ArticleInterface $article, ExecutionContextInterface $context): void
    {
        if (\is_object($article->getPublicationStartsAt())
            && \is_object($article->getPublicationEndsAt())
            && $article->getPublicationStartsAt()->format('U') >= $article->getPublicationEndsAt()->format('U')
        ) {
            $context->buildViolation('article.publication_start_date_before_end_date')->addViolation();
        }
    }*/

    public static function getTypeOptions()
    {
        return array(
            self::TYPE_DEFAULT => 'Default',
            self::TYPE_FAQ => 'FAQ',
            self::TYPE_LOOKBOOK => 'Lookbook',
            self::TYPE_PRESS_COVERAGE => 'Press Coverage',
            self::TYPE_SPOTTED => 'Spotted',
        );
    }

    public function getTypeLabel()
    {
        if ( empty( $this->type ) ) {
            return null;
        }

        $options = self::getTypeOptions();
        if ( !array_key_exists( $this->type, $options ) ) {
            return null;
        }

        return $options[ $this->type ];
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Media $image
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Media $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set thumbnail.
     *
     * @param \Application\Sonata\MediaBundle\Media $thumbnail
     *
     * @return Article
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \Application\Sonata\MediaBundle\Media $thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set gallery.
     *
     * @param \Application\Sonata\MediaBundle\Gallery $gallery
     *
     * @return Article
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery.
     *
     * @return \Application\Sonata\MediaBundle\Gallery $gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Set relatedProducts.
     *
     * @param array $relatedProducts
     */
    public function setRelatedProducts($relatedProducts)
    {
        foreach ($relatedProducts as $relatedProduct) {
            $this->addRelatedProduct($relatedProduct);
        }
    }

    /**
     * Get relatedProducts.
     *
     * @return ArticleProduct[] $relatedProducts
     */
    public function getRelatedProducts()
    {
        return $this->relatedProducts;
    }

    /**
     * @param ArticleProduct
     */
    public function addRelatedProduct(ArticleProduct $relatedProduct)
    {
        $relatedProduct->setArticle($this);

        $this->relatedProducts[] = $relatedProduct;
    }

    /**
     * @param ArticleProduct
     */
    public function removeRelatedProduct(ArticleProduct $relatedProduct)
    {
        $this->relatedProducts->removeElement($relatedProduct);
    }

    /**
     * {@inheritdoc}
     */
    public static function validatorPublicationEnds(ArticleInterface $article, ExecutionContextInterface $context): void
    {
        /*if (\is_object($article->getPublicationStartsAt())
            && \is_object($article->getPublicationEndsAt())
            && $article->getPublicationStartsAt()->format('U') >= $article->getPublicationEndsAt()->format('U')
        ) {
            $context->buildViolation('article.publication_start_date_before_end_date')->addViolation();
        }*/
    }

    public function getStatusLabel()
    {
        if ( empty( $this->status ) && !is_numeric( $this->status ) ) {
            return null;
        }

        $options = self::getStatuses();
        if ( !array_key_exists( $this->status, $options ) ) {
            return null;
        }

        return $options[ $this->status ];
    }

    public function getTagsExportData()
    {
        if ( empty( $this->tags ) ) {
            return null;
        }

        $tags = array();
        foreach ( $this->tags as $tag ) {
            $tags[] = $tag->getName();
        }

        return implode( ',', $tags );
    }

    public function getCategoriesExportData()
    {
        if ( empty( $this->categories ) ) {
            return null;
        }

        $categories = array();
        foreach ( $this->categories as $category ) {
            $categories[] = $category->getName();
        }

        return implode( ',', $categories );
    }
}
