<?php

namespace Application\Sonata\ArticleBundle\Entity;

use Sonata\ArticleBundle\Entity\AbstractFragment;
use Sonata\ArticleBundle\Model\ArticleInterface;

class Fragment extends AbstractFragment
{
    /**
     * @var int $id
     */
    protected $id;

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Updates dates before creating/updating entity.
     */
    public function prePersist(): void
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Updates dates before updating entity.
     */
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTime();
    }
}
