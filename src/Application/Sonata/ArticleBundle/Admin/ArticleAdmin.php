<?php

declare(strict_types=1);

namespace Application\Sonata\ArticleBundle\Admin;

use Sonata\ArticleBundle\Admin\ArticleAdmin as BaseArticleAdmin;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\ArticleBundle\Model\AbstractArticle;
use Sonata\ArticleBundle\Model\ArticleInterface;
use Sonata\ArticleBundle\Model\FragmentInterface;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Application\Sonata\ArticleBundle\Entity\Article;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\ArticleBundle\FragmentService\FragmentServiceInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\AdminBundle\Form\Type\ModelListType;

class ArticleAdmin extends BaseArticleAdmin
{
    /**
     * @var array
     */
    protected $datagridValues = ['_sort_by' => 'updatedAt', '_sort_order' => 'DESC'];

    /**
     * @var FragmentServiceInterface[]
     */
    private $fragmentServices = [];

    protected function configureRoutes(RouteCollection $collection)
    {
        // $collection->remove('history');
        $collection->add('select_type');
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();

        if ( $type = $this->getPersistentParameter( 'type' ) ) {
            $instance->setType($type);
        }

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $parameters = array_merge(
            parent::getPersistentParameters(),
            [
                //'type' => Article::TYPE_DEFAULT,
            ]
        );

        if ($this->getSubject()) {
            $parameters['type'] = $this->getSubject()->getType() ? $this->getSubject()->getType() : Article::TYPE_DEFAULT;

            return $parameters;
        }

        if ($this->hasRequest()) {
            $request = $this->getRequest();
            $fitlter = $request->query->get( 'filter' );
            $type = $request->get( 'type' );

            if ( !empty( $fitlter[ 'type' ][ 'value' ] ) ) {
                // $parameters[ 'type' ] = $fitlter[ 'type' ][ 'value' ];
            } elseif ( !empty( $type ) && array_key_exists( $type, Article::getTypeOptions() ) ) {
                $parameters[ 'type' ] = $request->get( 'type' );
            }

            return $parameters;
        }

        return $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = [];

        if (\in_array($action, ['tree', 'show', 'edit', 'delete', 'list', 'batch'], true)
            && $this->hasAccess('create')
            && $this->hasRoute('create')
        ) {
            $list['create'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_create'),
//                'template' => $this->getTemplateRegistry()->getTemplate('button_create'),
            ];
        }

        if (\in_array($action, ['show', 'delete', 'acl', 'history'], true)
            && $this->canAccessObject('edit', $object)
            && $this->hasRoute('edit')
        ) {
            $list['edit'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_edit'),
                //'template' => $this->getTemplateRegistry()->getTemplate('button_edit'),
            ];
        }

        if (\in_array($action, ['show', 'edit', 'acl'], true)
            && $this->canAccessObject('history', $object)
            && $this->hasRoute('history')
        ) {
            $list['history'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_history'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_history'),
            ];
        }

        if (\in_array($action, ['edit', 'history'], true)
            && $this->isAclEnabled()
            && $this->canAccessObject('acl', $object)
            && $this->hasRoute('acl')
        ) {
            $list['acl'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_acl'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_acl'),
            ];
        }

        if (\in_array($action, ['edit', 'history', 'acl'], true)
            && $this->canAccessObject('show', $object)
            && \count($this->getShow()) > 0
            && $this->hasRoute('show')
        ) {
            $list['show'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_show'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_show'),
            ];
        }

        if (\in_array($action, ['show', 'edit', 'delete', 'acl', 'batch', 'select_type'], true)
            && $this->hasAccess('list')
            && $this->hasRoute('list')
        ) {
            $list['list'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_list'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_list'),
            ];
        }

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $typeOptions = Article::getTypeOptions();
        $statusOptions = Article::getStatuses();

        $datagridMapper
            ->add('id', null, [ 'label' => 'ID' ])
            ->add('title', null, [ 'label' => 'Title' ])
            ->add('type', null, [ 'label' => 'Type', 'field_type' => ChoiceType::class ], null, [ 'choices' => array_flip( $typeOptions ) ])
            ->add('status', null, [ 'label' => 'Status', 'field_type' => ChoiceType::class ], null, [ 'choices' => array_flip( $statusOptions ) ])
            ->add('publicationStartsAt', 'doctrine_orm_datetime_range', ['label' => 'Publication Start'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('publicationEndsAt', 'doctrine_orm_datetime_range', ['label' => 'Publication End'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('position', null, array('label' => 'Position'))
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Date Created'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    public function toString($object): string
    {
        $value = $object->getTitle();
        $length = $this->maxLengthTitleForDisplay;
        $separator = '...';

        $suffix = null;
        /*$type = $object->getType();
        $typeOptions = Article::getTypeOptions();
        if ( !empty( $type ) && array_key_exists( $type, $typeOptions ) ) {
            $suffix = ' ( ' . $typeOptions[ $type ] . ' )';
        }*/

        if (mb_strlen($value) > $length) {
            // If breakpoint is on the last word, return the value without separator.
            if (false === ($breakpoint = mb_strpos($value, ' ', $length))) {
                return $value;
            }

            $length = $breakpoint;

            return rtrim(mb_substr($value, 0, $length)) . $separator . $suffix;
        }

        return $value . $suffix;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name): ?string
    {
        switch($name) {
            case 'edit':
                return '@SonataArticle/CRUD/Article/edit.html.twig';
            case 'show':
                return '@SonataArticle/CRUD/Article/show.html.twig';
            default:
                return parent::getTemplate($name);
        }

        return parent::getTemplate($name);
    }

    public function validate(ErrorElement $errorElement, $object): void
    {
        /*$errorElement
            ->with('title')
                ->assertNotNull()
            ->end()
            ->with('status')
                ->assertChoice(array_keys($this->isGranted('ROLE_ARTICLE_PUBLISH') ?
                    AbstractArticle::getStatuses() : AbstractArticle::getContributorStatus()))
            ->end()
        ;

        $fragmentAdmin = $this->getChild('sonata.article.admin.fragment');
        foreach ($object->getFragments() as $fragment) {
            $fragmentAdmin->validate($errorElement, $fragment);
        }*/
    }

    /**
     * @param ArticleInterface $object
     */
    public function prePersist($object): void
    {
        $this->sort($object);

        $fragmentAdmin = $this->getChild('sonata.article.admin.fragment');
        foreach ($object->getFragments() as $fragment) {
            $fragmentAdmin->prePersist($fragment);
        }
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $fragmentClass = $this->fragmentClass;
        $subject = $this->getSubject();

        $formMapper
            ->with('General', ['class' => 'col-md-8'])
                ->add('title', TextType::class, [
                    'attr' => ['maxlength' => 255],
                    'constraints' => [new NotBlank()],
                    'empty_data' => '',
                ])
                ->add('subtitle', TextType::class, [
                    'required' => false,
                    'attr' => ['maxlength' => 255],
                ])
                ->add('abstract', TextareaType::class, [
                    'required' => false,
                ])
                ->add('body', SimpleFormatterType::class, [
                    'required' => false,
                    'label' => 'Body',
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default', // optional
                    'format_options' => [
                        'attr' => [
                            'class' => 'span10 col-sm-10 col-md-10',
                            'rows' => 50,
                        ],
                    ],
                ])
            	->add('position', IntegerType::class)
            ->end()

            ->with('Publication', ['class' => 'col-md-4'])
                ->add('status', ChoiceType::class, [
                    'choices' => array_flip($this->isGranted('ROLE_ARTICLE_PUBLISH') ?
                        AbstractArticle::getStatuses() : AbstractArticle::getContributorStatus()),
                    'attr' => ['class' => 'full-width'],
                ])
                ->add('publicationStartsAt', DateTimePickerType::class, [
                    'label' => 'Publication Start Date',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'datepicker_use_button' => false,
                    'dp_side_by_side' => true,
                    //'dp_language' => 'fr',
                    'required' => false,
                ])
                ->add('publicationEndsAt', DateTimePickerType::class, [
                    'label' => 'Publication End Date',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'datepicker_use_button' => false,
                    'dp_side_by_side' => true,
                    //'dp_language' => 'fr',
                    'required' => false,
                ])
            ->end()

            ->with('Tags', ['class' => 'col-md-4'])
                ->add('tags', ModelType::class, [
                    'required' => false,
                    'property' => 'name',
                    'multiple' => true,
                    'label' => false,
                    // 'attr' => ['class' => 'show'],
                    /*'callback' => static function (AdminInterface $admin, $property, $searchText): void {
                        $datagrid = $admin->getDatagrid();
                        $datagrid->setValue($property, null, $searchText);
                        $datagrid->setValue('enabled', null, true);
                    },*/
                ])
            ->end()

            ->with('Categories', ['class' => 'col-md-4'])
                ->add('categories', ModelType::class, [
                    'required' => false,
                    'property' => 'name',
                    'multiple' => true,
                    'label' => false,
                    // 'attr' => ['class' => 'show'],
                    /*'callback' => static function (AdminInterface $admin, $property, $searchText): void {
                        $datagrid = $admin->getDatagrid();
                        $datagrid->setValue($property, null, $searchText);
                        $datagrid->setValue('enabled', null, true);
                    },*/
                ])
            ->end()

            ->with('Media', ['class' => 'col-md-4'])
                ->add('image', ModelListType::class, array('label' => 'Image', 'required' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'article', 'provider' => 'sonata.media.provider.image')))
                ->add('thumbnail', ModelListType::class, array('label' => 'Thumbnail', 'required' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'article', 'provider' => 'sonata.media.provider.image')))
                ->add('gallery', ModelListType::class, array('label' => 'Gallery', 'required' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'article')))
            ->end()

            /*->with('Fragments', ['class' => 'col-md-12'])
                ->add('fragments', CollectionType::class, [
                    'constraints' => [new Valid()],
                    'by_reference' => false,
                    'label' => false,
                    // callback of mapping fragment with the one selected on the list
                    'pre_bind_data_callback' => static function ($value) use ($fragmentClass, $subject) {
                        $fragment = null;
                        // existing fragment case
                        foreach ($subject->getFragments() as $existingFragment) {
                            if (null !== $existingFragment->getId() && $existingFragment->getId() === $value['id']) {
                                $fragment = $existingFragment;

                                break;
                            }
                        }
                        // new fragment case
                        if (!$fragment) {
                            // @var FragmentInterface $fragment
                            $fragment = new $fragmentClass();
                            $fragment->setType($value['type']);
                            $fragment->setEnabled(isset($value['enabled']) ? (bool) $value['enabled'] : false);
                            $fragment->setPosition($value['position'] ?: 1);
                            $fragment->setFields((isset($value['fields']) && \is_array($value['fields'])) ? $value['fields'] : []);

                            $subject->addFragment($fragment);
                        }

                        return $fragment;
                    },
                ], [
                    'edit' => 'inline',
                    //'inline' => 'table',
                    //'sortable' => 'position',
                    'link_parameters' => [ 'type' => 'sonata.article.fragment.' . $this->getPersistentParameter( 'type' ) ]
                ])
            ->end()*/
        ;

        $type = $this->getPersistentParameter('type');
        $service = $this->getService($type);

        if ( !empty( $service ) ) {
            $optionsResolver = new OptionsResolver();
            $service->configureOptions($optionsResolver);
            $service->buildArticleForm($formMapper, $this->getSubject());
        }
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id', null, [
                'sortable' => true,
                'label' => 'ID'
            ])
            ->addIdentifier('title', 'text', [
                'sortable' => true,
            ])
            ->add('type', 'choice', [
                'label' => 'Type',
                'choices' => Article::getTypeOptions(),
            ])
            ->add('status', 'choice', [
                'sortable' => true,
                'choices' => AbstractArticle::getStatuses(),
            ])
            ->add('updatedAt', 'datetime', [
                'label' => 'Last Update',
                'sortable' => true,
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id', null, array( 'label' => 'ID' ))
            ->add('title', 'text', array( 'label' => 'Title' ))
            ->add('subtitle', 'text', array( 'label' => 'Subtitle' ))
            ->add('abstract', 'textarea', array( 'label' => 'Abstract' ))
            ->add('body', 'html', array( 'label' => 'Body' ))
            ->add('status', 'choice', array( 'label' => 'Status', 'choices' => Article::getStatuses() ))
            ->add('publicationStartsAt', null, array( 'label' => 'Publication Start Date' ))
            ->add('publicationEndsAt', null, array( 'label' => 'Publication End Date' ))
            ->add('tags', null, array( 'label' => 'Tags' ))
            ->add('categories', null, array( 'label' => 'Categories' ))
            ->add('image', null, array( 'label' => 'Image' ))
            ->add('thumbnail', null, array( 'label' => 'Thumbnail' ))
            ->add('gallery', null, array( 'label' => 'Gallery' ))
            ->add('position', null, array( 'label' => 'Position' ))
            ->add('updatedAt', null, array( 'label' => 'Last Update' ))
            ->add('createdAt', null, array( 'label' => 'Date Created' ))
        ;

        $type = $this->getPersistentParameter('type');
        $service = $this->getService($type);

        if ( !empty( $service ) && method_exists( $service, 'configureShowFields') ) {
            $optionsResolver = new OptionsResolver();
            $service->configureOptions($optionsResolver);
            $service->configureShowFields($showMapper, $this->getSubject());
        }
    }

    /**
     * Sorting the fragments depending on position.
     */
    private function sort(ArticleInterface $article): void
    {
        $fragments = $article->getFragments()->toArray();

        usort($fragments, static function (FragmentInterface $a, FragmentInterface $b) {
            return $a->getPosition() - $b->getPosition();
        });

        $article->setFragments(new ArrayCollection($fragments));
    }

    public function setFragmentServices(array $fragmentServices): void
    {
        $this->fragmentServices = $fragmentServices;
    }

    /**
     * @return FragmentServiceInterface[]
     */
    public function getFragmentServices(): array
    {
        return $this->fragmentServices;
    }

    /**
     * @return FragmentServiceInterface|null
     */
    protected function getService(string $type)
    {
        $serviceId = 'sonata.article.fragment.' . $type;
        if (!\array_key_exists($serviceId, $this->fragmentServices)) {
            // throw new \RuntimeException(sprintf('Fragment service for type `%s` is not handled by this admin', $type));
            return null;
        }

        return $this->fragmentServices[$serviceId];
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Title' => 'title',
            'Type' => 'getTypeLabel',
            'Subtitle' => 'subtitle',
            'Abstract' => 'abstract',
            'Body' => 'body',
            'Status' => 'getStatusLabel',
            'Publication Start Date' => 'publicationStartsAt',
            'Publication End Date' => 'publicationEndsAt',
            'Tags' => 'getTagsExportData',
            'Categories' => 'getCategoriesExportData',
            'Image' => 'image',
            'Thumbnail' => 'thumbnail',
            'Gallery' => 'gallery',
            'Position' => 'position',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}
