<?php

namespace Application\Sonata\ArticleBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidArticle extends Constraint
{
    public $messagePublicationStartsAtLessThan = 'This value should be less than publication end date.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}