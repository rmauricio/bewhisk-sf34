<?php

namespace Application\Sonata\ArticleBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidArticleValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
    	if ($protocol->getPublicationStartsAt() instanceOf \DateTime && $protocol->getPublicationEndsAt() instanceOf \DateTime) {
        	if ($protocol->getPublicationStartsAt()->format('U') > $protocol->getPublicationEndsAt()->format('U')) {
    			$this->context->buildViolation($constraint->messagePublicationStartsAtLessThan)
                    ->atPath('publicationStartsAt')
                    ->addViolation();
    		}
        }
    }
}