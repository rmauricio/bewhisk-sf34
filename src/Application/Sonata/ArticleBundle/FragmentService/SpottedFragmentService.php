<?php

declare(strict_types=1);

namespace Application\Sonata\ArticleBundle\FragmentService;

use Sonata\ArticleBundle\FragmentService\AbstractFragmentService;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\ArticleBundle\Model\ArticleInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sonata\Form\Type\CollectionType;

class SpottedFragmentService extends AbstractFragmentService
{
    public function buildArticleForm(FormMapper $formMapper, ArticleInterface $article): void
    {
    	$formMapper
	    	->with('Related Products', ['class' => 'col-md-8'])
	    		->add('relatedProducts', CollectionType::class, array(
                        'label' => false,
                        'by_reference' => false,
                        // 'btn_add' => true,
                        'type_options' => array(
                            // Prevents the "Delete" option from being displayed
                            //'delete' => false,
                            /*'delete_options' => array(
                                // You may otherwise choose to put the field but hide it
                                // 'type' => HiddenType::class,
                                // In that case, you need to fill in the options as well
                                'type_options' => array(
                                    'mapped'   => false,
                                    'required' => false,
                                )
                            )*/
                        )
                    ),
                    array(
                        //'edit' => 'standard',
                        //'inline' => 'standard',
                        'sortable' => 'position',
                        // 'limit' => 1,
                        'edit' => 'inline',
                        'inline' => 'table',
                        // 'sortable' => 'position',
                        // 'link_parameters' => ['context' => $context],
                        'allow_delete' => true,
                        'admin_code' => 'app.content.admin.article_product',
                    )
                )
            ->end()
        ;
    }

    public function getTemplate(): string
    {
        return '@SonataArticle/Fragment/fragment_text.html.twig';
    }
}
