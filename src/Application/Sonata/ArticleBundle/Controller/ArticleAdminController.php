<?php

namespace Application\Sonata\ArticleBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormRenderer;
use Application\Sonata\ArticleBundle\Entity\Article;

class ArticleAdminController extends CRUDController
{
    public function selectTypeAction()
    {
        $type = $this->admin->getPersistentParameter( 'type' );
        if ( !empty( $type ) ) {
            return $this->redirect( $this->admin->generateUrl( 'create', [ 'type' => $type ] ) );
        }

        $request = $this->getRequest();

        $this->admin->checkAccess('create');

        return $this->renderWithExtraParams('@SonataAdmin/CRUD/select-type.html.twig', [
            'action' => 'select_type',
            'box_title' => 'Article Type Selection',
            'type_options' => Article::getTypeOptions(),
        ], null);
    }

    protected function preCreate(Request $request, $object)
    {
        $type = $this->admin->getPersistentParameter( 'type' );
        if ( empty( $type ) ) {
            return $this->redirect( $this->admin->generateUrl( 'select_type' ) );
        }

        return null;
    }

    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $datagridValues = $datagrid->getValues();

        $datagridTypeIsSet = isset($datagridValues['type']['value']) && !empty($datagridValues['type']['value']);

        if ($this->admin->getPersistentParameter('type') && !$datagridTypeIsSet) {
            $datagrid->setValue('type', null, $this->admin->getPersistentParameter('type'));
        }

        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFilterTheme());

        // NEXT_MAJOR: Remove this line and use commented line below it instead
        $template = $this->admin->getTemplate('list');
        // $template = $this->templateRegistry->getTemplate('list');

        return $this->renderWithExtraParams($template, [
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ], null);
    }

    /**
     * Sets the admin form theme to form view. Used for compatibility between Symfony versions.
     */
    private function setFormTheme(FormView $formView, array $theme = null): void
    {
        $twig = $this->get('twig');

        // BC for Symfony < 3.2 where this runtime does not exists
        if (!method_exists(AppVariable::class, 'getToken')) {
            $twig->getExtension(FormExtension::class)->renderer->setTheme($formView, $theme);

            return;
        }

        // BC for Symfony < 3.4 where runtime should be TwigRenderer
        if (!method_exists(DebugCommand::class, 'getLoaderPaths')) {
            $twig->getRuntime(TwigRenderer::class)->setTheme($formView, $theme);

            return;
        }

        $twig->getRuntime(FormRenderer::class)->setTheme($formView, $theme);
    }
}