<?php

declare(strict_types=1);

namespace Application\Sonata\ArticleBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class FragmentsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $fragmentServices = [];
        $fragmentServiceIds = $container->findTaggedServiceIds('sonata.article.fragment');
        $requiredFragmentsServices = $container->getParameter('sonata.article.admin.fragments.services');

        foreach ($fragmentServiceIds as $id => $attributes) {
            if (!isset($attributes[0]) || !isset($attributes[0]['key'])) {
                throw new \RuntimeException('You need to specify the `key` argument to your tag.');
            }

            if (\in_array($id, $requiredFragmentsServices['simple_array_provider'], true)) {
                $fragmentServices[$attributes[0]['key']] = new Reference($id);
            }
        }

        if ($container->hasDefinition('sonata.article.admin.article')) {
            $fragmentAdminDef = $container->getDefinition('sonata.article.admin.article');
            $fragmentAdminDef->addMethodCall('setFragmentServices', [$fragmentServices]);
        }
    }
}
