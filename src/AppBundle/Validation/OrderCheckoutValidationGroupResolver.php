<?php 

namespace AppBundle\Validation;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderCheckoutValidationGroupResolver
{
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    /**
     * @param FormInterface $form
     * @return array
     */
    public function __invoke(FormInterface $form)
    {
        $groups = ['AppStoreCheckout', 'AppStoreCheckoutShipping'];

        if ($form->getData()->getShippingSameAsBilling()) {
            $groups = ['AppStoreCheckout'];
        }
        $request = $this->requestStack->getCurrentRequest();
        if ($request->get('_route') == 'app_store_order_checkout_refresh_form' && $validationGroup = $request->request->get('validationGroup')) {
            $validationGroup = explode('__', $validationGroup);
            $groups = $validationGroup;
        }

        return $groups;
    }
}