<?php

// Change the namespace according to the location of this class in your bundle
namespace AppBundle\EventListener;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use AppBundle\Entity\Store\Order;

class LoginListener 
{    
    protected $userManager;

    protected $orderManager;

    protected $session;
    
    public function __construct(UserManagerInterface $userManager, $orderManager, $session) 
    {
        $this->userManager = $userManager;
        $this->orderManager = $orderManager;
        $this->session = $session;
    }
    
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if ($event->getAuthenticationToken()->getProviderKey() != 'main') {
            return;
        }

        $user = $event->getAuthenticationToken()->getUser();
        $order = $this->orderManager->getUserActiveOrder();
        
        if ($order) {
            if ($order->getStatus() == Order::STATUS_EXPIRED) {
                // Force regenerate cart key if user is not the owner of the order
                $this->session->getFlashBag()->add('GenCartUniqueKey', md5(strtotime("now")));
            } elseif ($order->getUser() && $order->getUser()->getId() != $user->getId()) {
                // @todo: Log access denied on someone's order
                // Force regenerate cart key if user is not the owner of the order
                $this->session->getFlashBag()->add('GenCartUniqueKey', md5(strtotime("now")));
            } elseif ($order && !$order->getUser()) {
                // Attach user to active order
                $order->setUser($user);
                $this->orderManager->save($order);
            }
        } 
    }
}