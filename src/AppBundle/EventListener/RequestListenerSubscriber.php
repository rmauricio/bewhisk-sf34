<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RequestListenerSubscriber implements EventSubscriberInterface
{
    protected $session;

    protected $authorizationChecker;

    protected $authenticationUtils;

    protected $resourceOwner;

    protected $storeOrderService;

    public function __construct(SessionInterface $session, $authorizationChecker, $authenticationUtils, $resourceOwner, $storeOrderService)
    {
        $this->session = $session;
        $this->authorizationChecker = $authorizationChecker;
        $this->authenticationUtils = $authenticationUtils;
        $this->resourceOwner = $resourceOwner;
        $this->storeOrderService = $storeOrderService;
    }

    public static function getSubscribedEvents()
    {
        return array(
			KernelEvents::REQUEST => array(
				array('onOAuthLogin'),
			),
			KernelEvents::RESPONSE => array(
				array('onAppResponse'),
			),
        );
    }
    
    public function onOAuthLogin(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $securityRoutes = array(
            'fos_user_security_login',
            'fos_user_registration_register'
        );

        $request =  $event->getRequest();

        // $cookie = new Cookie('foo', 'bar', strtotime('Wed, 28-Dec-2016 15:00:00 +0100'), '/', '.example.com', true, true, true),

        if ($request->get('_route') == 'facebook_login' && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            // $error = $this->sessionAuthUtils->getLastAuthenticationError(false);
            // $userInformation = $this->resourceOwner->getResourceOwnerByName('facebook')->getUserInformation($error->getRawToken());
            $this->session->getFlashBag()->add(
                'success',
                'You have successfully registered via facebook sign up.'
                // sprintf('Successfully connected your facebook account %s', $resourceOwner->getResourceOwnerByName('facebook'))
            );
        } elseif (in_array($request->get('_route'), $securityRoutes) && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $event->setResponse(new RedirectResponse('/'));
        }
    }

    public function onAppResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request =  $event->getRequest();

        // Do not proceed for admin routes
        if (stripos($request->getPathInfo(), '/admin') === 0) {
            return;
        }

        $genCartUniqueKey = $this->session->getFlashBag()->get('GenCartUniqueKey');
        if (!$this->storeOrderService->getCartUniqueID($request) || $genCartUniqueKey) {
            $force = false;
            if ($genCartUniqueKey) {
                $force = true;
            }
            $this->storeOrderService->generateCartUniqueID($response, $force);
        }
    }
}