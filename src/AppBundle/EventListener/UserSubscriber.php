<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Application\Sonata\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;

class UserSubscriber implements EventSubscriberInterface
{
    protected $router;

    protected $session;

    public function __construct(SessionInterface $session, RouterInterface $router)
    {
        $this->router = $router;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => array(
                array('onRegistrationSuccess', 999)
            ),
            FOSUserEvents::REGISTRATION_CONFIRM => array(
                array('onRegistrationConfirm', -1)
            ),
            FOSUserEvents::RESETTING_RESET_COMPLETED => array(
                array('onPasswordResetCompleted', -1)
            ),
            FOSUserEvents::PROFILE_EDIT_SUCCESS => array(
                array('onProfileEditSuccess', -1)
            ),
            FOSUserEvents::PROFILE_EDIT_COMPLETED => array(
                array('onProfileEditCompleted', -1)
            ),
        ];
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        // Set username with email address for new user accounts
        $this->_syncUsernameAndEmail($event);
    }

    public function onProfileEditSuccess(FormEvent $event)
    {
        // Set username with email address for new user accounts
        $this->_syncUsernameAndEmail($event);

        $url = $this->router->generate('fos_user_profile_edit');
        $response = new RedirectResponse($url);

        $event->setResponse($response);
    }

    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        $user = $event->getUser();

        $this->session->getFlashBag()->add('success', sprintf('Congrats %s, your account is now activated.', $user->getFirstname()));

        $url = $this->router->generate('page_slug', array('path' => '/'));
        $response = new RedirectResponse($url);

        $event->setResponse($response);
    }

    public function onProfileEditCompleted(FilterUserResponseEvent $event)
    {
        $url = $this->router->generate('fos_user_profile_edit');
        $response = new RedirectResponse($url);

        $event->setResponse($response);
    }

    public function onPasswordResetCompleted(FilterUserResponseEvent $event)
    {
        $url = $this->router->generate('fos_user_profile_edit');
        $response = new RedirectResponse($url);

        $event->setResponse($response);
    }

    private function _syncUsernameAndEmail($event)
    {
        $user = $event->getForm()->getData();
        $email = $user->getEmail();
        $user->setRoles(array('ROLE_USER'));
        $user->setUsername($email);
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }
}