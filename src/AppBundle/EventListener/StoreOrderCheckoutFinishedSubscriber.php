<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\Store\OrderCheckoutFinishedEvent;
use AppBundle\EntityManager\Store\OrderManager;
use AppBundle\Entity\Store\Order;

class StoreOrderCheckoutFinishedSubscriber implements EventSubscriberInterface
{
    protected $mailer;

    public function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderCheckoutFinishedEvent::NAME => array(
                array('onCheckoutFinished', -1)
            ),
        ];
    }

    public function onCheckoutFinished(OrderCheckoutFinishedEvent $event)
    {
        $order = $event->getOrder();
        try {
            $result = $this->mailer->sendCustomerOrderConfirmationEmail($order);
            if (!$result) {
                // @todo: Log
            }
        } catch (\Exception $e) {
            //@todo: Log
        }
    }
}