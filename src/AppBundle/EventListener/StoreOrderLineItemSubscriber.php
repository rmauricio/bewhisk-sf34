<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use AppBundle\Entity\Store\OrderLineItem;
use Keygen\Utility\Generator\KeyGenerator;
use Cocur\Slugify\Slugify;

class StoreOrderLineItemSubscriber implements EventSubscriber
{
    protected $serializer;

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            // Events::preRemove,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        // attach product data in product
        $this->attachProductData($args);
        // attach serialized data
        $this->attachSerializedData($args);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('product')) {
            // attach product data in product
            $this->attachProductData($args);
            // attach serialized data
            $this->attachSerializedData($args);
        }
    }

    public function attachProductData($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof OrderLineItem) {
            if ($entity->getProduct()) {
                $productName = $entity->getProduct()->getName();
                $productTypeId = $entity->getProduct()->getType()->getId();
                $productTypeCode = $entity->getProduct()->getType()->getCode();
                $productTypeName = $entity->getProduct()->getType()->getName();
                $productSku = $entity->getProduct()->getSku();
                $productPrice = $entity->getProduct()->getPrice();
                $productFinalPrice = $entity->getProduct()->computePrice();

                $entity->setProductName($productName);
                $entity->setProductTypeId($productTypeId);
                $entity->setProductTypeCode($productTypeCode);
                $entity->setProductTypeName($productTypeName);
                $entity->setProductSku($productSku);
                $entity->setPrice($productPrice);
                $entity->setFinalPrice($productFinalPrice);
            } else {
                $entity->setProductName(null);
                $entity->setProductTypeId(null);
                $entity->setProductTypeCode(null);
                $entity->setProductTypeName(null);
                $entity->setProductSku(null);
                $entity->setPrice(null);
                $entity->setFinalPrice(null);
            }
        }
    }

    public function attachSerializedData($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof OrderLineItem) {
            $productData = null;
            if ($entity->getProduct()) {
                $productData = $this->serializer->serialize( $entity->getProduct(), 'json' );
                $productData = json_decode( $productData, true );
            }

            $data = $entity->getData();
            $data['product'] = $productData;
            $entity->setData($data);
        }
    }

    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }
}