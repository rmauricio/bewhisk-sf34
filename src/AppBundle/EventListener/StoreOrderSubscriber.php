<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use AppBundle\Entity\Store\Order;
use Keygen\Utility\Generator\KeyGenerator;
use Cocur\Slugify\Slugify;
use FOS\UserBundle\Model\UserInterface;

class StoreOrderSubscriber implements EventSubscriber
{
    protected $serializer;

    protected $orderManager;

    protected $mailer;

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postUpdate,
            // Events::preRemove,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        // attach user data in order
        $this->attachUserData($args);
        // attach gift wrap data in order
        // $this->attachGiftWrapData($args);
        // attach promo code data in order
        $this->attachPromoCodeData($args);
        // attach serialized data
        // $this->attachOtherData($args);
        // this->attachSerializedData($args);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        // attach user data in order
        $this->attachUserData($args);
        // attach gift wrap data in order
        // $this->attachGiftWrapData($args);
        // attach promo code data in order
        $this->attachPromoCodeData($args);
        // attach serialized data
        // $this->attachOtherData($args);
        // $this->attachSerializedData($args);

        //$entity = $args->getObject();
        //dump($entity);die();
    }

    public function postUpdate($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order) {
            if ($entity->getStatusTransition() == 'paid') {
                $this->orderManager->updateStocksWithOrder($entity);
                $this->mailer->sendOrderPaidConfirmationEmail($entity);
            }
        }
    }

    public function attachUserData($args)
    {
        $entity = $args->getObject();
        
        if ($entity instanceof Order 
            && (
                    $entity->isOnInitialState() 
                    || ($args instanceof PreUpdateEventArgs && $args->hasChangedField('user'))
                    || ($args->hasChangedField('status') && array_key_exists($args->getOldValue('status'), Order::getInitialStateStatuses()))
                )
            ) {
            // if ($args->hasChangedField('user') && $eventArgs->getNewValue('name') == 'Alice') {

            $userData = null;
            if ($entity->getUser() instanceof UserInterface) {
                $username = $entity->getUser()->getUsername();
                $email = $entity->getUser()->getEmail();
                $firstName = $entity->getUser()->getFirstname();
                $lastName = $entity->getUser()->getLastname();
                $middleName = $entity->getUser()->getMiddlename();
    
                $entity->setUsername($username);
                $entity->setUserEmail($email);
                $entity->setUserFirstName($firstName);
                $entity->setUserLastName($lastName);
                $entity->setUserMiddleName($middleName);
    
                $userData = $this->serializer->serialize( $entity->getUser(), 'json' );
                $userData = json_decode( $userData, true );
            } else {
                $entity->setUsername(null);
                $entity->setUserEmail(null);
                $entity->setUserFirstName(null);
                $entity->setUserLastName(null);
                $entity->setUserMiddleName(null);
            }
            
            $data = $entity->getData();
            $data['user'] = $userData;
            $entity->setData($data);
        }
    }

    public function attachGiftWrapData($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order 
            && (
                    $entity->isOnInitialState() 
                    || ($args instanceof PreUpdateEventArgs && $args->hasChangedField('giftWrap'))
                    || ($args->hasChangedField('status') && array_key_exists($args->getOldValue('status'), Order::getInitialStateStatuses()))
                )
            ) {
            
            $giftWrapData = null;
            if ($entity->getGiftWrap()) {
                $name = $entity->getGiftWrap()->getName();
                $price = $entity->getGiftWrap()->getPrice();
                $description = $entity->getGiftWrap()->getDescription();
    
                $entity->setGiftWrapName($name);
                $entity->setGiftWrapPrice($price);
                $entity->setGiftWrapDescription($description);
    
                $giftWrapData = $this->serializer->serialize( $entity->getGiftWrap(), 'json' );
                $giftWrapData = json_decode( $giftWrapData, true );
            }
            
            $data = $entity->getData();
            $data['giftWrap'] = $giftWrapData;
            $entity->setData($data);
        }
    }

    public function attachPromoCodeData($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order 
            && ($entity->isOnInitialState() 
                    || ($args instanceof PreUpdateEventArgs && $args->hasChangedField('promoCode'))
                    || ($args->hasChangedField('status') && array_key_exists($args->getOldValue('status'), Order::getInitialStateStatuses()))
                )
            ) {

            $promoCodeData = null;
            $promoCode = $entity->getPromoCode();
            if ($promoCode) {
                // $code = $entity->getPromoCode()->getCode();
                // $minimumOrderTotal = $entity->getPromoCode()->getMinimumOrderTotal();
                // $entity->setPromoCodeCode($code);
                // $entity->setPromoCodeMinimumOrderTotal($minimumOrderTotal);
                $entity->setPromoCodeCode($promoCode->getCode());
                $entity->setPromoCodeValue($promoCode->getValue());
                $entity->setPromoCodeMinimumOrderTotal($promoCode->getMinimumOrderTotal());
                $entity->setPromoCodeStartDate($promoCode->getStartDate());
                $entity->setPromoCodeEndDate($promoCode->getEndDate());

                $promoCodeData = $this->serializer->serialize( $entity->getPromoCode(), 'json' );
                $promoCodeData = json_decode( $promoCodeData, true );
            } else {
                $entity->setPromoCodeCode(null);
                $entity->setPromoCodeValue(null);
                $entity->setPromoCodeMinimumOrderTotal(null);
                $entity->setPromoCodeStartDate(null);
                $entity->setPromoCodeEndDate(null);
            }

            $data = $entity->getData();
            $data['promoCode'] = $promoCodeData;
            $entity->setData($data);
        }
    }

    public function attachOtherData($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order) {
            if ($entity->getBilling()) {
                $billing = array(
                    'city' => $entity->getBilling()->getCity(),
                    'firstName' => $entity->getBilling()->getFirstName(),
                    'lastName' => $entity->getBilling()->getLastName(),
                    'address1' => $entity->getBilling()->getAddress1(),
                    'address2' => $entity->getBilling()->getAddress2(),
                    'postalCode' => $entity->getBilling()->getPostalCode(),
                    'contactNumber' => $entity->getBilling()->getContactNumber(),
                );
                $entity->setBillingCity($billing['city']);
                $entity->setBillingFirstName($billing['firstName']);
                $entity->setBillingLastName($billing['lastName']);
                $entity->setBillingAddress1($billing['address1']);
                $entity->setBillingAddress2($billing['address2']);
                $entity->setBillingPostalCode($billing['postalCode']);
                $entity->setBillingContactNumber($billing['contactNumber']);
            }
            if ($entity->getShipping()) {
                $shipping = array(
                    'city' => $entity->getShipping()->getCity(),
                    'firstName' => $entity->getShipping()->getFirstName(),
                    'lastName' => $entity->getShipping()->getLastName(),
                    'address1' => $entity->getShipping()->getAddress1(),
                    'address2' => $entity->getShipping()->getAddress2(),
                    'postalCode' => $entity->getShipping()->getPostalCode(),
                    'contactNumber' => $entity->getShipping()->getContactNumber(),
                );
                $entity->setShippingCity($shipping['city']);
                $entity->setShippingFirstName($shipping['firstName']);
                $entity->setShippingLastName($shipping['lastName']);
                $entity->setShippingAddress1($shipping['address1']);
                $entity->setShippingAddress2($shipping['address2']);
                $entity->setShippingPostalCode($shipping['postalCode']);
                $entity->setShippingContactNumber($shipping['contactNumber']);
            }      
        }        
    }

    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }

    public function setOrderManager($orderManager)
    {
        $this->orderManager = $orderManager;
    }

    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /*public function attachSerializedData(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order) {
            $userData = $this->serializer->serialize( $entity->getUser(), 'json' );
            $giftWrapData = $this->serializer->serialize( $entity->getGiftWrap(), 'json' );
            // $promoCodeData = $this->serializer->serialize( $entity->getPromoCode(), 'json' );

            $data = array(
                'user' => $userData,
                'giftWrap' => $giftWrapData,
            );
            $entity->setData($data);
        }

    }*/

    /*public function preRemove(LifecycleEventArgs $args)
    {
        $this->updateVotesCount($args, true);
    }

    public function generateReferenceNumber($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof RemittanceEntry || $entity instanceof DiaryEntry) {
            $keygen = new KeyGenerator('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $keys = $keygen->generateUniqueKeys(7, 1);

            $entityManager = $this->remittanceEntryManager;
            if ($entity instanceof DiaryEntry) {
                $entityManager = $this->diaryEntryManager;
            }
            $entry = $entityManager->findOneBy(array('referenceNumber' => $keys[0]));
            while ($entry) {
                $keys = $keygen->generateUniqueKeys(7, 1);
                $entry = $entityManager->findOneBy(array('referenceNumber' => $keys[0]));
            }
            $entity->setReferenceNumber($keys[0]);
        }
    }

    public function generateSlug($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof DiaryEntry) {
            if ($entity->getId()) {
                $slug = $entity->getSlug();
                if (empty($slug)) {
                    $slugify = new Slugify();
                    $slug = $slugify->slugify($entity->getTitle());
                }
                $entry = $this->diaryEntryManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                while ($entry) {
                    $slug = sprintf('%s-%d', $entry->getSlug(), 1);
                    $slugCheck = $slugify->slugify($entry->getTitle());
                    if ($slugCheck != $entry->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $entry->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $entry = $this->diaryEntryManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                }
                $entity->setSlug($slug);
            } else {
                $slugify = new Slugify();
                $slug = $slugify->slugify($entity->getTitle());
                $entry = $this->diaryEntryManager->findOneBy(array('slug' => $slug));
                while ($entry) {
                    $slug = sprintf('%s-%d', $entry->getSlug(), 1);
                    $slugCheck = $slugify->slugify($entry->getTitle());
                    if ($slugCheck != $entry->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $entry->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $entry = $this->diaryEntryManager->findOneBy(array('slug' => $slug));
                }
                $entity->setSlug($slug);
            }
        }
    }

    public function updateVotesCount($args, $decrement = false)
    {
        $entity = $args->getObject();

        if ($entity instanceof DiaryEntryVote) {
            $this->diaryEntryManager->updateVotesCount($entity->getDiaryEntry(), $decrement);
        }
    }

    public function setRemittanceEntryManager($remittanceEntryManager)
    {
        $this->remittanceEntryManager = $remittanceEntryManager;
    }

    public function setDiaryEntryManager($diaryEntryManager)
    {
        $this->diaryEntryManager = $diaryEntryManager;
    }*/
}