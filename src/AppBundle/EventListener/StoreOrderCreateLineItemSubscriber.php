<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Event\Store\OrderCreateLineItemEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\EntityManager\Store\OrderLineItemManager;
use AppBundle\EntityManager\Store\OrderManager;
use AppBundle\Entity\Store\Order;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;

class StoreOrderCreateLineItemSubscriber implements EventSubscriberInterface
{
    protected $session;

    protected $requestStack;

    protected $request;

    protected $router;

    protected $orderLineItemManager;

    protected $orderManager;

    protected $storeOrderService;

    // protected $authorizationChecker;

    public function __construct(SessionInterface $session, RequestStack $requestStack, $router, OrderLineItemManager $orderLineItemManager, OrderManager $orderManager, $storeOrderService)
    {
        $this->session = $session;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
        $this->router = $router;
        $this->orderLineItemManager = $orderLineItemManager;
        $this->orderManager = $orderManager;
        $this->storeOrderService = $storeOrderService;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderCreateLineItemEvent::NAME => array(
                array('onCreateLineItem', -1)
            ),
        ];
    }

    public function onCreateLineItem(OrderCreateLineItemEvent $event)
    {
        $orderLineItem = $event->getOrderLineItem();
        // Change retrieval of current user
        $user = $event->getUser();
        $sessionId = $this->session->getId();
        $cartUniqId = $this->storeOrderService->getCartUniqueID();

        $ipAddress = $this->request->getClientIp();

        $activeOrder = $this->orderManager->getUserActiveOrder(array('user' => $user));
        if ($activeOrder) {
            if ($activeOrder->getStatus() == Order::STATUS_EXPIRED) {
                // $activeOrder = clone $activeOrder;
                $this->session->getFlashBag()->add(
                    'warning', 
                    'We have just refreshed your cart. Please try adding again the product on your cart.'
                );
                $redirectRoute = $this->router->generate($this->request->get('_route'), $this->request->get('_route_params'));
                $response = new RedirectResponse($redirectRoute);
                $this->storeOrderService->setForceGenerateCartUniqueIDFlag();
                $event->setResponse($response);
            } elseif ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
                // @todo: Log access denied on someone's order
                $response = new RedirectResponse($this->router->generate('app_store_products_list'));
                $this->storeOrderService->setForceGenerateCartUniqueIDFlag();
                $event->setResponse($response);
                return;
            }
        }

        if (!$activeOrder) {
            $activeOrder = $this->orderManager->createNewOrder($user, $sessionId, $cartUniqId, $ipAddress);
            // <field name="username" type="string" column="username" length="150"/>
            // <field name="userEmail" type="string" column="user_email" length="255"/>
            // <field name="userFirstName" type="string" column="user_firstname" length="255"/>
            // <field name="userLastName" type="string" column="user_lastname" length="255"/>
            // <field name="userMiddleName" type="string" column="user_middlename" length="255" nullable="true"/>            
        } else {
            $activeOrder->setUser($user);
            $activeOrder->setUniqid($cartUniqId);
            // $activeOrder = $activeOrders[0];
        }
        
        $action = 'create';
        if ($activeOrder->getId()) {
            $lineItem = $this->orderLineItemManager->findOneBy(array('order' => $activeOrder, 'product' => $orderLineItem->getProduct()));
            if ($lineItem) {
                $action = 'update';
                $orderLineItemClone = clone $orderLineItem;
                // $quantityUpdate = $lineItem->getQuantity() + $orderLineItemClone->getQuantity();
                $quantityUpdate = $orderLineItemClone->getQuantity();
                $orderLineItem = $lineItem;
                $orderLineItem->setQuantity($quantityUpdate);
            }
        }
        
        try {
            $this->orderManager->saveOrderWithLineItem($activeOrder, array($orderLineItem), $action);
        } catch (\Exception $e) {
            // @todo: Log error
            // $this->session->getFlashBag()->add('warning', 'There was a error encountered while adding a product on your cart. Please contact administrator.');
            $event->setException($e);
        }
    }
}