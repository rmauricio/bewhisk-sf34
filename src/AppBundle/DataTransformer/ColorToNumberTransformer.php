<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\Store\Color;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ColorToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (color) to a string (number).
     *
     * @param  Color|null $color
     * @return string
     */
    public function transform($color)
    {
        if (null === $color) {
            return '';
        }

        return $color->getId();
    }

    /**
     * Transforms a string (number) to an object (color).
     *
     * @param  string $colorNumber
     * @return Color|null
     * @throws TransformationFailedException if object (color) is not found.
     */
    public function reverseTransform($colorNumber)
    {
        // no color number? It's optional, so that's ok
        if (!$colorNumber) {
            return;
        }

        $color = $this->entityManager
            ->getRepository(Color::class)
            // query for the color with this id
            ->find($colorNumber)
        ;

        if (null === $color) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A color with ID "%s" does not exist!',
                $colorNumber
            ));
        }

        return $color;
    }
}