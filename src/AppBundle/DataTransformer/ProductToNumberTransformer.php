<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\Store\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (product) to a string (number).
     *
     * @param  Product|null $product
     * @return string
     */
    public function transform($product)
    {
        if (null === $product) {
            return '';
        }

        return $product->getId();
    }

    /**
     * Transforms a string (number) to an object (product).
     *
     * @param  string $productNumber
     * @return Product|null
     * @throws TransformationFailedException if object (product) is not found.
     */
    public function reverseTransform($productNumber)
    {
        // no product number? It's optional, so that's ok
        if (!$productNumber) {
            return;
        }

        $product = $this->entityManager
            ->getRepository(Product::class)
            // query for the product with this id
            ->find($productNumber)
        ;

        if (null === $product) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A product with ID "%s" does not exist!',
                $productNumber
            ));
        }

        return $product;
    }
}