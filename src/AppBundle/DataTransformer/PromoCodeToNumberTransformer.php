<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\Store\PromoCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class PromoCodeToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (promoCode) to a string (number).
     *
     * @param  PromoCode|null $promoCode
     * @return string
     */
    public function transform($promoCode)
    {
        if (null === $promoCode) {
            return '';
        }

        return $promoCode->getId();
    }

    /**
     * Transforms a string (number) to an object (promoCode).
     *
     * @param  string $promoCodeNumber
     * @return PromoCode|null
     * @throws TransformationFailedException if object (promoCode) is not found.
     */
    public function reverseTransform($promoCodeNumber)
    {
        // no promoCode number? It's optional, so that's ok
        if (!$promoCodeNumber) {
            return;
        }

        $promoCode = $this->entityManager
            ->getRepository(PromoCode::class)
            // query for the promoCode with this id
            ->findOneBy(array('code' => $promoCodeNumber))
        ;

        if (null === $promoCode) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A promo code with ID "%s" does not exist!',
                $promoCodeNumber
            ));
        }

        return $promoCode;
    }
}