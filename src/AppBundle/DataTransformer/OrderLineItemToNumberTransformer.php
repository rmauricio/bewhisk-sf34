<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\Store\Order;
use AppBundle\Entity\Store\OrderLineItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class OrderLineItemToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (orderLineItem) to a string (number).
     *
     * @param  OrderLineItem|null $orderLineItem
     * @return string
     */
    public function transform($orderLineItem)
    {
        if (null === $orderLineItem) {
            return '';
        }

        return $orderLineItem->getId();
    }

    /**
     * Transforms a string (number) to an object (orderLineItem).
     *
     * @param  string $orderLineItemNumber
     * @return OrderLineItem|null
     * @throws TransformationFailedException if object (orderLineItem) is not found.
     */
    public function reverseTransform($orderLineItemNumber)
    {
        // no orderLineItem number? It's optional, so that's ok
        if (!$orderLineItemNumber) {
            return;
        }

        $orderLineItem = $this->entityManager
            ->getRepository(OrderLineItem::class)
            // query for the color with this id
            ->find($orderLineItemNumber)
        ;

        if (null === $orderLineItem) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A order line item with ID "%s" does not exist!',
                $orderLineItemNumber
            ));
        }

        return $orderLineItem;
    }
}