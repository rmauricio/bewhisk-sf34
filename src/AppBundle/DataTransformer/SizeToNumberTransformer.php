<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\Store\Size;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class SizeToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (size) to a string (number).
     *
     * @param  Size|null $size
     * @return string
     */
    public function transform($size)
    {
        if (null === $size) {
            return '';
        }

        return $size->getId();
    }

    /**
     * Transforms a string (number) to an object (size).
     *
     * @param  string $sizeNumber
     * @return Size|null
     * @throws TransformationFailedException if object (size) is not found.
     */
    public function reverseTransform($sizeNumber)
    {
        // no size number? It's optional, so that's ok
        if (!$sizeNumber) {
            return;
        }

        $size = $this->entityManager
            ->getRepository(Size::class)
            // query for the size with this id
            ->find($sizeNumber)
        ;

        if (null === $size) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A size with ID "%s" does not exist!',
                $sizeNumber
            ));
        }

        return $size;
    }
}