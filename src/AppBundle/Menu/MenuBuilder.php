<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    private $requestStack;

    private $router;

    private $securityAuthorizationChecker;

	private $securityTokenStorage;
	
    private $categoryManager;

    private $items;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, $requestStack, $router, $securityAuthorizationChecker, $securityTokenStorage, $categoryManager, array $items)
    {
        $this->factory = $factory;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->categoryManager = $categoryManager;
        $this->items = $items;
    }

    /**
     * Creates the main menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu()
    {
        return $this->generateMenu('main');
    }

    /**
     * Creates the product categories menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function productCategoriesMenu()
    {
        return $this->generateMenu('product_categories');
	}

    /**
     * Creates the user menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function userMenu1()
    {
        return $this->generateMenu('user1');
    }

    /**
     * Creates the user menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function userMenu2()
    {
        return $this->generateMenu('user2');
    }
	
	protected function getMenuArray($menuKey)
	{
		if ( $menuKey == 'product_categories' ) {
			$this->items[$menuKey]['items'] = $this->_getCategoryMenuLinkItems($menuKey, true);
		}

		if (isset($this->items[$menuKey]['items']['shop'])) {
			$items = $this->_getCategoryMenuLinkItems($menuKey);
			array_walk($items, array($this, '_alterMainMenuCategoryLinks'));
			$this->items[$menuKey]['items'] = array_merge( $this->items[$menuKey]['items'], $items );
		}

		return $this->items[$menuKey];
	}
	
	protected function generateMenu($menuKey)
	{
		$menuArray = $this->getMenuArray($menuKey);
		$menuOptions = $menuArray['options'];
		$menu = $this->factory->createItem('main', $menuOptions);
		
		$request = $this->requestStack->getCurrentRequest();
		
		try {
			$routeParams = $this->router->matchRequest($request);
		} catch (\Exception $e) {
			$routeParams = array();
		}

		$currentRequestUri = $request->getRequestUri();
		foreach ($menuArray['items'] as $menuItemKey => $menuItem) {

			$menuItemParams = array();

			if (!empty($menuItem['role'])) {
				if ('IS_AUTHENTICATED_ANONYMOUSLY' == $menuItem['role'] && $this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
					continue;
				} elseif (!$this->securityAuthorizationChecker->isGranted($menuItem['role'])) {
					continue;
				}
			}

			if (!empty($menuItem['label'])) {
				$menuItemParams['label'] = $menuItem['label'];
			}
			if (!empty($menuItem['label_callback']) && method_exists($this, $menuItem['label_callback'])) {
				$callbackResult = $this->{$menuItem['label_callback']}();
				$menuItemParams['label'] = sprintf($menuItem['label'], $callbackResult);
			}
			
			$currentItem = false;
			if (!empty($menuItem['uri'])) {
				$menuItemParams['uri'] = $this->router->generate('page_slug', array('path' => $menuItem['uri']));
			} elseif (!empty($menuItem['route_name'])) {
				$routeParams = array();
				if (!empty($menuItem['route_parameters'])) {
					$routeParams = $menuItem['route_parameters'];
				}
				$menuItemParams['uri'] = $this->router->generate($menuItem['route_name'], $routeParams);
			}
			
			$menuObject = $menu;
			if ($ancestors = $this->getAncestors($menuItem, $menuArray['items'])) {
				for ($i = (count($ancestors) - 1); $i >= 0; $i--) {
					$menuObject = $menuObject[$ancestors[$i]['label']];
				}
			}
			
			// $menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra();
			if (isset($menuItem['safe_label'])) {
				//$menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra('safe_label', $menuItem['safe_label']);
				$menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra('safe_label', $menuItem['safe_label']);
			} else {
				//$menuObject->addChild($menuItem['label'], $menuItemParams);
				$menuObject->addChild($menuItem['label'], $menuItemParams);
			}
			
			$menuItemObject = $menuObject[$menuItem['label']];
			
			if (!empty($menuItem['link_attributes'])) {
				foreach ($menuItem['link_attributes'] as $attributeKey => $attributeValue) {
					$menuItemObject->setLinkAttribute($attributeKey, $attributeValue);
				}
			}

			if (!isset($menuItem['attributes']['class'])) {
				$menuItem['attributes']['class'] = '';
			}
			if (stripos($currentRequestUri, $menuItemObject->getUri()) === 0) {
				$menuItem['attributes']['class'] = $menuItem['attributes']['class'] . ' ancestor';
			}

			if (!empty($menuItem['attributes'])) {
				foreach ($menuItem['attributes'] as $attributeKey => $attributeValue) {
					$menuItemObject->setAttribute($attributeKey, $attributeValue);
				}
			}

			if (isset($menuItem['icon'])) {
				$menuItemObject->setExtra('icon', $menuItem['icon']);
			}
			
			if (isset($menuItem['label_template']) && !empty($menuItem['label_template'])) {
				$menuItemObject->setExtra('label_template', $menuItem['label_template']);
			}
			
			if ($currentRequestUri == $menuItemObject->getUri()) {
				$menuItemObject->setCurrent(true);
			} /*elseif (stripos($currentRequestUri, $menuItemObject->getUri()) === 0) {
				$menuItemObject->setAttribute('class', 'ancestor');
			}*/

			// if ($menuKey == 'product_categories') {
			// 	dump($this->requestStack->getCurrentRequest()->query->get('product_filter'));
			// }
		}

        return $menu;
	}
	
	protected function getAncestors($menuItem, $menuItems = array())
	{
		$ancestors = array();
		
		if (!empty($menuItem['parent']) && array_key_exists($menuItem['parent'], $menuItems)) {
			$parentMenuItem = $menuItems[$menuItem['parent']];
			array_push($ancestors, $parentMenuItem);
			foreach ($this->getAncestors($parentMenuItem, $menuItems) as $key => $ancestor) {
				array_push($ancestors, $ancestor);
			}
		}
		
		return $ancestors;
	}

	private function getCurrentUserName()
	{
		$user = $this->securityTokenStorage->getToken()->getUser();

		if (!$user && !$this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
			return null;
		}

		return $user->getFirstname();
	}

	private function getCurrentUser()
	{
		return $this->securityTokenStorage->getToken()->getUser();
	}
	
	private function _getCategoryMenuLinkItems($menuKey, $attachQueryParams = false)
	{
		$items = array();
		$categories = $this->categoryManager->getProductCategoriesForNav();

		$request = $this->requestStack->getCurrentRequest();
		
		try {
			$routeParams = $this->router->matchRequest($request);
		} catch (\Exception $e) {
			$routeParams = array();
		}
		dump($routeParams);

		foreach ($categories as $category) {
			$menuItemKey = str_replace('-', '_', $category->getSlug());
			$parentKey = $category->getParent() ? str_replace('-', '_', $category->getParent()->getSlug()) : null;

			$categoryRouteParams = array(
				'parentCategorySlug' => $category->getParent()->getSlug(),
				'categorySlug' => $category->getSlug(),
			);

			$items[$menuItemKey] = array(
				'label' => $category->getName(),
				/*'uri' => $this->router->generate('app_store_products_category', array(
					'parentCategorySlug' => $category->getParent()->getSlug(),
					'categorySlug' => $category->getSlug(),
				)),*/
				'safe_label' => false,
				'route_name' => 'app_store_products_category',
				'route_parameters' => $categoryRouteParams,
				'role' => null,
				'link_attributes' => array(),
				'parent' => $parentKey
			);

			if ($attachQueryParams) {
				$request = $this->requestStack->getCurrentRequest();
				$items[$menuItemKey]['route_parameters'] = array_merge( $items[$menuItemKey]['route_parameters'], $request->query->all() );
			}
		}

		return $items;
	}

	private function _alterMainMenuCategoryLinks(&$item)
	{
		if ($item['parent'] == 'product') {
			$item['parent'] = 'shop';
		}
	}
}