<?php

namespace AppBundle\Service\Store;

class MailerService
{
    protected $mailer;

    protected $templating;

    protected $orderService;

    protected $storeSettings;

    public function __construct($mailer, $templating, $orderService, $mailConfig = array(), $storeSettings = array())
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->orderService = $orderService;
        $this->mailConfig = $mailConfig;
        $this->storeSettings = $storeSettings;
    }

    public function sendCustomerOrderConfirmationEmail($order)
    {
        $result = false;

        try {
            $orderTotals = $this->orderService->getTotals($order);

            $mailConfig = $this->mailConfig['customer_order_confirmation'];
            
            $emailTemplate = $this->templating->render(
                $mailConfig['template'],
                array(
                    'order' => $order,
                    'orderTotals' => $orderTotals,
                )
            );

            $message = \Swift_Message::newInstance()
                ->setSubject(str_replace("[ORDER_NUMBER]", $order->getOrderNumber(), $mailConfig['subject']))
                ->setFrom($mailConfig['from'])
                ->setTo($order->getBillingEmail())
                ->setCc($mailConfig['cc'])
                ->setBcc($mailConfig['bcc'])
                ->setBody($emailTemplate, 'text/html')
            ;

            // $test = file_put_contents('testmail-' . date('Ymd-His') . '.html', $emailTemplate);
            // $test = file_put_contents('testmail.html', $emailTemplate);
            // dump($test);die();

            $result = $this->mailer->send($message);
        } catch (\Exception $e) {
            // todo: Log
            throw $e;
        }

        return $result;
    }

    public function sendOrderPaidConfirmationEmail($order)
    {
        $result = false;

        try {
            // $orderTotals = $this->orderService->getTotals($order);

            $mailConfig = $this->mailConfig['paid_order_confirmation'];
            
            $emailTemplate = $this->templating->render(
                $mailConfig['template'],
                array(
                    'order' => $order,
                    // 'orderTotals' => $orderTotals,
                )
            );

            $message = \Swift_Message::newInstance()
                ->setSubject(str_replace("[ORDER_NUMBER]", $order->getOrderNumber(), $mailConfig['subject']))
                ->setFrom($mailConfig['from'])
                ->setTo($order->getBillingEmail())
                ->setCc($mailConfig['cc'])
                ->setBcc($mailConfig['bcc'])
                ->setBody($emailTemplate, 'text/html')
            ;

            // $test = file_put_contents('testmail-' . date('Ymd-His') . '.html', $emailTemplate);
            //$test = file_put_contents('testmail.html', $emailTemplate);
            //dump($test);die();

            $result = $this->mailer->send($message);
        } catch (\Exception $e) {
            // todo: Log
            throw $e;
        }

        return $result;
    }
}