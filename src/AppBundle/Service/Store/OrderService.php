<?php

namespace AppBundle\Service\Store;

use Symfony\Component\HttpFoundation\Cookie;

class OrderService
{
    protected $requestStack;

    protected $request;

    protected $session;

    protected $orderManager;

    protected $cartUniqidKey;

    protected $storeSettings;

    public function __construct($requestStack, $session, $orderManager, $cartUniqidKey, $storeSettings = array())
    {
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->orderManager = $orderManager;
        $this->cartUniqidKey = $cartUniqidKey;
        $this->storeSettings = $storeSettings;
    }

    public function getCartUniqueID($request = null)
    {
        if (!$request) {
            $request = $this->requestStack->getCurrentRequest();
        }
        if ($request->cookies->has($this->cartUniqidKey)) {
            return $request->cookies->get($this->cartUniqidKey);
        }
        
        return null;
    }

    public function generateCartUniqueID($response, $force = false)
    {
        $cartUniqid = hash("sha256", strtotime("now") . $this->session->getId() . uniqid("", true));
        
        if (!$this->requestStack->getCurrentRequest()->cookies->has($this->cartUniqidKey) || $force) {
            $response->headers->setCookie(
                // new Cookie('bw_cart_uniqid', $cartUniqid, 0, "/", $request->headers->get('host'))
                new Cookie($this->cartUniqidKey, $cartUniqid, 0, "/")
            );
        }

        return $cartUniqid;
    }

    public function setForceGenerateCartUniqueIDFlag()
    {
        $this->session->getFlashBag()->add('GenCartUniqueKey', md5(strtotime("now")));
    }

    public function isFreeShippingFee($orderTotal, $freeMinTotal = null)
    {
        if (empty($freeMinTotal) || ($freeMinTotal && $freeMinTotal <= 0)) {
            return false;
        }

        if ($orderTotal >= $freeMinTotal) {
            return true;
        }

        return false;
    }

    public function hasValidPromoCode($order, $totalAmount)
    {
        $promoCode = $order->getPromoCode();
        if (!$promoCode) {
            return false;
        }

        $currentDateTime = new \DateTime();

        if (!$promoCode->getEnabled()) {
            return false;
        } elseif ($promoCode->getStartDate()->format('U') > $currentDateTime->format('U')) {
            return false;
        } elseif ($promoCode->getEndDate() && $promoCode->getEndDate()->format('U') < $currentDateTime->format('U')) {
            return false;
        } else {
            if ($totalAmount < $promoCode->getMinimumOrderTotal()) {
                return false;
            }
        }

        return true;
    }

    public function getTotals($order, $forceDataFromRef = false)
    {
        $totals = $order->getTotals($forceDataFromRef);

        if ($order->isOnInitialState() || $forceDataFromRef) {
            $shippingFee = 0;
            // Get shipping fee
            if (isset($this->storeSettings['free_shipping_fee'])) {
                $freeShippingFeeMin = $this->storeSettings['free_shipping_fee'];
            }
            if (isset($this->storeSettings['shipping_fee']) && !$this->isFreeShippingFee($totals['lineItemsFinalTotalPrice'], $freeShippingFeeMin)) {
                $shippingFee = $this->storeSettings['shipping_fee'];
            }

            $totals['shippingFee'] = $shippingFee;
            $totals['totalPrice'] = $totals['totalPrice'] + $shippingFee;
            $totals['finalTotalPrice'] = $totals['finalTotalPrice'] + $shippingFee;
        }

        return $totals;
    }

    public function prepareForCheckout($order) 
    {
        $orderNumber = $this->generateOrderNumber($order);
        $orderNumberCheck = $this->orderManager->findOneBy(array('orderNumber' => $orderNumber));
        while ($orderNumberCheck) {
            $orderNumber = $this->generateOrderNumber($order);
            $orderNumberCheck = $this->orderManager->findOneBy(array('orderNumber' => $orderNumber));
        }
        $order->setOrderNumber($orderNumber);
    }

    public function generateOrderNumber($order)
    {
        $keygen = new \Keygen\Utility\Generator\KeyGenerator('abcdefghijklmnopqrstuvwxyz');

        $paymentMethodPrefix = 'W';
        if ($order->getPaymentMethod()) {
            $paymentMethodPrefix = substr($order->getPaymentMethod(), 0, 1);
        }
        $aphaKeys = $keygen->generateUniqueKeys(2, 1);

        $orderIdZeroFillLength = 6;
        $orderIdZeroFill = $order->getId();
        $orderIdZeroFill = str_repeat("0", ($orderIdZeroFillLength - strlen($orderIdZeroFill))) . $orderIdZeroFill;
        
        $orderNumber = strtoupper($paymentMethodPrefix . $aphaKeys[0] . $orderIdZeroFill . date('Ymdhi'));

        return $orderNumber;
    }

    public function getLineItemsPriceTotalsData($order, $key = null)
    {
        $orderData = $order->getData();

        $currentShippingFee = 0;
        $shippingFee = 0;

        $promoCodeDiscount = !empty($orderData['promoCode']['value']) ? $orderData['promoCode']['value'] : 0;
        $currentPromoCodeDiscount = 0;

        $giftWrap = !empty($orderData['giftWrap']['price']) ? $orderData['giftWrap']['price'] : 0;
        $total = 0;
        $totalDiscount = 0;
        $totalFinal = 0;
        $grandTotal = 0;
        $discountGrandTotal = 0;
        $lineItemsFinalTotal = 0;
        $finalGrandTotal = 0;
        $lineItemsPriceData = array();

        $currentGiftWrap = $order->getGiftWrap() ? $order->getGiftWrap()->getPrice() : 0;
        $currentTotal = 0;
        $currentTotalDiscount = 0;
        $currentTotalFinal = 0;
        $currentGrandTotal = 0;
        $currentDiscountGrandTotal = 0;
        $currentLineItemsFinalTotal = 0;
        $currentFinalGrandTotal = 0;

        if (count($order->getLineItems()) <= 0) {
            return array(
                'promoCodeDiscount' => $promoCodeDiscount,
                'giftWrap' => $giftWrap,
                'total' => $total,
                'totalDiscount' => $totalDiscount,
                'totalFinal' => $totalFinal,
                // 
                'grandTotal' => $grandTotal,
                'discountGrandTotal' => $discountGrandTotal,
                'shippingFee' => $shippingFee,
                'lineItemsFinalTotal' => $lineItemsFinalTotal,
                'finalGrandTotal' => $finalGrandTotal,
                // 
                'current' => array(
                    'promoCodeDiscount' => $currentPromoCodeDiscount,
                    'giftWrap' => $currentGiftWrap,
                    'total' => $currentTotal,
                    'totalDiscount' => $currentTotalDiscount,
                    'totalFinal' => $currentTotalFinal,
                    // 
                    'grandTotal' => $currentGrandTotal,
                    'discountGrandTotal' => $currentDiscountGrandTotal,
                    'shippingFee' => $currentShippingFee,
                    'lineItemsFinalTotal' => $currentLineItemsFinalTotal,
                    'finalGrandTotal' => $currentFinalGrandTotal,
                ),
                'lineItemsPriceData' => $lineItemsPriceData
            );
        }

        foreach ($order->getLineItems() as $lineItem) {
            $priceData = $lineItem->getPriceData();
            $lineItemsPriceData[$lineItem->getId()] = $priceData;

            $total = $total + $priceData['price'];
            $totalDiscount = $totalDiscount + $priceData['discount'];
            $totalFinal = $totalFinal + $priceData['final'];
            
            $grandTotal = $grandTotal + $priceData['subTotal'];
            $discountGrandTotal = $discountGrandTotal + $priceData['discountSubTotal'];
            $finalGrandTotal = $finalGrandTotal + $priceData['finalSubTotal'];

            $lineItemsFinalTotal = $lineItemsFinalTotal + $priceData['finalSubTotal'];

            // Current
            $currentTotal = $currentTotal + $priceData['current']['price'];
            $currentTotalDiscount = $currentTotalDiscount + $priceData['current']['discount'];
            $currentTotalFinal = $currentTotalFinal + $priceData['current']['final'];

            $currentGrandTotal = $currentGrandTotal + $priceData['current']['subTotal'];
            $currentDiscountGrandTotal = $currentDiscountGrandTotal + $priceData['current']['discountSubTotal'];
            $currentFinalGrandTotal = $currentFinalGrandTotal + $priceData['current']['finalSubTotal'];

            $currentLineItemsFinalTotal = $currentLineItemsFinalTotal + $priceData['current']['finalSubTotal'];
        }

        $freeShippingFeeMin = 0;
        $freeShippingFeeMinCurrent = 0;

        if ($orderData) {
            if (isset($orderData['store_settings']['shipping_fee'])) {
                $shippingFee = $orderData['store_settings']['shipping_fee'];
            }
            if (isset($orderData['store_settings']['free_shipping_fee'])) {
                $freeShippingFeeMin = $orderData['store_settings']['free_shipping_fee'];
            }
        }
        if (isset($this->storeSettings['shipping_fee'])) {
            $currentShippingFee = $this->storeSettings['shipping_fee'];
        }
        if (isset($this->storeSettings['free_shipping_fee'])) {
            $freeShippingFeeMinCurrent = $this->storeSettings['free_shipping_fee'];
        }
        
        if ($this->isFreeShippingFee($totalFinal, $freeShippingFeeMin)) {
            $shippingFee = 0;
        }
        if ($this->isFreeShippingFee($currentTotalFinal, $freeShippingFeeMinCurrent)) {
            $currentShippingFee = 0;
        }

        if ($this->hasValidPromoCode($order, $currentTotalFinal)) {
            $currentPromoCodeDiscount = $order->getPromoCode()->getValue();
        }

        $finalGrandTotal = ($finalGrandTotal + $shippingFee + $giftWrap) - $promoCodeDiscount;
        $currentFinalGrandTotal = ($currentFinalGrandTotal + $currentShippingFee + $currentGiftWrap) - $currentPromoCodeDiscount;

        return array(
            'promoCodeDiscount' => $promoCodeDiscount,
            'giftWrap' => $giftWrap,
            'total' => $total,
            'totalDiscount' => $totalDiscount,
            'totalFinal' => $totalFinal,
            // 
            'grandTotal' => $grandTotal,
            'discountGrandTotal' => $discountGrandTotal,
            'shippingFee' => $shippingFee,
            'lineItemsFinalTotal' => $lineItemsFinalTotal,
            'finalGrandTotal' => $finalGrandTotal,
            // 
            'current' => array(
                'promoCodeDiscount' => $currentPromoCodeDiscount,
                'giftWrap' => $currentGiftWrap,
                'total' => $currentTotal,
                'totalDiscount' => $currentTotalDiscount,
                'totalFinal' => $currentTotalFinal,
                // 
                'grandTotal' => $currentGrandTotal,
                'discountGrandTotal' => $currentDiscountGrandTotal,
                'shippingFee' => $currentShippingFee,
                'lineItemsFinalTotal' => $currentLineItemsFinalTotal,
                'finalGrandTotal' => $currentFinalGrandTotal,
            ),
            'lineItemsPriceData' => $lineItemsPriceData,
        );
    }
}