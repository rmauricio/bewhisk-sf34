<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;
use Application\Sonata\ClassificationBundle\Entity\Category;
use Sonata\ClassificationBundle\Model\CategoryInterface;

/**
 * ColorManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class ColorManager extends BaseEntityManager
{
    public function getParents()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder
			->where('c.enabled = :enabled')
			->andWhere('c.parent IS NULL')
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('enabled', true)
		;

		return $queryBuilder->getQuery()->getResult();
	}
	
	public function findByCategries($catgories)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('c')
			->from($this->getClass(), 'c')
		;

		$queryBuilder->join('c.categories', 'c1');

		$categoryIds = array();
		foreach ($catgories as $category) {
			if ($category instanceof CategoryInterface) {
				$categoryIds[] = $category->getId();
			} else {
				$categoryIds[] = $category;
			}
		}

		$queryBuilder
			->where('c.enabled = :enabled')
			->andWhere(
				$queryBuilder->expr()->in('c1.id', ':categoryIds')
			)
			->addOrderBy('c.position', 'ASC')
			->addOrderBy('c.name', 'ASC')
        ;
        
		$queryBuilder
			->setParameter('enabled', true)
			->setParameter('categoryIds', $categoryIds)
		;

		return $queryBuilder->getQuery()->getResult();
	}
}
