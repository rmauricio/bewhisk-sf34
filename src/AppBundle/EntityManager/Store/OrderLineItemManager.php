<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;

/**
 * OrderLineItemManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class OrderLineItemManager extends BaseEntityManager
{
    public function updatePriceData($lineItem, $flush = true)
    {
        $priceData = $lineItem->getPriceData();

        $lineItem->setPrice($priceData['price']);
        $lineItem->setFinalPrice($priceData['finalPrice']);

        if ($flush) {
            $entityManager = $this->getEntityManager();
            $entityManager->persist($lineItem);
            $entityManager->flush();
        }
    }
}
