<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;
use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Store\Order;
use Application\Sonata\UserBundle\Entity\User;

/**
 * ProductManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class ProductManager extends BaseEntityManager
{
    private $storeSettings;

    public function findOneProductBy(array $criteria)
    {
        if (empty($criteria)) {
            return null;
        }

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $currentDateTime = date("Y-m-d H:i:s");
        $parameters = array(
            'enabled' => true,
            'publicationStartDate' => $currentDateTime,
            'publicationEndDate' => $currentDateTime,
        );

		$queryBuilder
			->select('p')
            ->from($this->getClass(), 'p')
            ->where('p.enabled = :enabled')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->lte('p.publicationStartDate', ':publicationStartDate'),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->isNotNull('p.publicationEndDate'), 
                        $queryBuilder->expr()->gte('p.publicationEndDate', ':publicationEndDate')
                    )
                )
            )
        ;

        foreach ($criteria as $key => $value) {
            $operator = ' = ';
            if (is_array($value) && isset($value['value'])) {
                $operator = $value['operator'];
                $value = $value['value'];
            }

            if (stripos($key, '.') === false) {
                $key = sprintf('%s.%s', 'p', $key);
            }
            $placeholder = str_replace('.', '__', $key);

            $queryBuilder
                ->andWhere(sprintf('%s %s :%s', $key, $operator, $placeholder))
            ;
            $parameters[$placeholder] = $value;
        }

		$queryBuilder
		   ->setFirstResult(0)
		   ->setMaxResults(1)
		;

        $queryBuilder
			->setParameters($parameters)
        ;
        
		return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findProductCatalogBy(array $criteria, array $orderBy = null, $limit = null, $offset = null, &$remainingStocks = null, $activeOrder = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('p')
            ->from($this->getClass(), 'p')
        ;

        $hasAttributeCriteria = false;
        if (array_intersect(array_keys($criteria), array('colors', 'size'))) {
            $hasAttributeCriteria = true;
        }

        $currentDateTime = date("Y-m-d H:i:s");
        $parameters = array(
            'enabled' => true,
            'publicationStartDate' => $currentDateTime,
            'publicationEndDate' => $currentDateTime,
        );
        
        // Check for products availability by enabled and publication
        $queryBuilder
            ->where('p.enabled = :enabled')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->lte('p.publicationStartDate', ':publicationStartDate'),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->isNotNull('p.publicationEndDate'), 
                        $queryBuilder->expr()->gte('p.publicationEndDate', ':publicationEndDate')
                    )
                )
            )
        ;

        /*
        JOIN s.product p
                    LEFT JOIN p.lineItems l
                    LEFT JOIN l.order o 
                    WHERE s.product = :productId
                    AND (
                        o.status IN (:orderStatuses)
                        OR o.status IS NULL
                    )
         */
        
        /*$queryBuilder
            ->addSelect('( s.value - SUM( IFNULL( l.quantity, 0 ) ) ) AS remaining_stocks')
        ;
        $queryBuilder->join('p.stock', 's');
        $queryBuilder->leftJoin('p.lineItems', 'l');
        $queryBuilder->leftJoin('l.order', 'o');
        $queryBuilder->andWhere(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->isNull('o.status'), 
                $queryBuilder->expr()->gte('o.status', ':orderStatuses')
            )
        );
        $parameters['orderStatuses'] = array(
            Order::STATUS_NEW,
            Order::STATUS_IN_CART,
            Order::STATUS_IN_CHECKOUT,
            Order::STATUS_PENDING,
            Order::STATUS_IN_PROGRESS,
        );*/

        foreach ($criteria as $key => $value) {
            $operator = ' = ';
            if (is_array($value) && isset($value['value'])) {
                $operator = $value['operator'];
                $value = $value['value'];
            }

            $placeholder = str_replace('.', '__', $key);

            if ($key == 'colors') {
                $colors = $value;
                $value = array();
                foreach( $colors as $color ) {
                    $value[] = $color->getId();
                }
                $queryBuilder
                    ->andWhere(
                        $queryBuilder->expr()->in('p.color', ':colors')
                    )
                ;
                $parameters[$placeholder] = $value;
            } elseif ($key == 'categories') {
                $queryBuilder->join('p.categories', 'c');

                $categories = $value;
                $value = array();
                foreach( $categories as $category ) {
                    $value[] = $category->getId();
                }
                if ($hasAttributeCriteria) {
                    $queryBuilder
                        ->andWhere(
                            $queryBuilder->expr()->in('c.id', ':categories')
                        )
                    ;
                } else {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNotNull('p.parent'),
                                $queryBuilder->expr()->in('c.id', ':categories')
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNull('p.parent'),
                                $queryBuilder->expr()->eq('p.variationCount', 0),
                                $queryBuilder->expr()->in('c.id', ':categories')
                            )
                        )
                    );
                }
                $parameters[$placeholder] = $value;
            } elseif ($key == 'brand') {
                if ($hasAttributeCriteria) {
                    $queryBuilder->andWhere('p.brand = :brand');
                } else {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNotNull('p.parent'),
                                $queryBuilder->expr()->eq('p.brand', ':brand')
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNull('p.parent'),
                                $queryBuilder->expr()->eq('p.variationCount', 0),
                                $queryBuilder->expr()->eq('p.brand', ':brand')
                            )
                        )
                    );
                }
                $parameters[$placeholder] = $value;
            } elseif ($key == 'theme') {
                if ($hasAttributeCriteria) {
                    $queryBuilder->andWhere('p.theme = :theme');
                } else {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNotNull('p.parent'),
                                $queryBuilder->expr()->eq('p.theme', ':theme')
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNull('p.parent'),
                                $queryBuilder->expr()->eq('p.variationCount', 0),
                                $queryBuilder->expr()->eq('p.theme', ':theme')
                            )
                        )
                    );
                }
                $parameters[$placeholder] = $value;
            } elseif ($key == 'price') {
                if ($hasAttributeCriteria) {
                    $queryBuilder->andWhere(sprintf('p.price %s :%s', $operator, $placeholder));
                } else {
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNotNull('p.parent'),
                                sprintf('p.price %s :%s', $operator, $placeholder)
                            ),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNull('p.parent'),
                                $queryBuilder->expr()->eq('p.variationCount', 0),
                                sprintf('p.price %s :%s', $operator, $placeholder)
                            )
                        )
                    );
                }
                $parameters[$placeholder] = $value;
            } elseif ($key == 'new') {
                $placeholder = 'newDateTimeQuery';
                $dateTimeQuery = new \DateTime();
                try {
                    if ($this->storeSettings['new_products_date_filter']) {
                        $dateTimeQuery->modify($this->storeSettings['new_products_date_filter']);
                    }
                } catch (\Exception $e) {
                    $dateTimeQuery->modify('-3 months');
                }
                
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->gte('p.createdAt', ':' . $placeholder))
                ;
                $parameters[$placeholder] = $dateTimeQuery->format('Y-m-d');
            } elseif ($key == 'sale') {
                $queryBuilder->join('p.discounts', 'd');

                $queryBuilder
                    ->andWhere('d.enabled = true')
                    ->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->lte('d.startDate', ':discountStartDate'),
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->isNotNull('d.endDate'), 
                                $queryBuilder->expr()->gte('d.endDate', ':discountEndDate')
                            )
                        )
                    )
                ;

                $parameters['discountStartDate'] = $currentDateTime;
                $parameters['discountEndDate'] = $currentDateTime;
            } else {
                if (stripos($key, '.') === false) {
                    $key = sprintf('%s.%s', 'p', $key);
                }
                $placeholder = str_replace('.', '__', $key);

                $queryBuilder
                    ->andWhere(sprintf('%s %s :%s', $key, $operator, $placeholder))
                ;
                $parameters[$placeholder] = $value;
            }
        }

        // Apply sorting
        if (empty($orderBy)) {
            $orderBy = array();
        }

        $queryBuilder
            ->addOrderBy('p.position', 'ASC')
        ;
        foreach ($orderBy as $sort => $order) {
            if (stripos($sort, '.') === false) {
                $sort = sprintf('%s.%s', 'p', $sort);
            }
            $queryBuilder
                ->addOrderBy($sort, $order)
            ;
        }

        // Apply grouping
        if ($hasAttributeCriteria) {
            $queryBuilder
                ->andWhere(
                    $queryBuilder->expr()->isNotNull('p.parent')
                )
            ;
            $queryBuilder->addGroupBy('p.parent');
        } else {
            $queryBuilder->addSelect('IFELSE(p.parent IS NULL, p.id, p.parent) AS HIDDEN product_group');
            $queryBuilder->addGroupBy('product_group');
        }

        $queryBuilder
			->setParameters($parameters)
        ;
        
        // dump($parameters); dump($queryBuilder->getQuery()->getSql()); die();

        $result = $queryBuilder->getQuery()->getResult();

        if (is_array($remainingStocks)) {
            $productIds = array();
            foreach ($result as $product) {
                $productIds[] = $product->getId();
                $variations = $product->getVariations();
                if ($product->getParent()) {
                    $variations = $product->getParent()->getVariations();
                }
                foreach ($variations as $variation) {
                    $productIds[] = $variation->getId();
                }
            }
            if ($remainingStocksCheck = $this->countRemainingStocks($productIds, $activeOrder)) {
                $remainingStocks = $remainingStocksCheck;
            }
        }
        // dump($remainingStocks);die();

        return $result;
    }

    public function findProductBy(array $criteria = array(), array $orderBy = null, $excludedIds = array(), $limit, $offset = 0, &$remainingStocks = null, $activeOrder = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('p')
            ->from($this->getClass(), 'p')
        ;

        $currentDateTime = date("Y-m-d H:i:s");
        $parameters = array(
            'enabled' => true,
            'publicationStartDate' => $currentDateTime,
            'publicationEndDate' => $currentDateTime,
        );
        
        // Check for products availability by enabled and publication
        $queryBuilder
            ->where('p.enabled = :enabled')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->lte('p.publicationStartDate', ':publicationStartDate'),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->isNotNull('p.publicationEndDate'), 
                        $queryBuilder->expr()->gte('p.publicationEndDate', ':publicationEndDate')
                    )
                )
            )
        ;

        if (!empty($criteria['categories'])) {
            $queryBuilder->join('p.categories', 'c');

            $queryBuilder->andWhere(
                $queryBuilder->expr()->in('c.id', ':categories')
            );
            $parameters['categories'] = $criteria['categories'];
        }

        if (!empty($criteria['isFeatured'])) {
            $queryBuilder->andWhere('p.featured = true');
        }

        if ($excludedIds) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->notIn('p.id', ':excludedIds')
            );
            $parameters['excludedIds'] = $excludedIds;
        }

        if (!empty($criteria['isFeatured'])) {
            $orderBy['p.featured'] = 'DESC';
        }

        // Apply sorting
        if (empty($orderBy)) {
            $orderBy = array('p.position' => 'ASC');
        } else {
            $orderBy['p.position'] = 'ASC';
        }
        
        foreach ($orderBy as $sort => $order) {
            if (stripos($sort, '.') === false) {
                $sort = sprintf('%s.%s', 'p', $sort);
            }
            $queryBuilder
                ->addOrderBy($sort, $order)
            ;
        }

        // Apply grouping
        $queryBuilder->addSelect('IFELSE(p.parent IS NULL, p.id, p.parent) AS HIDDEN product_group');
        $queryBuilder->addGroupBy('product_group');
        $queryBuilder
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
        ;

        $queryBuilder
			->setParameters($parameters)
        ;
        
        // dump($parameters); dump($queryBuilder->getQuery()->getSql()); die();

        $result = $queryBuilder->getQuery()->getResult();

        if (is_array($remainingStocks)) {
            $productIds = array();
            foreach ($result as $product) {
                $productIds[] = $product->getId();
                $variations = $product->getVariations();
                if ($product->getParent()) {
                    $variations = $product->getParent()->getVariations();
                }
                foreach ($variations as $variation) {
                    $productIds[] = $variation->getId();
                }
            }
            if ($remainingStocksCheck = $this->countRemainingStocks($productIds, $activeOrder)) {
                $remainingStocks = $remainingStocksCheck;
            }
        }

        return $result;
    }

    public function countRemainingStocks($products = array(), $activeOrder = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        if (!is_array($products)) {
            $products = array($products);
        }

        foreach ($products as $key => $product) {
            if ($product instanceof Product) {
                $products[$key] = $product->getId();
            }
        }
        
        $orderStatuses = array(
            Order::STATUS_NEW,
            Order::STATUS_IN_CART,
            Order::STATUS_IN_CHECKOUT,
            Order::STATUS_PENDING,
            Order::STATUS_IN_PROGRESS,
        );

        // $selectRemainingStocksQuery = " ( s.value - SUM( IFNULL( l.quantity, 0 ) ) ) AS remaining_stocks";

        $selectRemainingStocksQuery = $this->getEntityManager()->createQueryBuilder();
        $selectRemainingStocksQuery
            ->select("SUM(ll.quantity)")
            ->from("AppBundle:Store\OrderLineItem", 'll')
            ->join('ll.order', 'oo')
        ;

        $selectRemainingStocksQuery
            ->where('ll.product = p.id')
            ->andWhere($selectRemainingStocksQuery->expr()->in('oo.status', ':orderStatuses'))
            ->groupBy('ll.product')
        ;

        $queryBuilder
            ->select('p.id')
            // ->addSelect($selectRemainingStocksQuery)
            ->addSelect(" ( s.value - IFNULL( ( " . $selectRemainingStocksQuery->getDql() . " ), 0 ) ) AS remaining_stocks")
            ->from($this->getClass(), 'p')
        ;

        if ($activeOrder) {
            $queryBuilderSubquery1 = $this->getEntityManager()->createQueryBuilder();
            $queryBuilderSubquery1
                ->select("SUM(la.quantity)")
                ->from("AppBundle:Store\OrderLineItem", 'la')
                ->where('la.product = p.id')
                ->andWhere('la.order = :activeOrder')
                ->groupBy('la.product')
            ;

            $queryBuilder->addSelect("IFNULL( ( " . $queryBuilderSubquery1->getDql() . " ), 0 ) AS reserved_stocks");
            $queryBuilder->setParameter('activeOrder', $activeOrder);
        }

        $queryBuilder->join('p.stock', 's');
        // $queryBuilder->leftJoin('p.lineItems', 'l');
        // $queryBuilder->leftJoin('l.order', 'o');

        $queryBuilder
            ->where($queryBuilder->expr()->in('s.product', ':products'))
            // ->andWhere(
            //     $queryBuilder->expr()->orX(
            //         $queryBuilder->expr()->in('o.status', ':orderStatuses'),
            //         $queryBuilder->expr()->isNull('o.status')
            //     )
            // )
        ;

        $queryBuilder->groupBy('s.product');

        $queryBuilder->setParameter('products', $products);
        $queryBuilder->setParameter('orderStatuses', $orderStatuses);

        // dump($queryBuilder->getQuery()->getSql());die();
        $result = $queryBuilder->getQuery()->getResult();


        if ($result) {
            $result = array_column($result, null, 'id');
            array_walk($result, array($this, 'computeRemainingStocks'));
            return $result;
        }

        return null;

        // $sql = "
        //     SELECT p.id, " . $selectRemainingStocksQuery . " AS remaining_stocks
        //     FROM AppBundle:Store\ProductStock s
        //     JOIN s.product p
        //     LEFT JOIN p.lineItems l
        //     LEFT JOIN l.order o 
        //     WHERE s.product IN (:products)
        //     AND (
        //         o.status IN (:orderStatuses)
        //         OR o.status IS NULL
        //     )
        //     GROUP BY s.product
        // ";

        // $query = $this->getEntityManager()->createQuery($sql);
    }

    public function computeRemainingStocks(&$remainingStock, $key)
    {
        if (isset($remainingStock['reserved_stocks'])) {
            $remainingStock = array( 
                'remaining_stocks' => (int) $remainingStock['remaining_stocks'],
                'reserved_stocks' => (int) $remainingStock['reserved_stocks'],
                'less_reserved' => (int) $remainingStock['remaining_stocks'] + (int) $remainingStock['reserved_stocks'],
            );
        } else {
            $remainingStock = (int) $remainingStock['remaining_stocks'];
        }
    }

    public function setStoreSettings($storeSettings)
    {
        $this->storeSettings = $storeSettings;
    }

    // public function _findProductBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    // {
    //     $queryBuilder = $this->getEntityManager()->createQueryBuilder();

    //     $queryBuilder
    //         ->select('p')
    //         ->from($this->getClass(), 'p')
    //     ;
        
    //     $hasAttributeCriteria = false;
    //     if (array_intersect(array_keys($criteria), array('colors', 'size'))) {
    //         $hasAttributeCriteria = true;
    //     }

    //     // Apply filters
    //     $currentDateTime = date("Y-m-d H:i:s");
    //     $parameters = array(
    //         'enabled' => true,
    //         'publicationStartDate' => $currentDateTime,
    //         'publicationEndDate' => $currentDateTime,
    //     );

	// 	$queryBuilder
    //         ->where('p.enabled = :enabled')
    //         ->andWhere(
    //             $queryBuilder->expr()->orX(
    //                 $queryBuilder->expr()->lte('p.publicationStartDate', ':publicationStartDate'),
    //                 $queryBuilder->expr()->andX(
    //                     $queryBuilder->expr()->isNotNull('p.publicationEndDate'), 
    //                     $queryBuilder->expr()->gte('p.publicationEndDate', ':publicationEndDate')
    //                 )
    //             )
    //         )
    //     ;

    //     foreach ($criteria as $key => $value) {
    //         $operator = ' = ';
    //         if (is_array($value) && isset($value['value'])) {
    //             $operator = $value['operator'];
    //             $value = $value['value'];
    //         }

    //         $placeholder = str_replace('.', '__', $key);

    //         if ($key == 'colors') {
    //             // $queryBuilder->join('p.color', 'c');
    //             $colors = $value;
    //             $value = array();
    //             foreach( $colors as $color ) {
    //                 $value[] = $color->getId();
    //             }
    //             $queryBuilder
    //                 ->andWhere(
    //                     $queryBuilder->expr()->in('p.color', ':colors')
    //                 )
    //             ;
    //             $parameters[$placeholder] = $value;
    //         } elseif ($key == 'categories') {
    //             $queryBuilder->join('p.categories', 'c');

    //             $categories = $value;
    //             $value = array();
    //             foreach( $categories as $category ) {
    //                 $value[] = $category->getId();
    //             }
    //             $queryBuilder
    //                 ->andWhere(
    //                     $queryBuilder->expr()->in('c.id', ':categories')
    //                 )
    //             ;
    //             $parameters[$placeholder] = $value;
    //         } elseif ($key == 'brand') {
    //             if ($hasAttributeCriteria) {
    //                 $queryBuilder->andWhere('p.brand = :brand');
    //             } else {
    //                 $brandQueryBuilder = $this->getEntityManager()->createQueryBuilder();
    //                 $brandQueryBuilder
    //                     ->select('IDENTITY(p_brand.parent)')
    //                     ->from($this->getClass(), 'p_brand')
    //                     ->where('p_brand.brand = :brand')
    //                     ->andWhere('IDENTITY(p_brand.parent) = p.id')
    //                 ;
                    
    //                 $queryBuilder->andWhere(
    //                     $queryBuilder->expr()->orX(
    //                         // $queryBuilder->expr()->eq('p.brand', ':brand'),
    //                         $queryBuilder->expr()->in('p.id', $brandQueryBuilder->getDql())
    //                     )
    //                 );
    //             }
                
    //             $parameters[$placeholder] = $value;
    //         } elseif ($key == 'theme') {
    //             if ($hasAttributeCriteria) {
    //                 $queryBuilder->andWhere('p.theme = :theme');
    //             } else {
    //                 $themeQueryBuilder = $this->getEntityManager()->createQueryBuilder();
    //                 $themeQueryBuilder
    //                     ->select('IDENTITY(p_theme.parent)')
    //                     ->from($this->getClass(), 'p_theme')
    //                     ->where('p_theme.theme = :theme')
    //                     ->andWhere('IDENTITY(p_theme.parent) = p.id')
    //                 ;

    //                 $queryBuilder->andWhere(
    //                     $queryBuilder->expr()->orX(
    //                         // $queryBuilder->expr()->eq('p.theme', ':theme'),
    //                         $queryBuilder->expr()->in('p.id', $themeQueryBuilder->getDql())
    //                     )
    //                 );
    //             }

    //             $parameters[$placeholder] = $value;
    //         } elseif ($key == 'price') {
    //             if ($hasAttributeCriteria) {
    //                 $queryBuilder->andWhere(sprintf('p.price %s :%s', $operator, $placeholder));
    //             } else {
    //                 $priceQueryBuilder = $this->getEntityManager()->createQueryBuilder();
    //                 $priceQueryBuilder
    //                     ->select('IDENTITY(p_price.parent)')
    //                     ->from($this->getClass(), 'p_price')
    //                     ->where('IDENTITY(p_price.parent) = p.id')
    //                     ->andWhere(sprintf('p.price %s :%s', $operator, $placeholder))
    //                 ;

    //                 $queryBuilder->andWhere(
    //                     $queryBuilder->expr()->orX(
    //                         // sprintf('p.price %s :%s', $operator, $placeholder),
    //                         $queryBuilder->expr()->in('p.id', $priceQueryBuilder->getDql())
    //                     )
    //                 );
    //             }
    //             $parameters[$placeholder] = $value;
    //         } else {
    //             if (stripos($key, '.') === false) {
    //                 $key = sprintf('%s.%s', 'p', $key);
    //             }
    //             $placeholder = str_replace('.', '__', $key);

    //             $queryBuilder
    //                 ->andWhere(sprintf('%s %s :%s', $key, $operator, $placeholder))
    //             ;
    //             $parameters[$placeholder] = $value;
    //         }
    //     }

    //     // Apply sorting
    //     if (empty($orderBy)) {
    //         $orderBy = array();
    //     }

    //     foreach ($orderBy as $sort => $order) {
    //         if (stripos($sort, '.') === false) {
    //             $sort = sprintf('%s.%s', 'p', $sort);
    //         }
    //         $queryBuilder
    //             ->addOrderBy($sort, $order)
    //         ;
    //     }
    //     $queryBuilder
    //         ->addOrderBy('p.position', 'ASC')
    //     ;

    //     // Apply grouping
    //     if ($hasAttributeCriteria) {
    //         // $queryBuilder->addSelect($queryBuilder->exp()->ifelse('p.parent IS NULL', 'p.id', 'p.parent'));
    //         $queryBuilder->addSelect('IFELSE(p.parent IS NULL, p.id, p.parent) AS HIDDEN product_group');
    //         $queryBuilder
    //             ->andWhere(
    //                 $queryBuilder->expr()->isNotNull('p.parent')
    //             )
    //         ;
    //         // $queryBuilder->addGroupBy($queryBuilder->expr()->ifElse('p.parent IS NULL', 'p.id', 'p.parent'));
    //         $queryBuilder->addGroupBy('p.parent');
    //     } else {
    //         $enbledChildQueryBuilder = $this->getEntityManager()->createQueryBuilder();
    //         $enbledChildQueryBuilder
    //             ->select('IDENTITY(p_enabled_child.parent)')
    //             ->from($this->getClass(), 'p_enabled_child')
    //             ->where('IDENTITY(p_enabled_child.parent) = p.id')
    //             ->andWhere(
    //                 $enbledChildQueryBuilder->expr()->eq('p_enabled_child.enabled', true)
    //             )
    //         ;

    //         $queryBuilder->andWhere(
    //             $queryBuilder->expr()->orX(
    //                 $queryBuilder->expr()->in('p.id', $enbledChildQueryBuilder->getDql()),
    //                 $queryBuilder->expr()->eq('p.variationCount', 0)
    //             )
    //         );

    //         $queryBuilder
    //             ->andWhere(
    //                 $queryBuilder->expr()->isNull('p.parent')
    //             )
    //         ;
    //     }
        
	// 	$queryBuilder
	// 		->setParameters($parameters)
    //     ;
        
    //     // $queryBuilder->addSelect($queryBuilder->expr()->max('p.price'));

    //     // dump($parameters); dump($queryBuilder->getQuery()->getSql()); die();

    //     return $queryBuilder->getQuery()->getResult();
    // }
}
