<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;

/**
 * ProductStockManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class ProductStockManager extends BaseEntityManager
{
    public function updateProductStock($product, $stockUpdate, $decrement = false)
    {
        $operator = $decrement ? '-' : '+';
        $query = $this->getEntityManager()->createQuery(
            'UPDATE AppBundle:Store\ProductStock s
            SET s.value = s.value ' . $operator . ' :stockUpdate
            WHERE s.product = :product'
        )
        ->setParameter('product', $product->getId())
        ->setParameter('stockUpdate', $stockUpdate);

        return $query->getResult();
    }
}
