<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;
use AppBundle\Entity\Store\Order;
use FOS\UserBundle\Model\UserInterface;

/**
 * OrderManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class OrderManager extends BaseEntityManager
{
    private $session;
    
    private $authorizationChecker;

    private $tokenStorage;

    private $requestStack;

    private $request;

    private $storeOrderService;

    private $orderLineItemManager;

    private $productStocksManager;

    private $storeSettings;
    
    public function getUserActiveOrder($criteria = array())
    {
        $orderStatuses = array(
            Order::STATUS_IN_CART,
            // Order::STATUS_IN_CHECKOUT,
            Order::STATUS_EXPIRED
        );

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('o')
			->from($this->getClass(), 'o')
		;

		$queryBuilder
			->where($queryBuilder->expr()->in('o.status', $orderStatuses))
			->orderBy('o.createdAt', 'DESC')
        ;

        if (!isset($criteria['user']) && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $criteria['user'] = $this->tokenStorage->getToken()->getUser();
        }
        if (empty($criteria['uniqid'])) {
            $cartUniqId = $this->storeOrderService->getCartUniqueID();
            $criteria['uniqid'] = $cartUniqId;
        }

        if (empty($criteria['user']) && empty($criteria['uniqid'])) {
            // @todo: Log
            return null;
        }
        
        if (isset($criteria['user']) && $criteria['user'] instanceof UserInterface && !empty($criteria['uniqid'])) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('o.user', ':user'),
                    $queryBuilder->expr()->eq('o.uniqid', ':uniqid'),
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('o.user', ':user'),
                        $queryBuilder->expr()->eq('o.uniqid', ':uniqid')
                    )
                )
            );

            $queryBuilder->setParameter('user', $criteria['user']);
            $queryBuilder->setParameter('uniqid', $criteria['uniqid']);
        } else {
            if (isset($criteria['user']) && $criteria['user'] instanceof UserInterface) {
                $queryBuilder->andWhere('o.user = :user');
                $queryBuilder->setParameter('user', $criteria['user']);
            } 
            if (!empty($criteria['uniqid'])) {
                $queryBuilder->andWhere('o.uniqid = :uniqid');
                $queryBuilder->setParameter('uniqid', $criteria['uniqid']);
            }
        }
        
        $queryBuilder
            ->setFirstResult(0)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
		// return $queryBuilder->getQuery()->getResult();
    }

    public function getUserOrderOnCheckoutSuccess($criteria = array())
    {
        if (empty($criteria['id'])) {
            // @todo: Log
            throw \Exception('Paramter order ID is required.');
        }

        $orderStatuses = array(
            Order::STATUS_IN_CHECKOUT,
            // Order::STATUS_IN_CHECKOUT,
            Order::STATUS_PENDING
        );

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('o')
			->from($this->getClass(), 'o')
		;

		$queryBuilder
            ->where($queryBuilder->expr()->in('o.status', $orderStatuses))
            ->andWhere('o.id = :id')
			->orderBy('o.createdAt', 'DESC')
        ;

        if (!isset($criteria['user']) && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $criteria['user'] = $this->tokenStorage->getToken()->getUser();
        }
        
        if (isset($criteria['user']) && $criteria['user'] instanceof UserInterface) {
            $queryBuilder->andWhere('o.user = :user');
            $queryBuilder->setParameter('user', $criteria['user']);
        } 

        $queryBuilder->setParameter('id', $criteria['id']);
        
        $queryBuilder
            ->setFirstResult(0)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function updateOrderTotalPrice($order, $flush = true)
    {
        // $totalPrice = $order->getLineItemsPriceTotals();
        // $discountAndExtraFees = $this->computeDiscountAndExtraFees($order, $totalPrice);
        $totals = $this->storeOrderService->getTotals($order);

        $order->setShippingFee($totals['shippingFee']);
        $order->setGiftWrapPrice($totals['giftWrapPrice']);
        $order->setPromoCodeValue($totals['discountTotal']);
        $order->setTotalPrice($totals['lineItemsFinalTotalPrice']);
        $order->setFinalTotalPrice($totals['finalTotalPrice']);

        //dump($totals);die();

        if ($flush) {
            $entityManager = $this->getEntityManager();
            $entityManager->persist($order);
            $entityManager->flush();
        }
    }

    public function saveOrderWithLineItem($order, $lineItems = array(), $action = 'new')
    {
        $entityManager = $this->getEntityManager();
        $entityManager->getConnection()->beginTransaction();
        try {
            foreach ($lineItems as $lineItem) {
                $order->addLineItem($lineItem);
            }
            // Update total price
            foreach ($order->getLineItems() as $lineItem) {
                $this->orderLineItemManager->updatePriceData($lineItem, false);
            }
            $this->updateOrderTotalPrice($order, false);

            $entityManager->persist($order);
            $entityManager->flush();

            $entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            // @todo: Log error
            $entityManager->getConnection()->rollBack();
            throw $e;
        }
    }

    public function updateStocksWithOrder($order)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->getConnection()->beginTransaction();

        try {
            foreach ($order->getLineItems() as $lineItem) {
                $this->productStocksManager->updateProductStock($lineItem->getProduct(), $lineItem->getQuantity(), true);
            }
            $entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            // @todo: Log error
            $entityManager->getConnection()->rollBack();
            throw $e;
        }
    }

    public function deleteLineItem($lineItem, $order)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->getConnection()->beginTransaction();
        try {
            $this->orderLineItemManager->delete($lineItem);
            // Update total price
            foreach ($order->getLineItems() as $lineItem) {
                $this->orderLineItemManager->updatePriceData($lineItem, false);
            }
            $this->updateOrderTotalPrice($order, false);
            
            $entityManager->persist($order);
            $entityManager->flush();

            $entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            // @todo: Log error
            $entityManager->getConnection()->rollBack();
            throw $e;
        }
    }

    public function saveOrder($order, $computeTotalPrice = true)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->getConnection()->beginTransaction();
        try {
            // Update total price
            if ($computeTotalPrice) {
                foreach ($order->getLineItems() as $lineItem) {
                    $this->orderLineItemManager->updatePriceData($lineItem, false);
                }
                $this->updateOrderTotalPrice($order, false);
            }
            
            $entityManager->persist($order);
            $entityManager->flush();

            $entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            // @todo: Log error
            $entityManager->getConnection()->rollBack();
            throw $e;
        }
    }

    public function createNewOrder($user = null, $sessionId = null, $uniqid = null, $ipAddress = null, $status = Order::STATUS_IN_CART)
    {
        $order = $this->create();
        $order->setUser($user);
        // $order->setOrderNumber(strtotime("now"));
        $order->setStatus($status);
        $order->setSessionId($sessionId);
        $order->setUniqid($uniqid);
        $order->setIpAddress($ipAddress);

        return $order;
    }

    public function getActiveOrder()
    {

    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function setTokenStorage($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function setStoreOrderService($storeOrderService)
    {
        $this->storeOrderService = $storeOrderService;
    }

    public function setRequestStack($requestStack)
    {
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function setOrderLineitemManager($orderLineItemManager)
    {
        $this->orderLineItemManager = $orderLineItemManager;
    }

    public function setProductStockManager($productStocksManager)
    {
        $this->productStocksManager = $productStocksManager;
    }

    public function setStoreSettings($storeSettings)
    {
        $this->storeSettings = $storeSettings;
    }
}
