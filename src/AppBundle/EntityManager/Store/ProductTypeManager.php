<?php 

namespace AppBundle\EntityManager\Store;

use Sonata\CoreBundle\Model\BaseEntityManager;

/**
 * ProductTypeManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class ProductTypeManager extends BaseEntityManager
{
	public function getDefault()
	{
		return $this->getRepository()->findOneBy(array('isDefault' => true), array('updatedAt' => 'DESC'));
	}
}
