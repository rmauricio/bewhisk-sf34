<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\ProductFilterType;
use AppBundle\Entity\Model\ProductFilter;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Form\AddToCartType;

class ProductsController extends Controller
{
    public function indexAction(Request $request, $category = null)
    {
        $productManager = $this->get('app.entity_manager.store.product');
        $orderManager = $this->get('app.entity_manager.store.order');
        $orderLineItemManager = $this->get('app.entity_manager.store.order_line_item');

        $products = array();
        $criteria = array();

        $isNew = $request->query->has('new') ? (int) $request->query->get('new') : 0;
        $isSale = $request->query->has('sale') ? (int) $request->query->get('sale') : 0;
        $currentPage = $request->query->get('page');
        if (empty($currentPage)) {
            $currentPage = 1;
        }

        $productFilter = new ProductFilter();
        $form = $this->createForm(ProductFilterType::class, $productFilter, array(
			'method' => 'GET',
			'attr' => array('novalidate' => true),
			// 'action' => $this->generateUrl('app_store_products_list')
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Submit'));

        $form->handleRequest($request);

        $sort = $productFilter->getSort();
        $sorting = ProductFilter::getSortOptionSorting($sort);
        $limit = 4;
        $offset = $currentPage - 1;

        if ($category) {
            $criteria['categories'] = array( $category );
            /*if ($category->getParent()) {
                $criteria['categories'][] = $category->getParent();
            }*/
            $children = $category->getChildren();
            if (count($children)) {
                foreach ($children as $childCategory) {
                    $criteria['categories'][] = $childCategory;
                }
            }
        }
        $pageTitle = 'Shop';
        if ($isNew) {
            $criteria['new'] = true;
            $pageTitle = "What's New";
        }
        if ($isSale) {
            $criteria['sale'] = true;
            $pageTitle = "Sale";
        }

        // process form
        if ($form->isSubmitted() && $form->isValid()) {
            $productFilter = $form->getData();

            if ($brand = $productFilter->getBrand()) {
                $criteria['brand'] = $brand;
            }
            if ($price = $productFilter->getPrice()) {
                $criteria['price'] = ProductFilter::getPriceOptionFilters($price);
            }
            if ($categories = $productFilter->getColor()) {
                if (count($categories)) {
                    $colorManager = $this->get('app.entity_manager.store.color');
                    $colors = $colorManager->findByCategries($categories);

                    $criteria['colors'] = array(
                        'value' => $colors,
                        'operator' => 'IN',
                    );
                }
            }
            if ($size = $productFilter->getSize()) {
                $criteria['size'] = $size;
            }
            if ($theme = $productFilter->getTheme()) {
                $criteria['theme'] = $theme;
            }
        }

        $user = null;
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
        }
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
        }

        $activeOrder = $orderManager->getUserActiveOrder(array('user' => $user));
        $activeOrderLineItems = array();
        if ($activeOrder) {
            $user = null;
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $this->getUser();
            }
            if ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
                $activeOrder = null;
            } else {
                $activeOrderLineItems = $activeOrder->getProductIndexedLineItems();
            }
        }
        
        $stocks = array();
        $productManager = $this->get('app.entity_manager.store.product');
        $products = $productManager->findProductCatalogBy($criteria, $sorting, $limit, $offset, $stocks, $activeOrder);
        $adapter = new ArrayAdapter($products);
        $productsPager = new Pagerfanta($adapter);
        $productsPager
            ->setMaxPerPage($limit)
            ->setCurrentPage($currentPage);

        $addToCartForms = array();

        foreach ($productsPager->getCurrentPageResults() as $productItem) {
            $productParent = $productItem;
            $variations = $productItem->getEnabledVariations();
            if (!$variations) {
                $variations = [];
            }
            if ($productItem->getParent()) {
                $productParent = $productItem->getParent();
                $variations = $productParent->getEnabledVariations(); 
            }

            $lineItem1 = $orderLineItemManager->create();
            $lineItem1->setProduct($productItem);
            $lineItem1->setParentProduct($productItem->getParent());
            $lineItem1->setColor($productItem->getColor());
            $lineItem1->setSize($productItem->getSize());
            
            $itemProductId = $productItem->getId();
            if (isset($activeOrderLineItems[$itemProductId])) {
                $quantity = $activeOrderLineItems[$itemProductId]->getQuantity();
                $lineItem1->setQuantity($quantity);
            }

            $addToCartForms[$productItem->getId()] = $this->createForm(AddToCartType::class, $lineItem1, array(
                'method' => 'POST',
                'attr' => array('novalidate' => true),
                'simple' => true,
            ))->createView();

            foreach ($variations as $variation) {
                $lineItem2 = $orderLineItemManager->create();
                $lineItem2->setProduct($variation);
                $lineItem2->setParentProduct($variation->getParent());
                $lineItem2->setColor($variation->getColor());
                $lineItem2->setSize($variation->getSize());

                $itemProductId = $variation->getId();
                if (isset($activeOrderLineItems[$itemProductId])) {
                    $quantity = $activeOrderLineItems[$itemProductId]->getQuantity();
                    $lineItem2->setQuantity($quantity);
                }

                $addToCartForms[$variation->getId()] = $this->createForm(AddToCartType::class, $lineItem2, array(
                    'method' => 'POST',
                    'attr' => array('novalidate' => true),
                    'simple' => true,
                ))->createView();
            }
        }

        $template = 'AppBundle:Theme\Store\Default\Products:index.html.twig';

        if ($request->isXmlHttpRequest()) {
            $formHtml = $this->renderView('AppBundle:Theme\Store\Default\Products:product_filter_form.html.twig', array(
                'category' => $category,
                'productsPager' => $productsPager,
                'form' => $form->createView(),
            ));
            $resultsListHtml = $this->renderView('AppBundle:Theme\Store\Default\Products:list.html.twig', array(
                'category' => $category,
                'productsPager' => $productsPager,
                'stocks' => $stocks,
                'addToCartForms' => $addToCartForms,
            ));
    
            $response = new JsonResponse();
            $response->setData(array(
                'form_html' => $formHtml,
                'results_list_html' => $resultsListHtml,
            ));

            return $response;
        }

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            dump($activeOrder, $stocks);
        }

        return $this->render($template, array(
            'navigation' => $this->_getNavigationView(
                array(
                    'productFilter' => $productFilter,
                    'form' => $form->createView(),
                )
            ),
            'category' => $category,
            'productsPager' => $productsPager,
            'form' => $form->createView(),
            'stocks' => $stocks,
            'addToCartForms' => $addToCartForms,
            'pageTitle' => $pageTitle,
        ));
    }

    public function categoryAction(Request $request, $parentCategorySlug, $categorySlug)
    {
        $categoryManager = $this->get('sonata.classification.manager.category');

        $category = $categoryManager->findCategoryByParentSlug($parentCategorySlug, $categorySlug);

        if (!$category) {
            throw $this->createNotFoundException('Category not found.');
        }

        return $this->indexAction($request, $category);
    }

    private function _getNavigationView($params = array())
    {
        $categoryManager = $this->get('sonata.classification.manager.category');
        $colorManager = $this->get('app.entity_manager.store.color');
        $sizeManager = $this->get('app.entity_manager.store.size');

        // $brandOptions = $categoryManager->getBrandCategories();
        // $themeOptions = $categoryManager->getThemeCategories();
        $priceOptions = ProductFilter::getPriceOptions();
        $colorOptions = $categoryManager->getColorCategories();
        $sizeOptions = $sizeManager->findBy(array('enabled' => true), array('position' => 'ASC', 'name' => 'ASC'));

        $params = array_merge($params, array(
            //'brandOptions' => $brandOptions,
            //'themeOptions' => $themeOptions,
            'priceOptions' => $priceOptions,
            'colorOptions' => $colorOptions,
            'sizeOptions' => $sizeOptions,
        ));

        return $this->renderView('AppBundle:Theme\Store\Default\Products:navigation.html.twig', $params);
    }
}
