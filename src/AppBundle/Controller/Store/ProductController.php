<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\AddToCartType;
use AppBundle\Entity\Model\ProductFilter;
use AppBundle\Entity\Model\AddToCart;
use AppBundle\Event\Store\OrderCreateLineItemEvent;

class ProductController extends Controller
{
    public function viewAction(Request $request, $slug)
    {
        $productManager = $this->get('app.entity_manager.store.product');
        $orderManager = $this->get('app.entity_manager.store.order');
        $orderLineItemManager = $this->get('app.entity_manager.store.order_line_item');

        $selectedProduct = $productManager->findOneProductBy(array('slug' => $slug));

        if (!$selectedProduct) {
            throw $this->createNotFoundException('The product does not exist');
        }

        $parentProduct = $selectedProduct;
        if ($selectedProduct->getParent()) {
            $parentProduct = $selectedProduct->getParent();
        }

        $enabledVariations = $parentProduct->getEnabledVariations();
        if (!$selectedProduct->getParent() && $enabledVariations) {
            $selectedProduct = $enabledVariations->first();
        }

        // $simpleAddToCart = $request->request->get('simple_add_to_cart');
        // if (isset($simpleAddToCart['product'])) {
        //     if ((int) $simpleAddToCart['product'] !== $selectedProduct->getId()) {
        //         // @todo: Log error
        //         $this->addFlash(
        //             'warning',
        //             'There was a problem encountered while adding a product on your cart.'
        //         );
        //         return $this->redirectToRoute('app_store_products_list');
        //     } 
        // }

        /*$selectedProductRemainingStocks = $productManager->countRemainingStocks($selectedProduct);
        if ($selectedProductRemainingStocks === null) {
            // @todo: Add logger
            throw new \Exception(sprintf('Error while checking stocks for product %s', $selectedProduct->getId()));
        } elseif ($selectedProductRemainingStocks <= 0) {
            $this->addFlash(
                'warning',
                'This product is currently out of stock.'
            );
        }*/

        $user = null;
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
        }
        
        $activeOrder = $orderManager->getUserActiveOrder(array('user' => $user));
        $activeOrderLineItems = array();
        if ($activeOrder) {
            $activeOrderLineItems = $activeOrder->getProductIndexedLineItems();
        }

        $orderLineItem = $orderLineItemManager->create();
        $orderLineItem->setProduct($selectedProduct);
        $orderLineItem->setParentProduct($selectedProduct->getParent());
        $orderLineItem->setColor($selectedProduct->getColor());
        $orderLineItem->setSize($selectedProduct->getSize());

        $itemProductId = $selectedProduct->getId();
        if (isset($activeOrderLineItems[$itemProductId])) {
            $quantity = $activeOrderLineItems[$itemProductId]->getQuantity();
            $orderLineItem->setQuantity($quantity);
        }

        $form = $this->createForm(AddToCartType::class, $orderLineItem, array(
			'method' => 'POST',
			'attr' => array('novalidate' => true),
        ));

        $variationsRef = array();
        $remainingStocks = array();

        // @todo: Refactor generation of variations reference
        if ($enabledVariations) {
            foreach($enabledVariations as $enabledVariation) {
                if ($enabledVariation->getColor()) {
                    if ($enabledVariation->getSize()) {
                        $variationsRef['color'][$enabledVariation->getColor()->getId()]['size'][] = $enabledVariation->getSize()->getId() . '__' . $enabledVariation->getSize()->getName();
                    }
                } 
                if ($enabledVariation->getSize()) {
                    if ($enabledVariation->getColor()) {
                        $variationsRef['size'][$enabledVariation->getSize()->getId()]['color'][] = $enabledVariation->getColor()->getId() . '__' . $enabledVariation->getColor()->getName();
                    }
                }
                // Reference for remaining stocks
                $variationId = $enabledVariation->getId();
                $remainingStocksCheck = $productManager->countRemainingStocks($enabledVariation, $activeOrder);
                $remainingStocks[$variationId] = $remainingStocksCheck[$variationId];
                $stock = $remainingStocks[$variationId];
                $hasReservedStock = false;
                if (is_array($stock)) {
                    if ($stock['reserved_stocks'] > 0) {
                        $hasReservedStock = true;
                    }
                    $stock = $stock['less_reserved'];
                }
                if (!$hasReservedStock && $stock <= 0) {
                    $form->add('submit_' . $variationId, SubmitType::class, array('label' => 'Out of stock', 'attr' => array(
                        'disabled' => true,
                        'class' => 'btn btn-default text-uppercase'
                    )));
                } else {
                    $form->add('submit_' . $enabledVariation->getId(), SubmitType::class, array('label' => 'Add to cart', 'attr' => array(
                        'class' => 'btn btn-black text-uppercase'
                    )));
                }
            }
        } else {
            // Reference for remaining stocks
            $selectedProductId = $selectedProduct->getId();
            $remainingStocksCheck = $productManager->countRemainingStocks($selectedProduct, $activeOrder);
            $remainingStocks[$selectedProductId] = $remainingStocksCheck[$selectedProductId];   
            $stock = $remainingStocks[$selectedProductId];
            $hasReservedStock = false;
            if (is_array($stock)) {
                if ($stock['reserved_stocks'] > 0) {
                    $hasReservedStock = true;
                }
                $stock = $stock['less_reserved'];
            }       
            if (!$hasReservedStock && $stock <= 0) {
                $form->add('submit', SubmitType::class, array('label' => 'Out of stock', 'attr' => array(
                    'disabled' => true,
                    'class' => 'btn btn-default text-uppercase'
                )));
            } else {
                $form->add('submit', SubmitType::class, array('label' => 'Add to cart', 'attr' => array(
                    'class' => 'btn btn-black text-uppercase'
                )));
            }
        }

        $form->handleRequest($request);
        
        // process form
        if ($form->isSubmitted() && $form->isValid()) {
            $orderLineItem = $form->getData();
            try {
                $event = new OrderCreateLineItemEvent($orderLineItem, $user);
                $this->get('event_dispatcher')->dispatch(OrderCreateLineItemEvent::NAME, $event);

                if ($event->getResponse()) {
                    return $event->getResponse();
                }

                if ($event->getException()) {
                    throw $event->getException();
                }
                $this->addFlash(
                    'success', 
                    sprintf('Successfully added product "<strong>%s</strong>" on your cart.', $orderLineItem->getProduct()->getName())
                );
                $formOptions = $form->getConfig()->getOptions();
                // if ($formOptions['simple']) {
                //     return $this->redirectToRoute('app_store_products_list');
                // }
            } catch (\Exception $e) {
                // @todo: Log error
                $this->addFlash(
                    'warning', 
                    'There was an error occured while adding a product on your cart. Please contact administrator.'
                );
                // Dump exception on dev environment
                if ($this->container->getParameter('kernel.environment') == 'dev') {
                    dump($e);
                }
            }
            if ($request->query->get('redirect')) {
                return $this->redirect($request->query->get('redirect'));
            } else {
                return $this->redirectToRoute('app_store_product_view', array('slug' => $slug));
            }
        }

        $productFilter = new ProductFilter();

        return $this->render('AppBundle:Theme\Store\Default\Product:view.html.twig', array(
            'navigation' => $this->_getNavigationView(
                array(
                    'productFilter' => $productFilter,
                )
            ),
            'form' => $form->createView(),
            'selectedProduct' => $selectedProduct,
            'parentProduct' => $parentProduct,
            'enabledVariations' => $enabledVariations,
            'variationsRef' => $variationsRef,
            'remainingStocks' => $remainingStocks,
        ));
    }

    private function _getNavigationView($params = array())
    {
        $categoryManager = $this->get('sonata.classification.manager.category');
        $colorManager = $this->get('app.entity_manager.store.color');
        $sizeManager = $this->get('app.entity_manager.store.size');

        $brandOptions = $categoryManager->getBrandCategories();
        $themeOptions = $categoryManager->getThemeCategories();
        $priceOptions = ProductFilter::getPriceOptions();
        $colorOptions = $colorManager->getParents();
        $sizeOptions = $sizeManager->findBy(array('enabled' => true), array('position' => 'ASC', 'name' => 'ASC'));

        $params = array_merge($params, array(
            'brandOptions' => $brandOptions,
            'themeOptions' => $themeOptions,
            'priceOptions' => $priceOptions,
            'colorOptions' => $colorOptions,
            'sizeOptions' => $sizeOptions,
            'hideFilterPanel' => true,
        ));

        return $this->renderView('AppBundle:Theme\Store\Default\Products:navigation.html.twig', $params);
    }
}
