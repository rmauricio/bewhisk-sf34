<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConfirmPaymentController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle:Theme\Store\Default\ConfirmPayment:index.html.twig', array(
        )); 
    }
}