<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class OrderHistoryController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:Theme\Store\Default\OrderHistory:index.html.twig', array());
    }
    public function viewAction($id)
    {
        return $this->render('AppBundle:Theme\Store\Default\OrderHistory:view.html.twig', array());
    }
}