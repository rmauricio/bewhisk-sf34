<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\CartType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Model\DeleteLineItem;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use AppBundle\Form\Store\ConfirmDeleteLineItemType;
use AppBundle\Entity\Store\Order;

class CartController extends Controller
{
    public function indexAction(Request $request)
    {
        $orderManager = $this->get('app.entity_manager.store.order');
        $orderLineItemManager = $this->get('app.entity_manager.store.order_line_item');
        $productManager = $this->get('app.entity_manager.store.product');
        $orderService = $this->get('app.service.store_order');

        $activeOrder = $orderManager->getUserActiveOrder();
        if ($activeOrder) {
            $user = null;
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $this->getUser();
            } elseif ($activeOrder->getUser()) {
                // @todo: Log access denied on someone's order
                $response = $this->redirectToRoute('app_store_products_list');
                $orderService->setForceGenerateCartUniqueIDFlag();
                return $response;
            }
            if ($activeOrder->getStatus() == Order::STATUS_EXPIRED) {
                $this->addFlash(
                    'warning', 
                    'Sorry your cart was already expired.'
                );
                $response = $this->redirectToRoute('app_store_products_list');
                $orderService->setForceGenerateCartUniqueIDFlag();
                return $response;
            } elseif ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
                // @todo: Log access denied on someone's order
                $response = $this->redirectToRoute('app_store_products_list');
                $orderService->setForceGenerateCartUniqueIDFlag();
                return $response;
            }
        }

        // $response = new Response();
        // $cookie = new Cookie('bw_test', 1, time()+3600);
        // $response->headers->setCookie($cookie);

        // Get cookie value
        // $request->cookies->get('bw_test');

        // dump($request->cookies->get('bw_cart_uniqid'));
        
        $form = $this->_cartForm($activeOrder);

        $form->handleRequest($request);
        
        // process form
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();

            try {
                $orderManager->saveOrder($order);
                if ($form->get('update')->isClicked()) {
                    $this->addFlash(
                        'success', 
                        'Your cart was successfully updated.'
                    );
                }
            } catch (\Exception $e) {
                $this->addFlash(
                    'warning', 
                    'There was a error occured while updateing your cart. Please contact administrator.'
                );
                // Dump exception on dev environment
                if ($this->container->getParameter('kernel.environment') == 'dev') {
                    dump($order);
                    dump($e);
                }
            }

            if ($form->get('checkout')->isClicked()) {
                return $this->redirectToRoute('app_store_order_checkout', array('cartId' => $activeOrder->getId()));
            }

            return $this->redirectToRoute('app_store_cart');
        }
        
        $removeItemConfirmForm = $this->_confirmDeleteItemForm();

        $orderTotals = array();
        if ($this->container->getParameter('kernel.environment') == 'dev' && $activeOrder) {
            $orderTotals = $orderService->getTotals($activeOrder);
            dump($activeOrder);
            dump($orderTotals);
        }

        return $this->render('AppBundle:Theme\Store\Default\Cart:index.html.twig', array(
            'activeOrder' => $activeOrder,
            'orderTotals' => $orderTotals,
            'form' => $form->createView(),
            'removeItemConfirmForm' => $removeItemConfirmForm->createView()
        ));
    }

    public function removeItemAction(Request $request, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createAccessDeniedException('Invalid request format.');
        }

        $orderManager = $this->get('app.entity_manager.store.order');
        $orderLineItemManager = $this->get('app.entity_manager.store.order_line_item');
        $orderService = $this->get('app.service.store_order');

        // @todo: Add handling for expired order
        $activeOrder = $this->get('app.entity_manager.store.order')->getUserActiveOrder();
        if (!$activeOrder) {
            throw new \Exception("Order not found.");
        } else {
            $user = null;
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $this->getUser();
            } elseif ($activeOrder->getUser()) {
                // @todo: Log access denied on someone's order
                throw $this->createAccessDeniedException("You are not allowed to access someone's order.");
            }
            if ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
                // @todo: Log error
                throw $this->createAccessDeniedException("You are not allowed to access someone's order.");
            }
        }

        $lineItem = $orderLineItemManager->findOneBy(array('id' => $id, 'order' => $activeOrder));
        if (!$lineItem) {
            throw new \Exception("Invalid line item.");
        }
        
        $cartFormHtml = "";
        $success = 0;

        $form = $this->_confirmDeleteItemForm();
        $form->handleRequest($request);
        
        // process form
        if ($form->isSubmitted()) {
            $deleteItemConfirm = $form->getData();
            $submittedItem = $deleteItemConfirm->getItem();
            $lineItemName = $lineItem->getProductName();

            if ($submittedItem->getId() !== $lineItem->getId()) {
                throw new \Exception("Invalid line item.");
            }

            if ($form->isValid()) {
                try {
                    $orderManager->deleteLineItem($submittedItem, $activeOrder);
                    $orderTotals = $orderService->getTotals($activeOrder);

                    $this->addFlash(
                        'success',
                        'Cart item <strong>"' . $lineItemName . '"</strong> was successfully removed.'
                    );
    
                    $cartForm = $this->_cartForm();
                    $cartFormHtml = $this->renderView('AppBundle:Theme\Store\Default\Cart:form.html.twig', array(
                        'activeOrder' => $activeOrder,
                        'orderTotals' => $orderTotals,
                        'form' => $cartForm->createView(),
                        'displayFlash' => true,
                    ));
    
                    $success = 1;
                } catch (\Exception $e) {
                    throw $e;
                    // Dump exception on dev environment
                    if ($this->container->getParameter('kernel.environment') == 'dev') {
                        dump($e);
                    }
                }
            }
        }
        
        $formHtml = $this->renderView('AppBundle:Theme\Store\Default\Cart:remove_item_form.html.twig', array(
            'form' => $form->createView(),
            'itemName' => $lineItem,
            'displayFlash' => true,
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'form_html' => $formHtml,
            'cart_form_html' => $cartFormHtml,
            'success' => $success,
        ));

        return $response;
    }
    
    private function _cartForm($activeOrder = null)
    {
        if (!$activeOrder) {
            $activeOrder = $this->get('app.entity_manager.store.order')->getUserActiveOrder();
        }

        $form = $this->createForm(CartType::class, $activeOrder, array(
			'method' => 'POST',
			'attr' => array('novalidate' => true),
        ));

        $form
            ->add('update', SubmitType::class, ['label' => 'Update Cart'])
            ->add('checkout', SubmitType::class, ['label' => 'Checkout'])
        ;

        return $form;
    }
    
    private function _confirmDeleteItemForm()
    {
        $deleteLineItem = new DeleteLineItem();
        $form = $this->createForm(ConfirmDeleteLineItemType::class, $deleteLineItem, array(
            'method' => 'POST',
			'attr' => array('novalidate' => true),
        ));

        return $form;
    }
}