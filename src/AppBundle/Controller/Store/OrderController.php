<?php

namespace AppBundle\Controller\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\Store\OrderCheckoutType;
use SM\Factory\Factory as SMFactory;
use AppBundle\Entity\Store\Order;
use AppBundle\Event\Store\OrderCheckoutFinishedEvent;

class OrderController extends Controller
{
    public function checkoutAction(Request $request, SMFactory $smFactory, $cartId)
    {
        $orderManager = $this->get('app.entity_manager.store.order');
        $orderLineItemManager = $this->get('app.entity_manager.store.order_line_item');
        $orderService = $this->get('app.service.store_order');

        $activeOrder = $orderManager->getUserActiveOrder();

        if (!$activeOrder) {
            // @todo: Log error
            return $this->redirectToRoute('app_store_cart');
        }
        if ($activeOrder->getId() != $cartId) {
            // @todo: Log error
            $this->addFlash(
                'warning', 
                "It seems that the order you are trying to checkout is not valid. Please try to click again the checkout button. If this error still persist please contact administrator."
            );
            return $this->redirectToRoute('app_store_cart');
        }
        $user = null;
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
        } elseif ($activeOrder->getUser()) {
            // @todo: Log access denied on someone's order
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        }
        if ($activeOrder->getStatus() == Order::STATUS_EXPIRED) {
            $this->addFlash(
                'warning', 
                'Sorry your cart was already expired.'
            );
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        } elseif ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
            // @todo: Log access denied on someone's order
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        }

        if ($activeOrder->getUser()) {
            // Add initial billing info if current user do not have
            if ($activeOrder->getUser()->getBillingInformation()->count()) {
                $billingInformation = $activeOrder->getUser()->getBillingInformation()[0];
                $region = $billingInformation->getCity() ? $billingInformation->getCity()->getRegion() : null;
                $activeOrder->setBillingFirstName($billingInformation->getFirstName());
                $activeOrder->setBillingLastName($billingInformation->getLastName());
                $activeOrder->setBillingEmail($billingInformation->getEmail());
                $activeOrder->setBillingAddress1($billingInformation->getAddress1());
                $activeOrder->setBillingAddress2($billingInformation->getAddress2());
                $activeOrder->setBillingPostalCode($billingInformation->getPostalCode());
                $activeOrder->setBillingContactNumber($billingInformation->getContactNumber());
                $activeOrder->setBillingCity($billingInformation->getCity());
                $activeOrder->setBillingRegion($region);
            } else {
                $activeOrder->setBillingFirstName($activeOrder->getUser()->getFirstname());
                $activeOrder->setBillingLastName($activeOrder->getUser()->getLastname());
                $activeOrder->setBillingEmail($activeOrder->getUser()->getEmail());
            }
            // Add initial shipping info if current user do not have
            if ($activeOrder->getUser()->getShippingInformation()->count()) {
                $shippingInformation = $activeOrder->getUser()->getShippingInformation()[0];
                $region = $shippingInformation->getCity() ? $shippingInformation->getCity()->getRegion() : null;
                $activeOrder->setShippingFirstName($shippingInformation->getFirstName());
                $activeOrder->setShippingLastName($shippingInformation->getLastName());
                $activeOrder->setShippingEmail($shippingInformation->getEmail());
                $activeOrder->setShippingAddress1($shippingInformation->getAddress1());
                $activeOrder->setShippingAddress2($shippingInformation->getAddress2());
                $activeOrder->setShippingPostalCode($shippingInformation->getPostalCode());
                $activeOrder->setShippingContactNumber($shippingInformation->getContactNumber());
                $activeOrder->setShippingCity($shippingInformation->getCity());
                $activeOrder->setShippingRegion($region);
            } else {
                $activeOrder->setShippingFirstName($activeOrder->getUser()->getFirstname());
                $activeOrder->setShippingLastName($activeOrder->getUser()->getLastname());
                $activeOrder->setShippingEmail($activeOrder->getUser()->getEmail());
            }
        }

        $form = $this->_checkoutForm($activeOrder);

        $form->handleRequest($request);
        
        // process form
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();

            if ($order->getShippingSameAsBilling()) {
                $order->setShippingFirstName($order->getBillingFirstName());
                $order->setShippingLastName($order->getBillingLastName());
                $order->setShippingEmail($order->getBillingEmail());
                $order->setShippingAddress1($order->getBillingAddress1());
                $order->setShippingAddress2($order->getBillingAddress2());
                $order->setShippingPostalCode($order->getBillingPostalCode());
                $order->setShippingContactNumber($order->getBillingContactNumber());
                $order->setShippingCity($order->getBillingCity());
                $order->setShippingRegion($order->getBillingCity()->getRegion());
            }

            $checkoutSuccess = false;
            $redirectRoute = 'app_store_order_success';
            $redirectRouteParams = array();
            $orderSM = $smFactory->get($order, 'default');
            // Check if a transition can be applied: returns true or false
            try {
                // Update total price before status update
                foreach ($order->getLineItems() as $lineItem) {
                    $orderLineItemManager->updatePriceData($lineItem, false);
                }
                $orderManager->updateOrderTotalPrice($order, false);

                if ($order->getPaymentMethod() == Order::PAYMENT_METHOD_BANK_TRANSFER) {
                    if (!$orderSM->can('pending')) {
                        // @todo: Log error message
                        throw new \Exception("An error occured while processing your request. Please contact administrator.");
                    }
                    // Apply a transition
                    $orderSM->apply('pending');
                } elseif ($order->getPaymentMethod() == Order::PAYMENT_METHOD_COD) {
                    if (!$orderSM->can('pending')) {
                        // @todo: Log error message
                        throw new \Exception("An error occured while processing your request. Please contact administrator.");
                    }
                    // Apply a transition
                    $orderSM->apply('pending');
                } else {
                    if (!$orderSM->can('checkout')) {
                        // @todo: Log error message
                        throw new \Exception("An error occured while processing your request. Please contact administrator.");
                    }
                    // Apply a transition
                    $orderSM->apply('checkout');
                    // Redirect to payment gateway
                    // $redirectResponse = $this->redirectToRoute('app_store_payment_redirect');
                }
                
                if ($this->container->getParameter('kernel.environment') == 'dev') {
                    // dump($order);die();
                }

                $orderManager->saveOrder($order, false);

                $redirectRouteParams = array('orderId' => $order->getId());
                $checkoutSuccess = true;
            } catch (\Exception $e) {
                $this->addFlash(
                    'error', 
                    $e->getMessage()
                );
            }

            if ($checkoutSuccess) {
                // $this->addFlash('checkout_order', $order->getId());
                $event = new OrderCheckoutFinishedEvent($order);
                $this->get('event_dispatcher')->dispatch(OrderCheckoutFinishedEvent::NAME, $event);

                $orderService->setForceGenerateCartUniqueIDFlag();
                return $this->redirectToRoute($redirectRoute, $redirectRouteParams);
            }
        }

        // $activeOrderPriceData = $orderService->getLineItemsPriceTotalsData($activeOrder);
        $orderTotals = $orderService->getTotals($activeOrder);

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            dump($orderTotals);
            // dump($activeOrderPriceData);
            dump($activeOrder);
        }

        return $this->render('AppBundle:Theme\Store\Default\Order:checkout.html.twig', array(
            'activeOrder' => $activeOrder,
            // 'activeOrderPriceData' => $activeOrderPriceData,
            'orderTotals' => $orderTotals,
            'form' => $form->createView(),
        ));
    }

    public function refreshCheckoutFormAction(Request $request, Security $security, $cartId)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('app_store_order_checkout');
        }

        // if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
        //     throw $this->createAccessDeniedException();
        // }

        $orderManager = $this->get('app.entity_manager.store.order');
        $orderService = $this->get('app.service.store_order');
        $activeOrder = $orderManager->getUserActiveOrder();

        if (!$activeOrder) {
            // @todo: Log error
            return $this->redirectToRoute('app_store_cart');
        }
        if ($activeOrder->getId() != $cartId) {
            // @todo: Log error
            $this->addFlash(
                'warning', 
                "It seems that the order you are trying to checkout is not valid. Please try to click again the checkout button. If this error still persist please contact administrator."
            );
            return $this->redirectToRoute('app_store_cart');
        }

        $user = null;
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
        } elseif ($activeOrder->getUser()) {
            // @todo: Log access denied on someone's order
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        }
        if ($activeOrder->getStatus() == Order::STATUS_EXPIRED) {
            $this->addFlash(
                'warning', 
                'Sorry your cart was already expired.'
            );
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        } elseif ($activeOrder->getUser() && $user && $activeOrder->getUser()->getId() !== $user->getId()) {
            // @todo: Log access denied on someone's order
            $response = $this->redirectToRoute('app_store_products_list');
            $orderService->setForceGenerateCartUniqueIDFlag();
            return $response;
        }

        // Add initial billing info if current user do not have
        if (!$activeOrder->getUser()->getBillingInformation()->count()) {
            $billingInformation = $this->get('app.entity_manager.user.billing_information')->create();
            $activeOrder->getUser()->addBillingInformation($billingInformation);
        }
        // Add initial shipping info if current user do not have
        if (!$activeOrder->getUser()->getBillingInformation()->count()) {
            $shippingInformation = $this->get('app.entity_manager.user.shipping_information')->create();
            $activeOrder->getUser()->addShippingInformation($shippingInformation);
        }

        $form = $this->_checkoutForm($activeOrder, $request->request->get('validate'));
        $form->handleRequest($request);

        // $activeOrderPriceData = $this->get('app.service.store_order')->getLineItemsPriceTotalsData($activeOrder);
        $orderTotals = $this->get('app.service.store_order')->getTotals($activeOrder);

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            dump($orderTotals);
            // dump($activeOrderPriceData);
            dump($activeOrder);
        }

        $formHtml = $this->renderView('AppBundle:Theme\Store\Default\Order:checkout_form.html.twig', array(
            'activeOrder' => $activeOrder,
            // 'activeOrderPriceData' => $activeOrderPriceData,
            'orderTotals' => $orderTotals,
            'form' => $form->createView(),
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'form_html' => $formHtml,
        ));

        return $response;
    }
    
    private function _checkoutForm($activeOrder = null, $validate = true)
    {
        if (!$activeOrder) {
            throw new \Exception("No active order.");
        }

        // $options = array('validation_groups' => 'AppBillingInfoEdit');
        $options = array(
			'method' => 'POST',
			'attr' => array('novalidate' => true),
        );
        if (!$validate) {
            $options['validation_groups'] = false;
        }

        $form = $this->createForm(OrderCheckoutType::class, $activeOrder, $options);

        $form
            ->add('submit', SubmitType::class, ['label' => 'Submit'])
        ;

        return $form;
    }

    public function successAction(Request $request, SMFactory $smFactory, $orderId)
    {
        $orderManager = $this->get('app.entity_manager.store.order');

        //$order = $orderManager->getUserOrderOnCheckoutSuccess(array('id' => $orderId));
        $order = $orderManager->find($orderId);

        if (!$order) {
            // @todo: Log
            throw $this->createNotFoundException('Order does not found');
        }

        $orderService = $this->get('app.service.store_order');
        $orderTotals = $orderService->getTotals($order);

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            dump($orderTotals);
            dump($order);
        }

        return $this->render('AppBundle:Theme\Store\Default\Order:checkout_success.html.twig', array(
            'order' => $order,
            'orderTotals' => $orderTotals,
        ));
    }
}