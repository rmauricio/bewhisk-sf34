<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Form\User\ShippingInfoType;

class ShippingInfoController extends Controller
{
    public function indexAction(Request $request, Security $security, SessionInterface $session)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $security->getUser();

        if ($user->getShippingInformation()->count()) {
            $shippingInfo = $user->getShippingInformation()->first();
        } else {
            $shippingInfo = $this->get('app.entity_manager.user.shipping_information')->create();
            $firstname = $user->getFirstname();
            $lastname = $user->getLastname();
            $shippingInfo->setFirstName($firstname);
            $shippingInfo->setLastName($lastname);
        }
        $shippingInfo->setUser($user);

        $city = $shippingInfo->getCity();
        if ($city) {
            $shippingInfo->setRegion($city->getRegion());
        }

        $form = $this->_getShippingInfoForm($shippingInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shippingInfo = $form->getData();

            $this->get('app.entity_manager.user.shipping_information')->save($shippingInfo);

            $session->getFlashBag()->add('success', 'Your shipping information has been successfully updated.');

            return $this->redirectToRoute('app_user_shipping_info_edit');
        }
        $formView = $form->createView();

        return $this->render('AppBundle:Theme\Store\Default\User:shipping_info_edit.html.twig', array(
            'form' => $formView,
            'shippingInfo' => $shippingInfo,
        ));
    }

    public function refreshShippingInfoFormAction(Request $request, Security $security)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('app_user_shipping_info_edit');
        }

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $security->getUser();

        if ($user->getShippingInformation()->count()) {
            $shippingInfo = $user->getShippingInformation()->first();
        } else {
            $shippingInfo = $this->get('app.entity_manager.user.shipping_information')->create();
        }
        $shippingInfo->setUser($user);

        $form = $this->_getShippingInfoForm($shippingInfo, $request->request->get('validate'));
        $form->handleRequest($request);

        $formHtml = $this->renderView('AppBundle:Theme\Store\Default\User:shipping_info_edit_form.html.twig', array(
            'form' => $form->createView(),
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'form_html' => $formHtml,
        ));

        return $response;
    }

    private function _getShippingInfoForm($data, $validate = true)
    {
        $options = array('validation_groups' => 'AppShippingInfoEdit');
        if (!$validate) {
            $options = array('validation_groups' => false);
        }

        $form = $this->createForm(ShippingInfoType::class, $data, $options);

        return $form;
    }
}