<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Form\User\BillingInfoType;

class BillingInfoController extends Controller
{
    public function indexAction(Request $request, Security $security, SessionInterface $session)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $security->getUser();

        if ($user->getBillingInformation()->count()) {
            $billingInfo = $user->getBillingInformation()->first();
        } else {
            $billingInfo = $this->get('app.entity_manager.user.billing_information')->create();
            $firstname = $user->getFirstname();
            $lastname = $user->getLastname();
            $billingInfo->setFirstName($firstname);
            $billingInfo->setLastName($lastname);
        }
        $billingInfo->setUser($user);

        $city = $billingInfo->getCity();
        if ($city) {
            $billingInfo->setRegion($city->getRegion());
        }

        $form = $this->_getBillingInfoForm($billingInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $billingInfo = $form->getData();

            $this->get('app.entity_manager.user.billing_information')->save($billingInfo);

            $session->getFlashBag()->add('success', 'Your billing information has been successfully updated.');

            return $this->redirectToRoute('app_user_billing_info_edit');
        }
        $formView = $form->createView();

        return $this->render('AppBundle:Theme\Store\Default\User:billing_info_edit.html.twig', array(
            'form' => $formView,
            'billingInfo' => $billingInfo,
        ));
    }

    public function refreshBillingInfoFormAction(Request $request, Security $security)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('app_user_billing_info_edit');
        }

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $security->getUser();

        if ($user->getBillingInformation()->count()) {
            $billingInfo = $user->getBillingInformation()->first();
        } else {
            $billingInfo = $this->get('app.entity_manager.user.billing_information')->create();
        }
        $billingInfo->setUser($user);

        $form = $this->_getBillingInfoForm($billingInfo, $request->request->get('validate'));
        $form->handleRequest($request);

        $formHtml = $this->renderView('AppBundle:Theme\Store\Default\User:billing_info_edit_form.html.twig', array(
            'form' => $form->createView(),
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'form_html' => $formHtml,
        ));

        return $response;
    }

    private function _getBillingInfoForm($data, $validate = true)
    {
        $options = array('validation_groups' => 'AppBillingInfoEdit');
        if (!$validate) {
            $options = array('validation_groups' => false);
        }

        $form = $this->createForm(BillingInfoType::class, $data, $options);

        return $form;
    }
}