<?php

namespace AppBundle\Controller\CRUD\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormRenderer;
use AppBundle\Entity\Store\Order;

use Sonata\AdminBundle\Controller\CRUDController;

class OrderPaymentCRUDController extends CRUDController
{
    protected function preCreate(Request $request, $object)
    {
        $allowedStatusesForPayment = Order::getAllowedStatusesForPayment();

        if (!$object || ($object && !$object->getId())) {
            $this->addFlash(
                'sonata_flash_error',
                'Creation of payment without a reference order is not allowed'
            );
        } elseif ($object && $allowedStatusesForPayment && !array_key_exists($object->getStatus(), $allowedStatusesForPayment)) {
            $this->addFlash(
                'sonata_flash_error',
                'Creation of payment for orders with the following statuses is not allowed. (' . implode(', ', $allowedStatusesForPayment) . ')'
            );
        }
        return null;
    }

    protected function preEdit(Request $request, $object)
    {
        if (!$object->getOrder() || ($object && !$object->getOrder()->getId())) {
            $this->addFlash(
                'sonata_flash_error',
                'Update of payment without a reference order is not allowed'
            );
        }
        return null;
    }
}