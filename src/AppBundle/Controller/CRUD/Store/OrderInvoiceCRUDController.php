<?php

namespace AppBundle\Controller\CRUD\Store;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormRenderer;
use AppBundle\Entity\Store\Order;

use Sonata\AdminBundle\Controller\CRUDController;

class OrderInvoiceCRUDController extends CRUDController
{
    protected function preCreate(Request $request, $object)
    {
        $allowedStatusesForInvoice = Order::getAllowedStatusesForInvoice();

        if (!$object || ($object && !$object->getId())) {
            $this->addFlash(
                'sonata_flash_error',
                'Creation of invoice without a reference order is not allowed'
            );
        } elseif ($object && $allowedStatusesForInvoice && !array_key_exists($object->getStatus(), $allowedStatusesForInvoice)) {
            $this->addFlash(
                'sonata_flash_error',
                'Creation of invoice for orders with the following statuses is not allowed. (' . implode(', ', $allowedStatusesForInvoice) . ')'
            );
        }
        return null;
    }
}