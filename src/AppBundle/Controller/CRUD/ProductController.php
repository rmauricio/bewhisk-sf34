<?php

namespace AppBundle\Controller\CRUD;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Component\Form\FormRenderer;

use Sonata\AdminBundle\Controller\CRUDController;

class ProductController extends CRUDController
{
	public function selectTypeAction()
    {
        $type = $this->admin->getPersistentParameter( 'type' );
        if ( !empty( $type ) ) {
            return $this->redirect( $this->admin->generateUrl( 'create', [ 'type' => $type ] ) );
        }

        $request = $this->getRequest();

        $this->admin->checkAccess('create');

        $typeOptions = array();
        $productTypes = $this->admin->getProductTypeManager()->findBy( array( 'enabled' => true ), array( 'position' => 'ASC', 'name' => 'ASC' ) );

        foreach ( $productTypes as $productType ) {
        	$typeOptions[ $productType->getCode() ] = $productType->getName();
        }

        if ( !$typeOptions ) {
        	// redirect to product type admin list
        	$this->addFlash('sonata_flash_warning', 'No available product types. Please create a product type and set it as default.');
        	$this->redirect( $this->admin->getProductTypeAdmin()->generateUrl( 'create' )  );
        }

        return $this->renderWithExtraParams('@SonataAdmin/CRUD/select-type.html.twig', [
            'action' => 'select_type',
            'box_title' => 'Product Type Selection',
            'type_options' => $typeOptions,
        ], null);
    }

    protected function preCreate(Request $request, $object)
    {
        $type = $this->admin->getPersistentParameter( 'type' );
        if ( empty( $type ) ) {
            return $this->redirect( $this->admin->generateUrl( 'select_type' ) );
        }

        return null;
    }

    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $datagridValues = $datagrid->getValues();

        $datagridTypeIsSet = isset($datagridValues['type']['value']) && !empty($datagridValues['type']['value']);

        if (!$this->admin->isChild() && $this->admin->getPersistentParameter('type') && !$datagridTypeIsSet) {
            $datagrid->setValue('type__code', null, $this->admin->getPersistentParameter('type'));
        }

        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFilterTheme());

        // NEXT_MAJOR: Remove this line and use commented line below it instead
        $template = $this->admin->getTemplate('list');
        // $template = $this->templateRegistry->getTemplate('list');

        return $this->renderWithExtraParams($template, [
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ], null);
    }

    /**
     * Sets the admin form theme to form view. Used for compatibility between Symfony versions.
     */
    private function setFormTheme(FormView $formView, array $theme = null): void
    {
        $twig = $this->get('twig');

        // BC for Symfony < 3.2 where this runtime does not exists
        if (!method_exists(AppVariable::class, 'getToken')) {
            $twig->getExtension(FormExtension::class)->renderer->setTheme($formView, $theme);

            return;
        }

        // BC for Symfony < 3.4 where runtime should be TwigRenderer
        if (!method_exists(DebugCommand::class, 'getLoaderPaths')) {
            $twig->getRuntime(TwigRenderer::class)->setTheme($formView, $theme);

            return;
        }

        $twig->getRuntime(FormRenderer::class)->setTheme($formView, $theme);
    }
}
