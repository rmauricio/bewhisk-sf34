<?php

namespace AppBundle\Entity\Traits;

/**
 * SortableTrait
 */
trait SortableTrait
{
    /**
     * @var int
     */
    protected $position = 0;

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return SortableTrait
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }
}