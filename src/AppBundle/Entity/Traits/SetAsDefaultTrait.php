<?php

namespace AppBundle\Entity\Traits;

/**
 * SetAsDefaultTrait
 */
trait SetAsDefaultTrait
{
    /**
     * @var bool
     */
    protected $isDefault = false;

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return SetAsDefaultTrait
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }
}