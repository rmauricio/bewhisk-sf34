<?php

namespace AppBundle\Entity\Traits;

/**
 * SoftDeleteTrait
 */
trait SoftDeleteTrait
{
    /**
     * @var bool
     */
    protected $deleted = false;

    /**
     * Set deleted.
     *
     * @param bool $deleted
     *
     * @return SoftDeleteTrait
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}