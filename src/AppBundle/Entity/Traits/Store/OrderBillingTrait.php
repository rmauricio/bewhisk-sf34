<?php

namespace AppBundle\Entity\Traits\Store;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderBillingTrait
 */
trait OrderBillingTrait
{
    protected $billingCity;
    protected $billingRegion;
    protected $billingFirstName;
    protected $billingLastName;
    protected $billingEmail;
    protected $billingAddress1;
    protected $billingAddress2;
    protected $billingPostalCode;
    protected $billingContactNumber;

    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    public function getBillingCity()
    {
        return $this->billingCity;
    }

    public function setBillingRegion($billingRegion)
    {
        $this->billingRegion = $billingRegion;

        return $this;
    }

    public function getBillingRegion()
    {
        return $this->billingRegion;
    }

    public function setBillingFirstName($billingFirstName)
    {
        $this->billingFirstName = $billingFirstName;

        return $this;
    }

    public function getBillingFirstName()
    {
        return $this->billingFirstName;
    }

    public function setBillingLastName($billingLastName)
    {
        $this->billingLastName = $billingLastName;

        return $this;
    }

    public function getBillingLastName()
    {
        return $this->billingLastName;
    }

    public function setBillingEmail($billingEmail)
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    public function getBillingEmail()
    {
        return $this->billingEmail;
    }

    public function setBillingAddress1($billingAddress1)
    {
        $this->billingAddress1 = $billingAddress1;

        return $this;
    }

    public function getBillingAddress1()
    {
        return $this->billingAddress1;
    }

    public function setBillingAddress2($billingAddress2)
    {
        $this->billingAddress2 = $billingAddress2;

        return $this;
    }

    public function getBillingAddress2()
    {
        return $this->billingAddress2;
    }

    public function setBillingPostalCode($billingPostalCode)
    {
        $this->billingPostalCode = $billingPostalCode;

        return $this;
    }

    public function getBillingPostalCode()
    {
        return $this->billingPostalCode;
    }

    public function setBillingContactNumber($billingContactNumber)
    {
        $this->billingContactNumber = $billingContactNumber;

        return $this;
    }

    public function getBillingContactNumber()
    {
        return $this->billingContactNumber;
    }
}