<?php

namespace AppBundle\Entity\Traits\Store;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPromoCodeTrait
 */
trait OrderPromoCodeTrait
{
    protected $promoCodeCode;
    protected $promoCodeValue;
    protected $promoCodeMinimumOrderTotal;
    protected $promoCodeStartDate;
    protected $promoCodeEndDate;

    public function setPromoCodeCode($promoCodeCode)
    {
        $this->promoCodeCode = $promoCodeCode;

        return $this;
    }

    public function getPromoCodeCode()
    {
        return $this->promoCodeCode;
    }

    public function setPromoCodeValue($promoCodeValue)
    {
        $this->promoCodeValue = $promoCodeValue;

        return $this;
    }

    public function getPromoCodeValue()
    {
        return $this->promoCodeValue;
    }

    public function setPromoCodeMinimumOrderTotal($promoCodeMinimumOrderTotal)
    {
        $this->promoCodeMinimumOrderTotal = $promoCodeMinimumOrderTotal;

        return $this;
    }

    public function getPromoCodeMinimumOrderTotal()
    {
        return $this->promoCodeMinimumOrderTotal;
    }

    public function setPromoCodeStartDate($promoCodeStartDate)
    {
        $this->promoCodeStartDate = $promoCodeStartDate;

        return $this;
    }

    public function getPromoCodeStartDate()
    {
        return $this->promoCodeStartDate;
    }

    public function setPromoCodeEndDate($promoCodeEndDate)
    {
        $this->promoCodeEndDate = $promoCodeEndDate;

        return $this;
    }

    public function getPromoCodeEndDate()
    {
        return $this->promoCodeEndDate;
    }
}