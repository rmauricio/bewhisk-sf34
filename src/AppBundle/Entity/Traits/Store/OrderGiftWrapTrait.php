<?php

namespace AppBundle\Entity\Traits\Store;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderGiftWrapTrait
 */
trait OrderGiftWrapTrait
{
    protected $giftWrapDedication;

    public function setGiftWrapDedication($giftWrapDedication)
    {
        $this->giftWrapDedication = $giftWrapDedication;

        return $this;
    }

    public function getGiftWrapDedication()
    {
        return $this->giftWrapDedication;
    }
}