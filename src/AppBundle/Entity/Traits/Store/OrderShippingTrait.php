<?php

namespace AppBundle\Entity\Traits\Store;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderShippingTrait
 */
trait OrderShippingTrait
{
    protected $shippingCity;
    protected $shippingRegion;
    protected $shippingFirstName;
    protected $shippingLastName;
    protected $shippingEmail;
    protected $shippingAddress1;
    protected $shippingAddress2;
    protected $shippingPostalCode;
    protected $shippingContactNumber;
    protected $shippingInstructions;

    public function setShippingCity($shippingCity)
    {
        $this->shippingCity = $shippingCity;

        return $this;
    }

    public function getShippingCity()
    {
        return $this->shippingCity;
    }

    public function setShippingRegion($shippingRegion)
    {
        $this->shippingRegion = $shippingRegion;

        return $this;
    }

    public function getShippingRegion()
    {
        return $this->shippingRegion;
    }

    public function setShippingFirstName($shippingFirstName)
    {
        $this->shippingFirstName = $shippingFirstName;

        return $this;
    }

    public function getShippingFirstName()
    {
        return $this->shippingFirstName;
    }

    public function setShippingLastName($shippingLastName)
    {
        $this->shippingLastName = $shippingLastName;

        return $this;
    }

    public function getShippingLastName()
    {
        return $this->shippingLastName;
    }

    public function setShippingEmail($shippingEmail)
    {
        $this->shippingEmail = $shippingEmail;

        return $this;
    }

    public function getShippingEmail()
    {
        return $this->shippingEmail;
    }

    public function setShippingAddress1($shippingAddress1)
    {
        $this->shippingAddress1 = $shippingAddress1;

        return $this;
    }

    public function getShippingAddress1()
    {
        return $this->shippingAddress1;
    }

    public function setShippingAddress2($shippingAddress2)
    {
        $this->shippingAddress2 = $shippingAddress2;

        return $this;
    }

    public function getShippingAddress2()
    {
        return $this->shippingAddress2;
    }

    public function setShippingPostalCode($shippingPostalCode)
    {
        $this->shippingPostalCode = $shippingPostalCode;

        return $this;
    }

    public function getShippingPostalCode()
    {
        return $this->shippingPostalCode;
    }

    public function setShippingContactNumber($shippingContactNumber)
    {
        $this->shippingContactNumber = $shippingContactNumber;

        return $this;
    }

    public function getShippingContactNumber()
    {
        return $this->shippingContactNumber;
    }

    public function setShippingInstructions($shippingInstructions)
    {
        $this->shippingInstructions = $shippingInstructions;

        return $this;
    }

    public function getShippingInstructions()
    {
        return $this->shippingInstructions;
    }
}