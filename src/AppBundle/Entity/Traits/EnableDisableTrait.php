<?php

namespace AppBundle\Entity\Traits;

/**
 * EnableDisableTrait
 */
trait EnableDisableTrait
{
    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return EnableDisableTrait
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}