<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Entity\TimestampableInterface;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * BillingInformation
 */
class BillingInformation implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $address1;

    /**
     * @var string
     */
    protected $address2;

    /**
     * @var string
     */
    protected $postalCode;

    /**
     * @var string
     */
    protected $contactNumber;

    /**
     * @var \AppBundle\Entity\Address\City
      */
    protected $city;

    /**
     * @var \AppBundle\Entity\Address\Region
      */
    protected $region;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
      */
    protected $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return BillingInformation
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return BillingInformation
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return BillingInformation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address1.
     *
     * @param string $address1
     *
     * @return BillingInformation
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1.
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2.
     *
     * @param string $address2
     *
     * @return BillingInformation
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2.
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set postalCode.
     *
     * @param string $postalCode
     *
     * @return BillingInformation
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode.
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set contactNumber.
     *
     * @param string $contactNumber
     *
     * @return BillingInformation
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber.
     *
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getAddress1() : 'New Billing Information';
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\Address\City $city
     *
     * @return BillingInformation
     */
    public function setCity(\AppBundle\Entity\Address\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\Address\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return BillingInformation
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set region
     *
     * @param \AppBundle\Entity\Address\Region $region
     *
     * @return BillingInformation
     */
    public function setRegion(\AppBundle\Entity\Address\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \AppBundle\Entity\Address\Region
     */
    public function getRegion()
    {
        return $this->region;
    }
}
