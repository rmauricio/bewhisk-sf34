<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * ProductAttributeColor
 */
class ProductAttributeColor implements SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\ProductAttribute
     */
    private $attribute;

    /**
     * @var \AppBundle\Entity\Store\Color
     */
    private $color;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Product Attribute Color: ' . $this->color : 'New Product Attribute Color';
    }

    /**
     * Set attribute.
     *
     * @param \AppBundle\Entity\Store\ProductAttribute $attribute
     *
     * @return ProductAttributeColor
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \AppBundle\Entity\Store\ProductAttribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return ProductAttributeColor
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set color.
     *
     * @param \AppBundle\Entity\Store\Color $color
     *
     * @return ProductAttributeColor
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return \AppBundle\Entity\Store\Color
     */
    public function getColor()
    {
        return $this->color;
    }
}
