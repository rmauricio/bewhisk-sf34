<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\DateRangeInterface;
use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\DateRangeTrait;
use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

use AppBundle\Entity\Store\Order;

use JMS\Serializer\Annotation\Exclude;

/**
 * PromoCode
 */
class PromoCode implements DateRangeInterface, EnableDisableInterface, SoftDeleteInterface, TimestampableInterface
{
    use DateRangeTrait;
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;
    
    /**
     * @var string
     */
    private $code;
    
    /**
     * @var float
     */
    private $value = 0;
    
    /**
     * @var float
     */
    private $minimumOrderTotal = 0;

    /**
     * @Exclude()
     * @var \AppBundle\Entity\Store\Order
     */
    private $order;

    /**
     * @Exclude()
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $orders;

    public function __construct() 
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param float $code
     *
     * @return PromoCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set value.
     *
     * @param float $value
     *
     * @return PromoCode
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set minimumOrderTotal.
     *
     * @param float $minimumOrderTotal
     *
     * @return PromoCode
     */
    public function setMinimumOrderTotal($minimumOrderTotal)
    {
        $this->minimumOrderTotal = $minimumOrderTotal;

        return $this;
    }

    /**
     * Get minimumOrderTotal.
     *
     * @return string $minimumOrderTotal
     */
    public function getMinimumOrderTotal()
    {
        return $this->minimumOrderTotal;
    }

    /**
     * Set order.
     *
     * @param \AppBundle\Entity\Store\Order $order
     *
     * @return PromoCode
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \AppBundle\Entity\Store\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set orders.
     *
     * @param array $orders
     */
    public function setOrders($orders)
    {
        foreach ($orders as $order) {
            $this->addOrder($order);
        }
    }

    /**
     * Get orders.
     *
     * @return AppBundle\Entity\Store\Order[] $orders
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order
     */
    public function addOrder(Order $order)
    {
        $order->setPromoCode($this);

        $this->orders[] = $order;
    }
 
    /**
     * @param Order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getCode() : 'New Promo Code';
    }
}
