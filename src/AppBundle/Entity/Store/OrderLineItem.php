<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * OrderLineItem
 */
class OrderLineItem implements SoftDeleteInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use TimestampableTrait;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Order
     */
    private $order;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var string
     */
    private $productSku;

    /**
     * @var int
     */
    private $productTypeId;

    /**
     * @var string
     */
    private $productTypeCode;

    /**
     * @var string
     */
    private $productTypeName;

    /**
     * @var int
     */
    private $quantity = 1;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $finalPrice;

    /**
     * @var json
     */
    private $data;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set productName.
     *
     * @param string $productName
     *
     * @return OrderLineItem
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName.
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }
    
    /**
     * Set productSku.
     *
     * @param string $productSku
     *
     * @return OrderLineItem
     */
    public function setProductSku($productSku)
    {
        $this->productSku = $productSku;

        return $this;
    }

    /**
     * Get productSku.
     *
     * @return string
     */
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * Set productTypeId.
     *
     * @param int $productTypeId
     *
     * @return OrderLineItem
     */
    public function setProductTypeId($productTypeId)
    {
        $this->productTypeId = $productTypeId;

        return $this;
    }

    /**
     * Get productTypeId.
     *
     * @return int
     */
    public function getProductTypeId()
    {
        return $this->productTypeId;
    }

    /**
     * Set productTypeCode.
     *
     * @param string $productTypeCode
     *
     * @return OrderLineItem
     */
    public function setProductTypeCode($productTypeCode)
    {
        $this->productTypeCode = $productTypeCode;

        return $this;
    }

    /**
     * Get productTypeCode.
     *
     * @return string
     */
    public function getProductTypeCode()
    {
        return $this->productTypeCode;
    }

    /**
     * Set productTypeName.
     *
     * @param string $productTypeName
     *
     * @return OrderLineItem
     */
    public function setProductTypeName($productTypeName)
    {
        $this->productTypeName = $productTypeName;

        return $this;
    }

    /**
     * Get productTypeName.
     *
     * @return string
     */
    public function getProductTypeName()
    {
        return $this->productTypeName;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return OrderLineItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return OrderLineItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set finalPrice.
     *
     * @param float $finalPrice
     *
     * @return OrderLineItem
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;

        return $this;
    }

    /**
     * Get finalPrice.
     *
     * @return float
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * Set data.
     *
     * @param json $data
     *
     * @return OrderLineItem
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return json
     */
    public function getData()
    {
        return $this->data;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Order Line Item ID: ' . $this->getId() . ' (' . $this->productName . ')' : 'New Order Line Item';
    }

    /**
     * Set order.
     *
     * @param \AppBundle\Entity\Store\Order $order
     *
     * @return OrderLineItem
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \AppBundle\Entity\Store\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return OrderLineItem
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    protected $parentProduct;

    protected $color;

    protected $size;

    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;

        return $this;
    }

    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getPriceData($forceDataFromRef = false) 
    {
        $dataFromRef = false;
        if (!$this->id) {
            $dataFromRef = true;
        } elseif ($forceDataFromRef) { // force get data from reference
            $dataFromRef = true;
        } elseif ($this->order && $this->order->getId() && $this->order->isOnInitialState()) {
            $dataFromRef = true;
        }

        // Get price based on current product price and discounts
        if ($dataFromRef) {
            $productPriceData = $this->product->getPriceData();
            return array(
                'price' => $productPriceData['price'],
                'discount' => $productPriceData['discount'],
                'finalPrice' => $productPriceData['final'],
                // Totals
                'subTotal' => $productPriceData['price'] * $this->quantity,
                'discountTotal' => $productPriceData['discount'] * $this->quantity,
                'finalSubTotal' => $productPriceData['final'] * $this->quantity,
                // Extra
                'hasDiscount' => ($productPriceData['discount'] > 0)
            );
        }

        return array(
            'price' => $this->price,
            'discount' => ($this->price - $this->finalPrice),
            'finalPrice' => $this->finalPrice,
            // Totals
            'subTotal' => ($this->price * $this->quantity),
            'discountTotal' => (($this->price - $this->finalPrice) * $this->quantity),
            'finalSubTotal' => ($this->finalPrice * $this->quantity),
            // Extra
            'hasDiscount' => ($this->finalPrice < $this->price)
        );
    }

    public function getPriceData_()
    {
        $basePrice = $this->price;
        $price = $this->price;
        $final = $price;
        $discountTotal = 0;

        $currentDate = new \DateTime();

        if (!empty($this->data['product']['discounts'])) {
            foreach ($this->data['product']['discounts'] as $discount) {
                // validate discount availability
                if (!$discount['enabled']) {
                    continue;
                }
                // if ($discount['start_date'] && $discount['start_date']->format('U') > $currentDate->format('U')) {
                //     continue;
                // }
                // if ($discount->getEndDate() && $discount->getEndDate()->format('U') < $currentDate->format('U')) {
                //     continue;
                // }

                // start computation
                $value = $discount['value'];
                if ($discount['type'] == Discount::TYPE_FIXED_AMOUNT) {
                    $final = $final - $value;
                    $discountTotal = $discountTotal + $value;
                } elseif ($discount['type'] == Discount::TYPE_PERCENTAGE) {
                    $percentageValue = ( $value / 100 ) * $basePrice;
                    $final = $final - $percentageValue;
                    $discountTotal = $discountTotal + $percentageValue;
                }
            }
        }

        $subTotal = $price * $this->quantity;
        $discountSubTotal = $discountTotal * $this->quantity;
        $finalSubTotal = $final * $this->quantity;

        // Current product price
        $currentBasePrice = $this->product->getPrice();
        $currentPrice = $this->product->getPrice();
        $currentFinal = $currentPrice;
        $currentDiscountTotal = 0;

        if ($this->product->getDiscounts()) {
            foreach ($this->product->getDiscounts() as $discount) {
                // validate discount availability
                if (!$discount->getEnabled()) {
                    continue;
                }
                if ($discount->getStartDate() && $discount->getStartDate()->format('U') > $currentDate->format('U')) {
                    continue;
                }
                if ($discount->getEndDate() && $discount->getEndDate()->format('U') < $currentDate->format('U')) {
                    continue;
                }
                
                // start computation
                $value = $discount->getValue();
                if ($discount->getType() == Discount::TYPE_FIXED_AMOUNT) {
                    $currentFinal = $currentFinal - $value;
                    $currentDiscountTotal = $currentDiscountTotal + $value;
                } elseif ($discount->getType() == Discount::TYPE_PERCENTAGE) {
                    $percentageValue = ( $value / 100 ) * $currentBasePrice;
                    $currentFinal = $currentFinal - $percentageValue;
                    $currentDiscountTotal = $currentDiscountTotal + $percentageValue;
                }
            }
        }

        $currentSubTotal = $currentPrice * $this->quantity;
        $currentDiscountSubTotal = $currentDiscountTotal * $this->quantity;
        $currentFinalSubTotal = $currentFinal * $this->quantity;

        return array(
            'current' => array(
                'price' => $currentPrice,
                'discount' => $currentDiscountTotal,
                'final' => $currentFinal,
                'subTotal' => $currentSubTotal,
                'discountSubTotal' => $currentDiscountSubTotal,
                'finalSubTotal' => $currentFinalSubTotal,
                'hasDiscount' => ($currentFinal < $currentPrice)
            ),
            'price' => $price,
            'discount' => $discountTotal,
            'final' => $final,
            'subTotal' => $subTotal,
            'discountSubTotal' => $discountSubTotal,
            'finalSubTotal' => $finalSubTotal,
            'hasDiscount' => ($final < $price)
        );
    }
}
