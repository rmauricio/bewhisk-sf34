<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * Invoice
 */
class Invoice implements SoftDeleteInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Order
     */
    private $order;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Invoice ID: ' . $this->getId() : 'New Invoice';
    }

    /**
     * Set order.
     *
     * @param \AppBundle\Entity\Store\Order $order
     *
     * @return Invoice
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \AppBundle\Entity\Store\Order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
