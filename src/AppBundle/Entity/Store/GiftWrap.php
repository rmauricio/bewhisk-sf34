<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

use JMS\Serializer\Annotation\Exclude;

/**
 * GiftWrap
 */
class GiftWrap implements EnableDisableInterface, SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @Exclude()
     * @var \Application\Sonata\MediaBundle\Media $image
     */
    protected $image;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return GiftWrap
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return GiftWrap
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return GiftWrap
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Media $image
     *
     * @return GiftWrap
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Media $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'New Gift Wrap';
    }
}
