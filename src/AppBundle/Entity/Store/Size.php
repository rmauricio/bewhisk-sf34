<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * Size
 */
class Size implements EnableDisableInterface, SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $code;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Size
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Size
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'New Size';
    }
}
