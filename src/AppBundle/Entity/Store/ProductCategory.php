<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * ProductCategory
 */
class ProductCategory implements SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;

    /**
     * @var \Application\Sonata\ClassificationBundle\Entity\Category
     */
    private $category;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Product Category: ' . $this->category->getName() : 'New Product Category';
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return ProductAttributeColor
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set category.
     *
     * @param \Application\Sonata\ClassificationBundle\Entity\Category $category
     *
     * @return ProductAttributeColor
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}
