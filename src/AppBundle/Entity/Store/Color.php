<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * Color
 */
class Color implements EnableDisableInterface, SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var AppBundle\Entity\Store\Color
     */
    private $parent;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $children;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $hexCode;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $categories;


    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Color
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hexCode.
     *
     * @param string $hexCode
     *
     * @return Color
     */
    public function setHexCode($hexCode)
    {
        $this->hexCode = $hexCode;

        return $this;
    }

    /**
     * Get hexCode.
     *
     * @return string
     */
    public function getHexCode()
    {
        return $this->hexCode;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'New Color';
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(Color $child, $nested = false)
    {
        $this->children[] = $child;

        if (!$nested) {
            $child->setParent($this, true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeChild(Color $childToDelete)
    {
        foreach ($this->getChildren() as $pos => $child) {
            if ($childToDelete->getId() && $child->getId() === $childToDelete->getId()) {
                unset($this->children[$pos]);

                return;
            }

            if (!$childToDelete->getId() && $child === $childToDelete) {
                unset($this->children[$pos]);

                return;
            }
        }
    }

    /**
     * Get children.
     *
     * @return ArrayCollection[] $children
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set children.
     *
     * @param array $children
     */
    public function setChildren($children)
    {
        $this->children = new ArrayCollection();

        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    public function hasChildren()
    {
        return \count($this->children) > 0;
    }

    /**
     * Set parent.
     *
     * @param \AppBundle\Entity\Store\Color $parent
     *
     * @return Color
     */
    public function setParent(Color $parent = null, $nested = false)
    {
        $this->parent = $parent;

        if (!$nested && $parent) {
            $parent->addChild($this, true);
        }
    }

    /**
     * Get parent.
     *
     * @return \AppBundle\Entity\Store\Color
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set categories.
     *
     * @param array $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Get categories.
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category[] $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
