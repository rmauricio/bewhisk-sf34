<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\TimestampableInterface;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * ProductAttribute
 */
class ProductStock implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;

    /**
     * @var int
     */
    private $value = 0;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return ProductStock
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set value.
     *
     * @param int $value
     *
     * @return ProductStock
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->value . ' Stocks' : 'New Product Stock';
    }
}
