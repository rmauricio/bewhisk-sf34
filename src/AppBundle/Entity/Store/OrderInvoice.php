<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\TimestampableInterface;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * OrderInvoice
 */
class OrderInvoice implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $invoiceNumber;

    /**
     * @var \AppBundle\Entity\Store\Order
     */
    private $order;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoiceNumber.
     *
     * @param string|null $invoiceNumber
     *
     * @return OrderInvoice
     */
    public function setInvoiceNumber($invoiceNumber = null)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber.
     *
     * @return string|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set order.
     *
     * @param \AppBundle\Entity\Store\Order $order
     *
     * @return OrderInvoice
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \AppBundle\Entity\Store\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set user.
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return OrderInvoice
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
