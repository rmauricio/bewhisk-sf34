<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

/**
 * CartLineItem
 */
class CartLineItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Cart
     */
    private $cart;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var int
     */
    private $productTypeId;

    /**
     * @var string
     */
    private $productTypeCode;

    /**
     * @var string
     */
    private $productTypeName;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var json
     */
    private $data;

    /**
     * @var bool
     */
    private $deleted = 0;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productName.
     *
     * @param string $productName
     *
     * @return CartLineItem
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName.
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set productTypeId.
     *
     * @param int $productTypeId
     *
     * @return CartLineItem
     */
    public function setProductTypeId($productTypeId)
    {
        $this->productTypeId = $productTypeId;

        return $this;
    }

    /**
     * Get productTypeId.
     *
     * @return int
     */
    public function getProductTypeId()
    {
        return $this->productTypeId;
    }

    /**
     * Set productTypeCode.
     *
     * @param string $productTypeCode
     *
     * @return CartLineItem
     */
    public function setProductTypeCode($productTypeCode)
    {
        $this->productTypeCode = $productTypeCode;

        return $this;
    }

    /**
     * Get productTypeCode.
     *
     * @return string
     */
    public function getProductTypeCode()
    {
        return $this->productTypeCode;
    }

    /**
     * Set productTypeName.
     *
     * @param string $productTypeName
     *
     * @return CartLineItem
     */
    public function setProductTypeName($productTypeName)
    {
        $this->productTypeName = $productTypeName;

        return $this;
    }

    /**
     * Get productTypeName.
     *
     * @return string
     */
    public function getProductTypeName()
    {
        return $this->productTypeName;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return CartLineItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return CartLineItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set data.
     *
     * @param json $data
     *
     * @return CartLineItem
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return json
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set deleted.
     *
     * @param bool $deleted
     *
     * @return CartLineItem
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return CartLineItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return CartLineItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Line Item ID: ' . $this->getId() . ' (' . $this->productName . ')' : 'New Cart Line Item';
    }

    /**
     * Set cart.
     *
     * @param \AppBundle\Entity\Store\Cart $cart
     *
     * @return CartLineItem
     */
    public function setCart($cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart.
     *
     * @return \AppBundle\Entity\Store\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return CartLineItem
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
