<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

use AppBundle\Entity\Store\Discount;
use AppBundle\Entity\Store\OrderLineItem;
use JMS\Serializer\Annotation\Exclude;

/**
 * Product
 */
class Product implements EnableDisableInterface, SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $abstract;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $price = 0;

    /**
     * @var \AppBundle\Entity\Store\ProductStock
     */
    private $stock;

    /**
     * @var \DateTime
     */
    private $publicationStartDate;

    /**
     * @var \DateTime
     */
    private $publicationEndDate;

    /**
     * @var int
     */
    private $variationCount = 0;

    /**
     * @var AppBundle\Entity\Store\Product
     */
    private $parent;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $variations;

    /**
     * @Exclude()
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $colors;

    /**
     * @Exclude()
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $sizes;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $categories;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $discounts;

    /**
     * @Exclude()
     * @var \Application\Sonata\MediaBundle\Media $image
     */
    protected $image;

    /**
     * @Exclude()
     * @var \Application\Sonata\MediaBundle\Media $thumbnail
     */
    protected $thumbnail;

    /**
     * @Exclude()
     * @var \Application\Sonata\MediaBundle\Gallery $gallery
     */
    protected $gallery;

    /**
     * @var \AppBundle\Entity\Store\ProductType $type
     */
    protected $type;

    /**
     * @var \AppBundle\Entity\Store\Color $color
     */
    protected $color;

    /**
     * @var \AppBundle\Entity\Store\Size $size
     */
    protected $size;

    /**
     * @var \Application\Sonata\ClassificationBundle\Category $brand
     */
    protected $brand;

    /**
     * @var \Application\Sonata\MediaBundle\Category $theme
     */
    protected $theme;

    /**
     * @Exclude()
     */
    protected $lineItems;

    /**
     * @var bool
     */
    private $sale = false;

    /**
     * @var bool
     */
    private $featured = false;

    /**
     * @var int
     */
    private $productStock;

    public function __construct()
    {
        $this->publicationStartDate = new \DateTime();
        $this->variations = new ArrayCollection();
        $this->colors = new ArrayCollection();
        $this->sizes = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->lineItems = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sku.
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku.
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set abstract.
     *
     * @param string $abstract
     *
     * @return Product
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract.
     *
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set stock.
     *
     * @param \AppBundle\Entity\Store\ProductStock $stock
     *
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock.
     *
     * @return \AppBundle\Entity\Store\ProductStock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set sale.
     *
     * @param bool $sale
     *
     * @return Product
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale.
     *
     * @return bool
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set featured.
     *
     * @param bool $featured
     *
     * @return Product
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured.
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set publicationStartDate.
     *
     * @param \DateTime $publicationStartDate
     *
     * @return Discount
     */
    public function setPublicationStartDate($publicationStartDate)
    {
        $this->publicationStartDate = $publicationStartDate;

        return $this;
    }

    /**
     * Get publicationStartDate.
     *
     * @return \DateTime
     */
    public function getPublicationStartDate()
    {
        return $this->publicationStartDate;
    }

    /**
     * Set publicationEndDate.
     *
     * @param \DateTime $publicationEndDate
     *
     * @return Discount
     */
    public function setPublicationEndDate($publicationEndDate)
    {
        $this->publicationEndDate = $publicationEndDate;

        return $this;
    }

    /**
     * Get publicationEndDate.
     *
     * @return \DateTime
     */
    public function getPublicationEndDate()
    {
        return $this->publicationEndDate;
    }

    /**
     * Set variationCount.
     *
     * @param int $variationCount
     *
     * @return Product
     */
    public function setVariationCount($variationCount)
    {
        $this->variationCount = $variationCount;

        return $this;
    }

    /**
     * Get variationCount.
     *
     * @return int
     */
    public function getVariationCount()
    {
        return $this->variationCount;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() . ' (' . $this->getSku() . ')' : 'New Product';
    }

    /**
     * {@inheritdoc}
     */
    public function addVariation(Product $variation, $nested = false)
    {
        $this->variations[] = $variation;

        if (!$nested) {
            $variation->setParent($this, true);
        }

        $this->variationCount += 1;
    }

    /**
     * {@inheritdoc}
     */
    public function removeVariation(Product $variationToDelete)
    {
        foreach ($this->getVariations() as $pos => $variation) {
            if ($variationToDelete->getId() && $variation->getId() === $variationToDelete->getId()) {
                unset($this->variations[$pos]);
                $this->variationCount -= 1;
                return;
            }

            if (!$variationToDelete->getId() && $variation === $variationToDelete) {
                unset($this->variations[$pos]);
                $this->variationCount -= 1;
                return;
            }
        }
    }

    /**
     * Get variations.
     *
     * @return ArrayCollection[] $variations
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * Set variations.
     *
     * @param array $variations
     */
    public function setVariations($variations)
    {
        $this->variations = new ArrayCollection();

        foreach ($variations as $variation) {
            $this->addVariation($variation);
        }
    }

    public function hasVariations()
    {
        return \count($this->variations) > 0;
    }

    /**
     * Set colors.
     *
     * @param array $colors
     */
    public function setColors($attributeColors)
    {
        $this->colors = new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($attributeColors as $attributeColor) {
            $this->addColor($attributeColor);
        }
    }

    /**
     * Get colors.
     *
     * @return \AppBundle\Entity\Store\ProductAttributeColor[] $colors
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * @param \AppBundle\Entity\Store\ProductAttributeColor
     */
    public function addColor(\AppBundle\Entity\Store\ProductAttributeColor $attributeColor)
    {
        $attributeColor->setProduct($this);

        $this->colors[] = $attributeColor;
    }

    /**
     * @param ArticleProduct
     */
    public function removeColor(\AppBundle\Entity\Store\ProductAttributeColor $attributeColor)
    {
        $this->colors->removeElement($attributeColor);
    }

    /**
     * Set sizes.
     *
     * @param array $sizes
     */
    public function setSizes($attributeSizes)
    {
        $this->sizes = new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($attributeSizes as $attributeSize) {
            $this->addSize($attributeSize);
        }
    }

    /**
     * Get sizes.
     *
     * @return \AppBundle\Entity\Store\ProductAttributeSize[] $sizes
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @param \AppBundle\Entity\Store\ProductAttributeSize
     */
    public function addSize(\AppBundle\Entity\Store\ProductAttributeSize $attributeSize)
    {
        $attributeSize->setProduct($this);

        $this->sizes[] = $attributeSize;
    }

    /**
     * @param ArticleProduct
     */
    public function removeSize(\AppBundle\Entity\Store\ProductAttributeSize $attributeSize)
    {
        $this->sizes->removeElement($attributeSize);
    }

    /**
     * Set categories.
     *
     * @param array $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Get categories.
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category[] $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set discounts.
     *
     * @param array $discounts
     */
    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;
    }

    /**
     * Get discounts.
     *
     * @return \AppBundle\Entity\Store\Discount[] $discounts
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * Set parent.
     *
     * @param \AppBundle\Entity\Store\Product $parent
     *
     * @return Product
     */
    public function setParent(Product $parent = null, $nested = false)
    {
        $this->parent = $parent;

        if (!$nested && $parent) {
            $parent->addVariation($this, true);
        }
    }

    /**
     * Get parent.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Media $image
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Media $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set thumbnail.
     *
     * @param \Application\Sonata\MediaBundle\Media $thumbnail
     *
     * @return Product
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \Application\Sonata\MediaBundle\Media $thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set gallery.
     *
     * @param \Application\Sonata\MediaBundle\Gallery $gallery
     *
     * @return Product
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery.
     *
     * @return \Application\Sonata\MediaBundle\Gallery $gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Set type.
     *
     * @param \AppBundle\Entity\Store\ProductType $type
     *
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \AppBundle\Entity\Store\ProductType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set color.
     *
     * @param \AppBundle\Entity\Store\Color $color
     *
     * @return Product
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return \AppBundle\Entity\Store\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set size.
     *
     * @param \AppBundle\Entity\Store\Size $size
     *
     * @return Product
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return \AppBundle\Entity\Store\Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set brand.
     *
     * @param \Application\Sonata\ClassificationBundle\Category $brand
     *
     * @return Product
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \Application\Sonata\ClassificationBundle\Category $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set theme.
     *
     * @param \Application\Sonata\ClassificationBundle\Category $theme
     *
     * @return Product
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme.
     *
     * @return \Application\Sonata\ClassificationBundle\Category $theme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    public function computeDiscount()
    {
        $basePrice = $this->price;
        $discountTotal = 0;

        $currentDate = new \DateTime();
        foreach ($this->discounts as $discount) {
            // validate discount availability
            if (!$discount->getEnabled()) {
                continue;
            }
            if ($discount->getStartDate() && $discount->getStartDate()->format('U') > $currentDate->format('U')) {
                continue;
            }
            if ($discount->getEndDate() && $discount->getEndDate()->format('U') < $currentDate->format('U')) {
                continue;
            }

            // start computation
            $value = $discount->getValue();
            if ($discount->getType() == Discount::TYPE_FIXED_AMOUNT) {
                $discountTotal = $discountTotal + $value;
            } elseif ($discount->getType() == Discount::TYPE_PERCENTAGE) {
                $percentageValue = ( $value / 100 ) * $basePrice;
                $discountTotal = $discountTotal + $percentageValue;
            }
        }

        return $discountTotal;
    }

    public function computePrice(&$discountTotal = 0)
    {
        if (count($this->discounts) == 0) {
            return $this->price;
        }

        $basePrice = $this->price;
        $computedPrice = $basePrice;
        $discountTotal = $this->computeDiscount();

        $computedPrice = $computedPrice - $discountTotal;

        // avoid returning price less than zero
        if ($computedPrice < 0) {
            $computedPrice = $basePrice;
        }

        return $computedPrice;
    }

    public function getPriceData()
    {
        $discountTotal = 0;
        $final = $this->computePrice($discountTotal);

        return array(
            'price' => $this->price,
            'final' => $final,
            'discount' => $discountTotal,
            'hasDiscount' => ($final < $this->price)
        );
    }

    public function getEnabledVariations()
    {
        if (!$this->hasVariations()) {
            return array();
        }

        return $this->getVariations()->filter(function(Product $variation) {
            return $variation->getEnabled();
        });
    }

    public function getColorVariations($all = true)
    {
        if (!$this->hasVariations()) {
            return array();
        }

        return $this->getVariations()->filter(function(Product $variation, $all) {
            if (!$all) {
                return $variation->getColor() && $variation->getEnabled();
            }
            return $variation->getColor();
        });
    }

    public function getEnabledColorVariations()
    {
        return $this->getColorVariations(false);
    }

    public function getSizeVariations($all = true)
    {
        if (!$this->hasVariations()) {
            return array();
        }

        return $this->getVariations()->filter(function(Product $variation, $all) {
            if (!$all) {
                return $variation->getSize() && $variation->getEnabled();
            }
            return $variation->getSize();
        });
    }

    public function getEnabledSizeVariations()
    {
        return $this->getSizeVariations(false);
    }

    /**
     * Set lineItems.
     *
     * @param array $lineItems
     */
    public function setLineItems($lineItems)
    {
        foreach ($lineItems as $lineItem) {
            $this->addLineItem($lineItem);
        }
    }

    /**
     * Get lineItems.
     *
     * @return AppBundle\Entity\Store\OrderLineItem[] $lineItems
     */
    public function getLineItems()
    {
        return $this->lineItems;
    }

    /**
     * @param OrderLineItem
     */
    public function addLineItem(OrderLineItem $lineItem)
    {
        $lineItem->setOrder($this);

        $this->lineItems[] = $lineItem;
    }

    /**
     * @param OrderLineItem
     */
    public function removeLineItem(OrderLineItem $lineItem)
    {
        $this->lineItems->removeElement($lineItem);
    }
    
    /**
     * Set productStock.
     *
     * @param int $productStock
     *
     * @return Product
     */
    public function setProductStock($productStock)
    {
        $this->productStock = $productStock;

        return $this;
    }

    /**
     * Get productStock.
     *
     * @return int
     */
    public function getProductStock()
    {
        return $this->productStock;
    }
}
