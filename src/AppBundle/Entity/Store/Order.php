<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

use AppBundle\Entity\Traits\Store\OrderBillingTrait;
use AppBundle\Entity\Traits\Store\OrderShippingTrait;
use AppBundle\Entity\Traits\Store\OrderGiftWrapTrait;
use AppBundle\Entity\Traits\Store\OrderPromoCodeTrait;

use AppBundle\Entity\Store\OrderLineItem;
use AppBundle\Entity\Store\OrderInvoice;
use AppBundle\Entity\Store\OrderPayment;

/**
 * Order
 */
class Order implements SoftDeleteInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use TimestampableTrait;
    use OrderBillingTrait;
    use OrderShippingTrait;
    use OrderGiftWrapTrait;
    use OrderPromoCodeTrait;

    const STATUS_NEW = 'new';
    const STATUS_IN_CART = 'in_cart';
    const STATUS_IN_CHECKOUT = 'in_checkout';
    const STATUS_IN_PAYMENT_GATEWAY = 'in_payment_gateway';
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_PAID = 'paid';
    const STATUS_COMPLETE = 'complete';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_ERROR = 'error';
    const STATUS_EXPIRED = 'expired';
    const STATUS_VOID = 'void';

    const PAYMENT_METHOD_COD = 'cash_on_delivery';
    const PAYMENT_METHOD_PAYNAMICS = 'paynamics';
    const PAYMENT_METHOD_BANK_TRANSFER = 'bank_transfer';
    const PAYMENT_METHOD_CEBUANA_LHUILLIER = 'cebuana_lhuillier';

    /**
     * @var int
     */
    private $id;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Store\Cart
     */
    private $cart;

    /**
     * @var \AppBundle\Entity\Store\GiftWrap
     */
    private $giftWrap;

    /**
     * @var \AppBundle\Entity\Store\PromoCode
     */
    private $promoCode;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $uniqid;

    /**
     * @var string
     */
    private $ipAddress;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * @var string
     */
    private $status = self::STATUS_NEW;

    private $statusTransition = null;

    /**
     * @var float
     */
    private $totalPrice = 0;

    /**
     * @var float
     */
    private $finalTotalPrice = 0;

    /**
     * @var float
     */
    private $shippingFee = 0;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @var string
     */
    private $userFirstName;

    /**
     * @var string
     */
    private $userLastName;

    /**
     * @var string
     */
    private $userMiddleName;

    /**
     * @var string
     */
    private $giftWrapName;

    /**
     * @var float
     */
    private $giftWrapPrice;

    /**
     * @var string
     */
    private $giftWrapDescription;

    /**
     * @var json
     */
    private $data;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $lineItems;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $invoices;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $payments;

    /**
     * @var \AppBundle\Entity\User\BillingInformation
     */
    private $billing;

    /**
     * @var \AppBundle\Entity\User\ShippingInformation
     */
    private $shipping;

    /**
     * @var boolean
     */
    private $shippingSameAsBilling;


    public function __construct() 
    {
        $this->lineItems = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderNumber.
     *
     * @param string $orderNumber
     *
     * @return Order
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber.
     *
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set sessionId.
     *
     * @param string $sessionId
     *
     * @return Order
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId.
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set uniqid.
     *
     * @param string $uniqid
     *
     * @return Order
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid.
     *
     * @return string
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }

    /**
     * Set ipAddress.
     *
     * @param string $ipAddress
     *
     * @return Order
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus($status = self::STATUS_NEW)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusTransition.
     *
     * @param string $statusTransition
     *
     * @return Order
     */
    public function setStatusTransition($statusTransition)
    {
        $this->statusTransition = $statusTransition;

        return $this;
    }

    /**
     * Get statusTransition.
     *
     * @return string
     */
    public function getStatusTransition()
    {
        return $this->statusTransition;
    }

    /**
     * Set totalPrice.
     *
     * @param float $totalPrice
     *
     * @return Order
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice.
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set finalTotalPrice.
     *
     * @param float $finalTotalPrice
     *
     * @return Order
     */
    public function setFinalTotalPrice($finalTotalPrice)
    {
        $this->finalTotalPrice = $finalTotalPrice;

        return $this;
    }

    /**
     * Get finalTotalPrice.
     *
     * @return float
     */
    public function getFinalTotalPrice()
    {
        return $this->finalTotalPrice;
    }

    /**
     * Set shippingFee.
     *
     * @param float $shippingFee
     *
     * @return Order
     */
    public function setShippingFee($shippingFee)
    {
        $this->shippingFee = $shippingFee;

        return $this;
    }

    /**
     * Get shippingFee.
     *
     * @return float
     */
    public function getShippingFee()
    {
        return $this->shippingFee;
    }

    /**
     * Set paymentMethod.
     *
     * @param string $paymentMethod
     *
     * @return Order
     */
    public function setPaymentMethod($paymentMethod = self::PAYMENT_METHOD_PAYNAMICS)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod.
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set data.
     *
     * @param json $data
     *
     * @return Order
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return json
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Order
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userEmail.
     *
     * @param string $userEmail
     *
     * @return Order
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail.
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set userFirstName.
     *
     * @param string $userFirstName
     *
     * @return Order
     */
    public function setUserFirstName($userFirstName)
    {
        $this->userFirstName = $userFirstName;

        return $this;
    }

    /**
     * Get userFirstName.
     *
     * @return string
     */
    public function getUserFirstName()
    {
        return $this->userFirstName;
    }

    /**
     * Set userLastName.
     *
     * @param string $userLastName
     *
     * @return Order
     */
    public function setUserLastName($userLastName)
    {
        $this->userLastName = $userLastName;

        return $this;
    }

    /**
     * Get userLastName.
     *
     * @return string
     */
    public function getUserLastName()
    {
        return $this->userLastName;
    }

    /**
     * Set userMiddleName.
     *
     * @param string $userMiddleName
     *
     * @return Order
     */
    public function setUserMiddleName($userMiddleName)
    {
        $this->userMiddleName = $userMiddleName;

        return $this;
    }

    /**
     * Get userMiddleName.
     *
     * @return string
     */
    public function getUserMiddleName()
    {
        return $this->userMiddleName;
    }

    /**
     * Set giftWrapName.
     *
     * @param string $giftWrapName
     *
     * @return Order
     */
    public function setGiftWrapName($giftWrapName)
    {
        $this->giftWrapName = $giftWrapName;

        return $this;
    }

    /**
     * Get giftWrapName.
     *
     * @return string
     */
    public function getGiftWrapName()
    {
        return $this->giftWrapName;
    }

    /**
     * Set giftWrapPrice.
     *
     * @param float $giftWrapPrice
     *
     * @return Order
     */
    public function setGiftWrapPrice($giftWrapPrice)
    {
        $this->giftWrapPrice = $giftWrapPrice;

        return $this;
    }

    /**
     * Get giftWrapPrice.
     *
     * @return float
     */
    public function getGiftWrapPrice()
    {
        return $this->giftWrapPrice;
    }

    /**
     * Set giftWrapDescription.
     *
     * @param string $giftWrapDescription
     *
     * @return Order
     */
    public function setGiftWrapDescription($giftWrapDescription)
    {
        $this->giftWrapDescription = $giftWrapDescription;

        return $this;
    }

    /**
     * Get giftWrapDescription.
     *
     * @return string
     */
    public function getGiftWrapDescription()
    {
        return $this->giftWrapDescription;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Order ID: ' . $this->getId() : 'New Order';
    }

    /**
     * Set user.
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return Order
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cart.
     *
     * @param \AppBundle\Entity\Store\Cart $cart
     *
     * @return Order
     */
    public function setCart($cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart.
     *
     * @return \AppBundle\Entity\Store\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set giftWrap.
     *
     * @param \AppBundle\Entity\Store\GiftWrap $giftWrap
     *
     * @return Order
     */
    public function setGiftWrap($giftWrap)
    {
        $this->giftWrap = $giftWrap;

        return $this;
    }

    /**
     * Get giftWrap.
     *
     * @return \AppBundle\Entity\Store\GiftWrap
     */
    public function getGiftWrap()
    {
        return $this->giftWrap;
    }

    /**
     * Set promoCode.
     *
     * @param \AppBundle\Entity\Store\PromoCode $promoCode
     *
     * @return Order
     */
    public function setPromoCode($promoCode)
    {
        $this->promoCode = $promoCode;

        return $this;
    }

    /**
     * Get promoCode.
     *
     * @return \AppBundle\Entity\Store\PromoCode
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    public static function getStatusOptions()
    {
        return array(
            self::STATUS_NEW => 'New',
            self::STATUS_IN_CART => 'In Cart',
            self::STATUS_IN_CHECKOUT => 'In Checkout',
            self::STATUS_IN_PAYMENT_GATEWAY => 'In Payment Gateway',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_IN_PROGRESS => 'In Progress',
            self::STATUS_PAID => 'Paid',
            self::STATUS_COMPLETE => 'Complete',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_ERROR => 'Error',
            self::STATUS_EXPIRED => 'Expired',
            self::STATUS_VOID => 'Void',
        );
    }

    public function getDataFromRefByStatus()
    {
        return $this->isOnInitialState();
    }

    public static function getInitialStateStatuses()
    {
        $statuses = array(
            self::STATUS_NEW => 'New',
            self::STATUS_IN_CART => 'In Cart',
        );

        return $statuses;
    }

    public function isOnInitialState()
    {
        $statuses = self::getInitialStateStatuses();
        if (array_key_exists($this->status, $statuses)) {
            return true;
        }
        return false;
    }

    public static function getAllowedStatusesForPayment()
    {
        return array(
            //self::STATUS_NEW => 'New',
            //self::STATUS_IN_CART => 'In Cart',
            self::STATUS_IN_CHECKOUT => 'In Checkout',
            self::STATUS_IN_PAYMENT_GATEWAY => 'In Payment Gateway',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_IN_PROGRESS => 'In Progress',
            self::STATUS_PAID => 'Paid',
            self::STATUS_COMPLETE => 'Complete',
            //self::STATUS_CANCELLED => 'Cancelled',
            //self::STATUS_ERROR => 'Error',
            //self::STATUS_EXPIRED => 'Expired',
            //self::STATUS_VOID => 'Void',
        );
    }

    public static function getAllowedStatusesForInvoice()
    {
        return array(
            //self::STATUS_NEW => 'New',
            //self::STATUS_IN_CART => 'In Cart',
            self::STATUS_IN_CHECKOUT => 'In Checkout',
            self::STATUS_IN_PAYMENT_GATEWAY => 'In Payment Gateway',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_IN_PROGRESS => 'In Progress',
            self::STATUS_PAID => 'Paid',
            self::STATUS_COMPLETE => 'Complete',
            //self::STATUS_CANCELLED => 'Cancelled',
            //self::STATUS_ERROR => 'Error',
            //self::STATUS_EXPIRED => 'Expired',
            //self::STATUS_VOID => 'Void',
        );
    }

    public static function getAllowedForUpdateStatuses()
    {
        return array(
            self::STATUS_NEW => 'New',
            self::STATUS_IN_CART => 'In Cart',
            self::STATUS_IN_CHECKOUT => 'In Checkout',
            self::STATUS_IN_PAYMENT_GATEWAY => 'In Payment Gateway',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_IN_PROGRESS => 'In Progress',
            self::STATUS_PAID => 'Paid',
            //self::STATUS_COMPLETE => 'Complete',
            //self::STATUS_CANCELLED => 'Cancelled',
            //self::STATUS_ERROR => 'Error',
            //self::STATUS_EXPIRED => 'Expired',
            //self::STATUS_VOID => 'Void',
        );
    }

    public static function getLastStateStatuses()
    {
        return array(
            //self::STATUS_NEW => 'New',
            //self::STATUS_IN_CART => 'In Cart',
            //self::STATUS_IN_CHECKOUT => 'In Checkout',
            //self::STATUS_IN_PAYMENT_GATEWAY => 'In Payment Gateway',
            //self::STATUS_PENDING => 'Pending',
            //self::STATUS_IN_PROGRESS => 'In Progress',
            //self::STATUS_PAID => 'Paid',
            self::STATUS_COMPLETE => 'Complete',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_ERROR => 'Error',
            self::STATUS_EXPIRED => 'Expired',
            self::STATUS_VOID => 'Void',
        );
    }

    public function getStatusLabel()
    {
        if ( empty( $this->status ) ) {
            return null;
        }

        $options = self::getStatusOptions();
        if ( !array_key_exists( $this->status, $options ) ) {
            return null;
        }

        return $options[ $this->status ];
    }

    public static function getPaymentMethodOptions()
    {
        return array(
            self::PAYMENT_METHOD_COD => 'Cash on Delivery',
            // self::PAYMENT_METHOD_PAYNAMICS => 'Paynamics',
            self::PAYMENT_METHOD_BANK_TRANSFER => 'Bank Transfer',
            // self::PAYMENT_METHOD_CEBUANA_LHUILLIER => 'Cebuana Lhuillier',
        );
    }

    public function getPaymentMethodLabel()
    {
        if ( empty( $this->paymentMethod ) ) {
            return null;
        }

        $options = self::getPaymentMethodOptions();
        if ( !array_key_exists( $this->paymentMethod, $options ) ) {
            return null;
        }

        return $options[ $this->paymentMethod ];
    }

    /**
     * Set lineItems.
     *
     * @param array $lineItems
     */
    public function setLineItems($lineItems)
    {
        foreach ($lineItems as $lineItem) {
            $this->addLineItem($lineItem);
        }
    }

    /**
     * Get lineItems.
     *
     * @return AppBundle\Entity\Store\OrderLineItem[] $lineItems
     */
    public function getLineItems()
    {
        return $this->lineItems;
    }

    /**
     * Get lineItems.
     *
     * @return AppBundle\Entity\Store\OrderLineItem[] $lineItems
     */
    public function getProductIndexedLineItems()
    {
        if (count($this->lineItems) <= 0) {
            return array();
        }

        $lineItems = array();

        foreach ($this->lineItems as $lineItem) {
            $lineItems[$lineItem->getProduct()->getId()] = $lineItem;
        }

        return $lineItems;
    }

    /**
     * @param OrderLineItem
     */
    public function addLineItem(OrderLineItem $lineItem)
    {
        $lineItem->setOrder($this);

        $this->lineItems[] = $lineItem;
    }

    /**
     * @param OrderLineItem
     */
    public function removeLineItem(OrderLineItem $lineItem)
    {
        $this->lineItems->removeElement($lineItem);
    }

    /**
     * Set invoices.
     *
     * @param array $invoices
     */
    public function setInvoices($invoices)
    {
        foreach ($invoices as $invoice) {
            $this->addOrderInvoice($invoice);
        }
    }

    /**
     * Get invoices.
     *
     * @return AppBundle\Entity\Store\OrderInvoice[] $invoices
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @param OrderInvoice
     */
    public function addOrderInvoice(OrderInvoice $invoice)
    {
        $invoice->setOrder($this);

        $this->invoices[] = $invoice;
    }

    /**
     * @param OrderInvoice
     */
    public function removeOrderInvoice(OrderInvoice $invoice)
    {
        $this->invoices->removeElement($invoice);
    }

    /**
     * Set payments.
     *
     * @param array $payments
     */
    public function setPayments($payments)
    {
        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }
    }

    /**
     * Get payments.
     *
     * @return AppBundle\Entity\Store\OrderPayment[] $payments
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param OrderPayment
     */
    public function addPayment(OrderPayment $payment)
    {
        $payment->setOrder($this);

        $this->payments[] = $payment;
    }

    /**
     * @param OrderPayment
     */
    public function removePayment(OrderPayment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Set billing.
     *
     * @param AppBundle\Entity\User\BillingInformation $billing
     *
     * @return Order
     */
    public function setBilling($billing)
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * Get billing.
     *
     * @return string
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Set shipping.
     *
     * @param AppBundle\Entity\User\ShippingInformation $shipping
     *
     * @return Order
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping.
     *
     * @return string
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set shippingSameAsBilling.
     *
     * @param boolean $shippingSameAsBilling
     *
     * @return Order
     */
    public function setShippingSameAsBilling($shippingSameAsBilling)
    {
        $this->shippingSameAsBilling = $shippingSameAsBilling;

        return $this;
    }

    /**
     * Get shippingSameAsBilling.
     *
     * @return boolean
     */
    public function getShippingSameAsBilling()
    {
        return $this->shippingSameAsBilling;
    }

    public function hasValidPromoCode($promoCode, $orderTotal)
    {
        if (!$promoCode) {
            return false;
        }

        $currentDateTime = new \DateTime();

        if ($this->promoCode->getOrders() && $this->promoCode->getOrders()->count()) {
            return false;
        } elseif (!$promoCode->getEnabled()) {
            return false;
        } elseif ($promoCode->getStartDate()->format('U') > $currentDateTime->format('U')) {
            return false;
        } elseif ($promoCode->getEndDate() && $promoCode->getEndDate()->format('U') < $currentDateTime->format('U')) {
            return false;
        } else {
            if ($orderTotal < $promoCode->getMinimumOrderTotal()) {
                return false;
            }
        }

        return true;
    }

    public function getLineItemsPriceTotals($forceDataFromRef = false)
    {
        $totalPrice = 0;
        $finalTotalPrice = 0;
        foreach ($this->getLineItems() as $lineItem) {
            $lineItemPriceData = $lineItem->getPriceData($forceDataFromRef);
            $totalPrice = $totalPrice + $lineItemPriceData['subTotal'];
            $finalTotalPrice = $finalTotalPrice + $lineItemPriceData['finalSubTotal'];
        }

        return array(
            'totalPrice' => $totalPrice,
            'finalTotalPrice' => $finalTotalPrice,
        );
    }

    public function getTotals($forceDataFromRef = false)
    {
        $shippingFee = 0;
        $giftWrapPrice = 0;
        $discountTotal = 0;
        $totalPrice = 0;
        $finalTotalPrice = 0;
        
        $lineItemsTotal = $this->getLineItemsPriceTotals($forceDataFromRef);

        $lineItemsTotalPrice = $lineItemsTotal['totalPrice'];
        $lineItemsFinalTotalPrice = $lineItemsTotal['finalTotalPrice'];

        if ($forceDataFromRef || $this->isOnInitialState()) {
            // Get gift wrap price
            // $giftWrap = $this->getGiftWrap();
            // if ($giftWrap && $giftWrap->getPrice()) {
            //     $giftWrapPrice = $giftWrap->getPrice();
            // }
            // Get promo code discount
            $promoCode = $this->promoCode;
            if ($this->hasValidPromoCode($promoCode, $lineItemsFinalTotalPrice)) {
                $discountTotal = $this->promoCode->getValue();
            }
            // Get shipping fee
            // if (isset($this->storeSettings['free_shipping_fee'])) {
            //     $freeShippingFeeMin = $this->storeSettings['free_shipping_fee'];
            // }
            // if (isset($this->storeSettings['shipping_fee']) && !$this->isFreeShippingFee($finalTotalPrice, $freeShippingFeeMin)) {
            //     $shippingFee = $this->storeSettings['shipping_fee'];
            // }
            $totalPrice = ($lineItemsFinalTotalPrice + $shippingFee + $giftWrapPrice);
            $finalTotalPrice = ($lineItemsFinalTotalPrice + $shippingFee + $giftWrapPrice) - $discountTotal;
        } else {
            // Get gift wrap price
            // $giftWrapPrice = $this->getGiftWrapPrice();
            // Get promo code discount
            $discountTotal = $this->promoCodeValue;
            // Get shipping fee
            $shippingFee = $this->shippingFee;
            $totalPrice = $this->totalPrice;
            $finalTotalPrice = $this->finalTotalPrice;
        }

        return array(
            'lineItemsTotalPrice' => $lineItemsTotalPrice,
            'lineItemsFinalTotalPrice' => $lineItemsFinalTotalPrice,
            'shippingFee' => $shippingFee,
            'giftWrapPrice' => $giftWrapPrice,
            'discountTotal' => $discountTotal,
            'totalPrice' => $totalPrice,
            'finalTotalPrice' => $finalTotalPrice,
        );        
    }

    public function getLineItemsPriceTotalsData()
    {
        $total = 0;
        $totalDiscount = 0;
        $totalFinal = 0;
        $grandTotal = 0;
        $discountGrandTotal = 0;
        $finalGrandTotal = 0;
        $lineItemsPriceData = array();

        $currentTotal = 0;
        $currentTotalDiscount = 0;
        $currentTotalFinal = 0;
        $currentGrandTotal = 0;
        $currentDiscountGrandTotal = 0;
        $currentFinalGrandTotal = 0;

        $lineItems = $this->lineItems;
        if (count($lineItems) <= 0) {
            return array(
                'current' => array(
                    'grandTotal' => $currentGrandTotal,
                    'discountGrandTotal' => $currentDiscountGrandTotal,
                    'finalGrandTotal' => $currentFinalGrandTotal,
                    'total' => $currentTotal,
                    'totalDiscount' => $currentTotalDiscount,
                    'totalFinal' => $currentTotalFinal,
                ),
                'grandTotal' => $grandTotal,
                'discountGrandTotal' => $discountGrandTotal,
                'finalGrandTotal' => $finalGrandTotal,
                'total' => $total,
                'totalDiscount' => $totalDiscount,
                'totalFinal' => $totalFinal,
                'lineItemsPriceData' => $lineItemsPriceData
            );
        }

        foreach ($lineItems as $lineItem) {
            $priceData = $lineItem->getPriceData();
            $lineItemsPriceData[$lineItem->getId()] = $priceData;
            
            $grandTotal = $grandTotal + $priceData['subTotal'];
            $discountGrandTotal = $discountGrandTotal + $priceData['discountSubTotal'];
            $finalGrandTotal = $finalGrandTotal + $priceData['finalSubTotal'];

            $total = $total + $priceData['price'];
            $totalDiscount = $totalDiscount + $priceData['discount'];
            $totalFinal = $totalFinal + $priceData['final'];

            // Current
            $currentGrandTotal = $currentGrandTotal + $priceData['current']['subTotal'];
            $currentDiscountGrandTotal = $currentDiscountGrandTotal + $priceData['current']['discountSubTotal'];
            $currentFinalGrandTotal = $currentFinalGrandTotal + $priceData['current']['finalSubTotal'];

            $currentTotal = $currentTotal + $priceData['current']['price'];
            $currentTotalDiscount = $currentTotalDiscount + $priceData['current']['discount'];
            $currentTotalFinal = $currentTotalFinal + $priceData['current']['final'];
        }

        return array(
            'current' => array(
                'grandTotal' => $currentGrandTotal,
                'discountGrandTotal' => $currentDiscountGrandTotal,
                'finalGrandTotal' => $currentFinalGrandTotal,
                //
                'total' => $currentTotal,
                'totalDiscount' => $currentTotalDiscount,
                'totalFinal' => $currentTotalFinal,
            ),
            'grandTotal' => $grandTotal,
            'discountGrandTotal' => $discountGrandTotal,
            'finalGrandTotal' => $finalGrandTotal,
            //
            'total' => $total,
            'totalDiscount' => $totalDiscount,
            'totalFinal' => $totalFinal,
            'lineItemsPriceData' => $lineItemsPriceData,
        );
    }

    public function getEnabledLineItems()
    {
        if (count($this->lineItems) <= 0) {
            return array();
        }

        return $this->getLineItems()->filter(function(OrderLineItem $lineItem) {
            return $lineItem->getProduct()->getEnabled();
        });
    }

    public static function getStatusTransitionLabels()
    {
        /*
            create:
                from: [new]
                to: in_cart
            checkout:
                from: [new, in_cart]
                to: in_checkout
            payment_gateway:
                from: [in_checkout]
                to: in_payment_gateway
            pending:
                from: [new, in_cart, in_checkout]
                to: pending
            in_progress:
                from: [in_checkout]
                to: in_progress
            paid:
                from: [in_checkout, pending, in_progress, awaiting_confirmation]
                to: paid
            complete:
                from: [paid]
                to: complete
            cancelled:
                from: [new, in_cart, in_checkout, pending, in_progress]
                to: cancelled
            expired:
                from: [new, in_cart, in_checkout]
                to: expired
            error:
                from: [in_checkout]
                to: error
            void:
                from: [in_checkout, pending, in_progress]
                to: void
        */
        return array(
            'create' => 'In Cart',
            'checkout' => 'Checkout',
            'payment_gateway' => 'Payment Gateway',
            'pending' => 'Pending',
            'in_progress' => 'In Progress',
            'paid' => 'Paid',
            'complete' => 'Complete',
            'cancelled' => 'Cancelled',
            'expired' => 'Expired',
            'error' => 'Error',
            'void' => 'Void',
        );
    }
}
