<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * DiscountUsage
 */
class DiscountUsage implements SoftDeleteInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\Order
     */
    private $order;

    /**
     * @var \AppBundle\Entity\Store\OrderLineItem
     */
    private $orderLineItem;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;

    /**
     * @var \AppBundle\Entity\Store\Discount
     */
    private $discount;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Discount Usage ID: ' . $this->getId() : 'New Discount Usage';
    }

    /**
     * Set order.
     *
     * @param \AppBundle\Entity\Store\Order $order
     *
     * @return DiscountUsage
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \AppBundle\Entity\Store\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set orderLineItem.
     *
     * @param \AppBundle\Entity\Store\OrderLineItem $orderLineItem
     *
     * @return DiscountUsage
     */
    public function setOrderLineItem($orderLineItem)
    {
        $this->orderLineItem = $orderLineItem;

        return $this;
    }

    /**
     * Get orderLineItem.
     *
     * @return \AppBundle\Entity\Store\OrderLineItem
     */
    public function getOrderLineItem()
    {
        return $this->orderLineItem;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return DiscountUsage
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set discount.
     *
     * @param \AppBundle\Entity\Store\Discount $discount
     *
     * @return DiscountUsage
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return \AppBundle\Entity\Store\Discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set user.
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return DiscountUsage
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
