<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\DateRangeInterface;
use AppBundle\Entity\EnableDisableInterface;
use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\DateRangeTrait;
use AppBundle\Entity\Traits\EnableDisableTrait;
use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * Discount
 */
class Discount implements DateRangeInterface, EnableDisableInterface, SoftDeleteInterface, TimestampableInterface
{
    const TYPE_FIXED_AMOUNT = 'fixed_amount';
    const TYPE_PERCENTAGE = 'percentage';

    use DateRangeTrait;
    use EnableDisableTrait;
    use SoftDeleteTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type = self::TYPE_FIXED_AMOUNT;

    /**
     * @var float
     */
    private $value = 0;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Discount
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Discount
     */
    public function setType($type = self::TYPE_FIXED_AMOUNT)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value.
     *
     * @param float $value
     *
     * @return Discount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'New Discount';
    }

    public static function getTypeOptions()
    {
        return array(
            self::TYPE_FIXED_AMOUNT => 'Fixed Amount',
            self::TYPE_PERCENTAGE => 'Percentage',
        );
    }

    public function getTypeLabel()
    {
        if ( empty( $this->type ) ) {
            return null;
        }

        $options = self::getTypeOptions();
        if ( !array_key_exists( $this->type, $options ) ) {
            return null;
        }

        return $options[ $this->type ];
    }
}
