<?php

namespace AppBundle\Entity\Store;

use Doctrine\ORM\Mapping as ORM;

use AppBundle\Entity\SoftDeleteInterface;
use AppBundle\Entity\SortableInterface;
use AppBundle\Entity\TimestampableInterface;

use AppBundle\Entity\Traits\SoftDeleteTrait;
use AppBundle\Entity\Traits\SortableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;

/**
 * ProductAttributeSize
 */
class ProductAttributeSize implements SoftDeleteInterface, SortableInterface, TimestampableInterface
{
    use SoftDeleteTrait;
    use SortableTrait;
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Store\ProductAttribute
     */
    private $attribute;

    /**
     * @var \AppBundle\Entity\Store\Size
     */
    private $size;

    /**
     * @var \AppBundle\Entity\Store\Product
     */
    private $product;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? 'Product Attribute Size: ' . $this->size : 'New Product Attribute Size';
    }

    /**
     * Set attribute.
     *
     * @param \AppBundle\Entity\Store\ProductAttribute $attribute
     *
     * @return ProductAttributeSize
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \AppBundle\Entity\Store\ProductAttribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Store\Product $product
     *
     * @return ProductAttributeSize
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Store\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set size.
     *
     * @param \AppBundle\Entity\Store\Size $size
     *
     * @return ProductAttributeSize
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return \AppBundle\Entity\Store\Size
     */
    public function getSize()
    {
        return $this->size;
    }
}
