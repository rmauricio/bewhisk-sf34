<?php

namespace AppBundle\Entity;

/**
 * DateRangeInterface
 */
interface DateRangeInterface
{
	/**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return DateRangeInterface
     */
    public function setStartDate($startDate);

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate();

	/**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return DateRangeInterface
     */
    public function setEndDate($endDate);

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate();
}