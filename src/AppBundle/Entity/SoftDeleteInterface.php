<?php

namespace AppBundle\Entity;

/**
 * SoftDeleteInterface
 */
interface SoftDeleteInterface
{
	/**
     * Set deleted.
     *
     * @param bool $deleted
     *
     * @return SoftDeleteInterface
     */
    public function setDeleted($deleted);

    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted();
}