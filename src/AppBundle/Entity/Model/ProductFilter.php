<?php

namespace AppBundle\Entity\Model;

class ProductFilter
{
    const SORT_PUBLICATION_START_DATE = 'publication_start_date';
    const SORT_PRICE = 'price';
    const SORT_NAME = 'created_at';
    const PRICE_LTE_300 = 'LTE_300';
    const PRICE_LTE_500 = 'LTE_500';
    const PRICE_LTE_1000 = 'LTE_1000';
    const PRICE_LTE_2500 = 'LTE_2500';
    const PRICE_GTE_2501 = 'GTE_2501';

    protected $brand;

    protected $price;

    protected $color;

    protected $size;

    protected $theme;

    protected $sort = self::SORT_PUBLICATION_START_DATE . '__DESC';

    protected $sortField;

    protected $sortOrder;

    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    public function getTheme()
    {
        return $this->theme;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSortField($sortField)
    {
        $this->sortField = $sortField;

        return $this;
    }

    public function getSortField()
    {
        return $this->sortField;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public static function getSortOptions()
    {
        return array(
            self::SORT_PUBLICATION_START_DATE . '__DESC' => 'Newest',
            self::SORT_PUBLICATION_START_DATE . '__ASC' => 'Oldest',
            self::SORT_PRICE . '__ASC' => 'Lowest Price',
            self::SORT_PRICE . '__DESC' => 'Highest Price',
            self::SORT_NAME . '__ASC' => 'Alphabetical',
        );
    }

    public static function getPriceOptions()
    {
        return array(
            self::PRICE_LTE_300 => 'Php 300 and below',
            self::PRICE_LTE_500 => 'Php 500 and below',
            self::PRICE_LTE_1000 => 'Php 1000 and below',
            self::PRICE_LTE_2500 => 'Php 2500 and below',
            self::PRICE_GTE_2501 => 'Php 2,501 and up',
        );
    }

    public static function getSortOptionSorting($key = null)
    {
        $options = array(
            self::SORT_PUBLICATION_START_DATE . '__DESC' => array(
                'publicationStartDate' => 'DESC'
            ),
            self::SORT_PUBLICATION_START_DATE . '__ASC' => array(
                'publicationStartDate' => 'ASC'
            ),
            self::SORT_PRICE . '__ASC' => array(
                'price' => 'ASC'
            ),
            self::SORT_PRICE . '__DESC' => array(
                'publicationStartDate' => 'DESC'
            ),
            self::SORT_NAME . '__ASC' => array(
                'name' => 'ASC'
            ),
        );

        if (!empty($key)) {
            return $options[$key];
        }

        return $options;
    }

    public static function getPriceOptionFilters($key = null)
    {
        // @todo: Should be placed somewhere else to make it configurable
        $options = array(
            self::PRICE_LTE_300 => array(
                'value' => 300,
                'operator' => '<=',
            ),
            self::PRICE_LTE_500 => array(
                'value' => 500,
                'operator' => '<=',
            ),
            self::PRICE_LTE_1000 => array(
                'value' => 1000,
                'operator' => '<=',
            ),
            self::PRICE_LTE_2500 => array(
                'value' => 2500,
                'operator' => '<=',
            ),
            self::PRICE_GTE_2501 => array(
                'value' => 2501,
                'operator' => '>=',
            ),
        );

        if (!empty($key)) {
            return $options[$key];
        }

        return $options;
    }
}