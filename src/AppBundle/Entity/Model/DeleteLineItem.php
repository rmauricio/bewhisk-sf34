<?php

namespace AppBundle\Entity\Model;

class DeleteLineItem
{
    protected $item;

    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    public function getItem()
    {
        return $this->item;
    }
}