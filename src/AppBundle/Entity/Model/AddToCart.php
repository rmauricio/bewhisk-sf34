<?php

namespace AppBundle\Entity\Model;

class AddToCart
{
    protected $color;

    protected $size;

    protected $quantity = 1;

    protected $product;

    protected $parentProduct;

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;

        return $this;
    }

    public function getParentProduct()
    {
        return $this->parentProduct;
    }
}