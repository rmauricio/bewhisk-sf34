<?php

namespace AppBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;

class ProductsList
{
    protected $productsVariations;

    public function __construct()
    {
        $this->productsVariations = new ArrayCollection();
    }

    public function getProductVariations()
    {
        return $this->productsVariations;
    }
}