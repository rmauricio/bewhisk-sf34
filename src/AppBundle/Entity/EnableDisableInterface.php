<?php

namespace AppBundle\Entity;

/**
 * EnableDisableInterface
 */
interface EnableDisableInterface
{
	/**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return EnableDisableInterface
     */
    public function setEnabled($enabled);

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled();
}