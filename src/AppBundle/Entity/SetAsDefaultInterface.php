<?php

namespace AppBundle\Entity;

/**
 * SetAsDefaultInterface
 */
interface SetAsDefaultInterface
{
	/**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return SetAsDefaultInterface
     */
    public function setIsDefault($isDefault);

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault();
}