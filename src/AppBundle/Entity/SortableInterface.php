<?php

namespace AppBundle\Entity;

/**
 * SortableInterface
 */
interface SortableInterface
{
	/**
     * Set position.
     *
     * @param int $position
     *
     * @return SortableInterface
     */
    public function setPosition($position);

    /**
     * Get position.
     *
     * @return bool
     */
    public function getPosition();
}