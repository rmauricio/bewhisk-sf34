<?php

namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBUserProvider;
use FOS\UserBundle\Model\User;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\Connect\AccountConnectorInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use AppBundle\Security\Core\Exception\EmailExistException;
use AppBundle\Security\Core\Exception\PermissionDeclinedException;
use AppBundle\Security\Core\Exception\FacebookUidExistException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntityConstraint;
use Application\Sonata\UserBundle\Entity\User as AppUser;

class FOSUBUserProvider extends BaseFOSUBUserProvider
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    protected $tokenGenerator;

    protected $validator;

    protected $session;

    /**
     * @var array
     */
    protected $properties = array(
        'identifier' => 'id',
    );

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager fOSUB user provider
     * @param array                $properties  property mapping
     */
    public function __construct(UserManagerInterface $userManager, array $properties)
    {
        $this->userManager = $userManager;
        $this->properties = array_merge($this->properties, $properties);
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        // Compatibility with FOSUserBundle < 2.0
        if (class_exists('FOS\UserBundle\Form\Handler\RegistrationFormHandler')) {
            return $this->userManager->loadUserByUsername($username);
        }

        return $this->userManager->findUserByUsername($username);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $facebookUid = $response->getId();

        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $facebookUid));

        if (null === $user || null === $facebookUid) {
            // throw new AccountNotLinkedException(sprintf("User '%s' not found.", $username));
            $user = $this->createUser($response);
        }

        return $user;
    }

    protected function createUser($response)
    {
        try {
            $user = $this->userManager->createUser();
            $user->setPassword(substr($this->tokenGenerator->generateToken(), 0, 20));
            $user->setEnabled(true);
            $user->setCreatedVia(AppUser::CREATED_VIA_FACEBOOK);
    
            foreach($response->getPaths() as $field => $map) {
                if (!$this->accessor->isWritable($user, $field)) {
                    continue;
                }
                
                if (is_array($map)) {
                    $value = $this->accessor->getValue($response, $field);
                } else {
                    $value = $this->accessor->getValue($response, $map);
                }
    
                $this->accessor->setValue($user, $field, $value);
            }
    
            if ($response->getResourceOwner()->getName() == 'facebook' && !$user->getEmail()) {
                throw new PermissionDeclinedException('We need your facebook email to link it on your account.');
            }
    
            $uniqueEmailConstraint = new UniqueEntityConstraint(['fields' => ['email'], 'message' => 'Facebook Email already registered.']);
            
            $errors = $this->validator->validate($user, $uniqueEmailConstraint);
            if (count($errors) > 0) {
                throw new EmailExistException($errors[0]->getMessage());
            }

            // Save and uthenticate user
            $this->userManager->save($user);
            // $this->authenticateUser($user, new Response());

            return $user;
        } catch (\Exception $e) {
            $username = $response->getUsername();
            throw new AccountNotLinkedException(sprintf("User '%s' not found.", $username));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Expected an instance of FOS\UserBundle\Model\User, but got "%s".', get_class($user)));
        }

        $property = $this->getProperty($response);

        // Symfony <2.5 BC
        if (method_exists($this->accessor, 'isWritable') && !$this->accessor->isWritable($user, $property)
            || !method_exists($this->accessor, 'isWritable') && !method_exists($user, 'set'.ucfirst($property))) {
            throw new \RuntimeException(sprintf("Class '%s' must have defined setter method for property: '%s'.", get_class($user), $property));
        }

        $facebookUid = $response->getId();
        $userCheck = $this->userManager->findUserBy(array($property => $facebookUid));

        if ($userCheck && $userCheck->getEmail() != $user->getEmail()) {
            throw new FacebookUidExistException('Your facebook account was already linked to a different account.');
        }

        foreach ($response->getPaths() as $field => $map) {
            if (in_array($field, array("username", "usernameCanonical", "email", "emailCanonical"))) {
                continue;
            } elseif (!$this->accessor->isWritable($user, $field)) {
                continue;
            }
            
            if (is_array($map)) {
                $value = $this->accessor->getValue($response, $field);
            } else {
                $value = $this->accessor->getValue($response, $map);
            }

            $this->accessor->setValue($user, $field, $value);
        }

        if ($response->getResourceOwner()->getName() == 'facebook' && !$user->getEmail()) {
            throw new PermissionDeclinedException('We need your facebook email to link it on your account.');
        }

        // if (null !== $previousUser = $this->userManager->findUserBy(array($property => $facebookUid))) {
        //     $this->disconnect($previousUser, $response);
        // }

        // $this->accessor->setValue($user, $property, $username);

        $this->userManager->updateUser($user);
    }

    /**
     * Disconnects a user.
     *
     * @param UserInterface         $user
     * @param UserResponseInterface $response
     */
    public function disconnect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);

        $this->accessor->setValue($user, $property, null);
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        // Compatibility with FOSUserBundle < 2.0
        if (class_exists('FOS\UserBundle\Form\Handler\RegistrationFormHandler')) {
            return $this->userManager->refreshUser($user);
        }

        $identifier = $this->properties['identifier'];
        if (!$user instanceof User || !$this->accessor->isReadable($user, $identifier)) {
            throw new UnsupportedUserException(sprintf('Expected an instance of FOS\UserBundle\Model\User, but got "%s".', get_class($user)));
        }

        $userId = $this->accessor->getValue($user, $identifier);
        if (null === $user = $this->userManager->findUserBy(array($identifier => $userId))) {
            throw new UsernameNotFoundException(sprintf('User with ID "%d" could not be reloaded.', $userId));
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        $userClass = $this->userManager->getClass();

        return $userClass === $class || is_subclass_of($class, $userClass);
    }

    /**
     * Gets the property for the response.
     *
     * @param UserResponseInterface $response
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getProperty(UserResponseInterface $response)
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        return $this->properties[$resourceOwnerName];
    }

    public function setTokenGenerator($tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    public function setValidator($validator)
    {
        $this->validator = $validator;
    }
}
