<?php

namespace AppBundle\Security\Core\Exception;

use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;

/**
 * PermissionDeclinedException
 */
class PermissionDeclinedException extends AccountNotLinkedException
{
}