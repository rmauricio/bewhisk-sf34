<?php

namespace AppBundle\Security\Core\Exception;

use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;

/**
 * EmailExistException
 */
class EmailExistException extends AccountNotLinkedException
{
}