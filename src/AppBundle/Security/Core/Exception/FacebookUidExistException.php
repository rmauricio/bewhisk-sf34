<?php

namespace AppBundle\Security\Core\Exception;

use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;

/**
 * FacebookUidExistException
 */
class FacebookUidExistException extends AccountNotLinkedException
{
}