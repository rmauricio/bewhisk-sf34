<?php

namespace AppBundle\Provider;

class BaseProductAttributeProvider
{
	protected $productAttributesManager;

	protected $doctrine;

	public function setProductAttributesManager( $productAttributesManager )
	{
		$this->productAttributesManager = $productAttributesManager;
	}

	public function setDoctrine( $doctrine )
	{
		$this->doctrine = $doctrine;
	}
}