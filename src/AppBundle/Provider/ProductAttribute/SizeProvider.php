<?php

namespace AppBundle\Provider\ProductAttribute;

use AppBundle\Provider\BaseProductAttributeProvider;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Store\ProductAttributeSize;

class SizeProvider extends BaseProductAttributeProvider
{
    public function buildAttributeForm(FormMapper $formMapper, Product $product): void
    {
        // set default attribute entry
        /*if ( !$product->getSizes()->count() ) {
            $sizeAttribute = $this->productAttributesManager->findOneBy( array( 'code' => 'size' ) );
            if ( !$sizeAttribute ) {
                throw new \Exception( "Size attribute does not exist" );
            }
            $productAttributeSize = new ProductAttributeSize();
            $productAttributeSize->setAttribute( $sizeAttribute );
            $product->addSize( $productAttributeSize );
        }*/

        $em = $this->doctrine->getRepository('AppBundle\Entity\Store\Size');
        $sizeQuery = $em->createQueryBuilder('s')
                    ->select('s')
                    // ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("s.enabled = true")
                    ->orderBy('s.name', 'ASC');

    	$formMapper
	    	->with('Attributes', ['class' => 'col-md-4'])
	    		/*->add('sizes', CollectionType::class, array(
                        'label' => 'Size',
                        'by_reference' => false,
                        'btn_add' => false,
                        'type_options' => array(
                            // Prevents the "Delete" option from being displayed
                            'delete' => true,
                            'delete_options' => array(
                                // You may otherwise choose to put the field but hide it
                                // 'type' => HiddenType::class,
                                // In that case, you need to fill in the options as well
                                'type_options' => array(
                                    'mapped'   => false,
                                    'required' => false,
                                )
                            ),
                        )
                    ),
                    array(
                        //'edit' => 'inline',
                        'inline' => 'standard',
                        // 'sortable' => 'position',
                        'limit' => 1,
                        'edit' => 'standard',
                        // 'inline' => 'table',
                        // 'sortable' => 'position',
                        // 'link_parameters' => ['context' => $context],
                        'allow_delete' => true,
                        'admin_code' => 'app.store.admin.product_attribute_color',
                    )
                )*/
                ->add('size', ModelType::class, array('required' => false, 'btn_add' => false, 'query' => $sizeQuery))
            ->end()
        ;
    }

    public function configureShowFields(ShowMapper $showMapper, Product $product): void
    {
        $showMapper
            ->add('size', 'text', array( 'label' => 'Size' ))
        ;
    }

    public function configureListFields(ListMapper $listMapper): void
    {
    	$listMapper
    		->add('size.name', null, array( 'label' => 'Size' ))
    	;
    }
}