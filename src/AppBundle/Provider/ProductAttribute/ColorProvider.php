<?php

namespace AppBundle\Provider\ProductAttribute;

use AppBundle\Provider\BaseProductAttributeProvider;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Store\ProductAttributeColor;

class ColorProvider extends BaseProductAttributeProvider
{
    public function buildAttributeForm(FormMapper $formMapper, Product $product): void
    {
        // set default attribute entry
        /*if ( !$product->getColors()->count() ) {
        	$colorAttribute = $this->productAttributesManager->findOneBy( array( 'code' => 'color' ) );
        	if ( !$colorAttribute ) {
        		throw new \Exception( "Color attribute does not exist" );
        	}
        	$productAttributeColor = new ProductAttributeColor();
        	$productAttributeColor->setAttribute( $colorAttribute );
            $product->addColor( $productAttributeColor );
        }*/

        /*$em = $this->doctrine->getRepository('AppBundle\Entity\Category');
        $categoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    // ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'color'")
                    ->andWhere("c.parent IS NOT NULL")
                    ->orderBy('c.position, c.name', 'ASC');*/

        $em = $this->doctrine->getRepository('AppBundle\Entity\Store\Color');
        $colorQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    // ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.enabled = true")
                    ->orderBy('c.name', 'ASC');

    	$formMapper
	    	->with('Attributes', ['class' => 'col-md-4'])
	    		/*->add('colors', CollectionType::class, array(
                        'label' => 'Color',
                        'by_reference' => false,
                        'btn_add' => false,
                        'type_options' => array(
                            // Prevents the "Delete" option from being displayed
                            'delete' => true,
                            'delete_options' => array(
                                // You may otherwise choose to put the field but hide it
                                // 'type' => HiddenType::class,
                                // In that case, you need to fill in the options as well
                                'type_options' => array(
                                    'mapped'   => false,
                                    'required' => false,
                                )
                            ),
                        )
                    ),
                    array(
                        //'edit' => 'inline',
                        'inline' => 'standard',
                        // 'sortable' => 'position',
                        'limit' => 1,
                        'edit' => 'standard',
                        // 'inline' => 'table',
                        // 'sortable' => 'position',
                        // 'link_parameters' => ['context' => $context],
                        'allow_delete' => true,
                        'admin_code' => 'app.store.admin.product_attribute_color',
                    )
                )*/
                // ->add('color', ModelType::class, array(
                //     'required' => false,
                //     'query' => $categoriesQuery,
                //     'required' => false,
                //     'property' => 'name',
                //     'multiple' => true,
                //     'btn_add' => false,
                // ))
                ->add('color', ModelType::class, array('required' => false, 'btn_add' => false, 'query' => $colorQuery))
            ->end()
        ;
    }

    public function configureShowFields(ShowMapper $showMapper, Product $product): void
    {
    	$showMapper
    		->add('color', null, array( 'label' => 'Color' ))
    	;
    }

    public function configureListFields(ListMapper $listMapper): void
    {
    	$listMapper
    		->add('color.name', null, array( 'label' => 'Color' ))
    	;
    }
}