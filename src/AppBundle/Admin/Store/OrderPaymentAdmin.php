<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Store\Order;
use AppBundle\Entity\Store\OrderInvoice;
use AppBundle\Entity\Store\OrderPayment;

class OrderPaymentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId() && $subject->getOrderNumber()) {
            $formMapper
                ->tab('General')
                    ->with('General', ['class' => 'col-md-4'])
                        ->add('orderNumberData', TextType::class, array('mapped' => false, 'required' => false, 'attr' => array('label' => 'Order Number', 'readonly' => true), 'data' => $subject->getOrderNumber()))
                    ->end()
                ->end()
            ;
        }

        $formMapper
            ->add('order')
            ->add('amountPaid', MoneyType::class, array('scale' => 4, 'currency' => 'PHP'))
            ->add('reference', TextType::class, array('required' => false))
            ->add('comment', TextareaType::class, array('required' => false, 'label' => 'Comment', 'attr' => array('rows' => 5)))
        ;
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('order')
            ->add('amountPaid')
            ->add('reference')
            ->add('comment')
            ->add('updatedAt', null, array( 'label' => 'Last Update' ))
            ->add('createdAt', null, array( 'label' => 'Date Created' ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
            ->add('order.id', null, array('label' => 'Order ID'))
            ->add('order.orderNumber', null, array('label' => 'Order Number'))
            ->add('amountPaid', null, array('label' => 'Username'))
            ->add('reference', null, array('label' => 'User Email'))
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
                    'field_options' => array(
                        'dp_side_by_side'    => false,
                        //'dp_min_view_mode'   => 'days',
                        'dp_use_current'     => false,
                        'format'             => 'yyyy-MM-dd HH:mm'
                    )
                )
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('order.orderNumber')
            ->add('amountPaid')
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Order ID' => 'order.id',
            'Order Number' => 'order.orderNumber',
            'Amount Paid' => 'amountPaid',
            'Reference' => 'reference',
            'Comment' => 'comment',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}