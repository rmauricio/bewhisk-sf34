<?php

namespace AppBundle\Admin\Store;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Sonata\Form\Type\CollectionType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Sonata\AdminBundle\Route\RouteCollection;

use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Store\ProductAttributeColor;

class ProductVariationAdmin extends ProductAdmin
{
    protected $parentAssociationMapping = 'parent';

    protected $baseRouteName = 'product_variation';

    protected $baseRoutePattern = 'product-variation';

    protected $attributeProviders;

    protected $serializer;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName, $productTypeManager, $productStockManager, $productTypeAdmin)
    {
        parent::__construct($code, $class, $baseControllerName, $productTypeManager, $productStockManager, $productTypeAdmin);

        $this->attributeProviders = array();
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        
        // Clone parent
        $parent = $this->getParent()->getSubject();
        // $parentClone = clone $parent;

        // $variationSuffix = md5( $parentClone->getId() . '|' . $parentClone->getVariations()->count() . '|' . strtotime("now") );

        $instance->setVariations( new \Doctrine\Common\Collections\ArrayCollection );
        $instance->setDiscounts( new \Doctrine\Common\Collections\ArrayCollection );

        // $instanceJson = $this->serializer->serialize( $instance, 'json' );
        // $instanceArray = json_decode( $instanceJson, true );
        // $instanceArray[ 'id' ] = null;
        // $instanceArray[ 'name' ] = $instanceArray[ 'name' ] . ' - ' . $variationSuffix;
        // $instanceArray[ 'sku' ] = $instanceArray[ 'sku' ] . '_' . $variationSuffix;
        // $instanceArray[ 'slug' ] = $instanceArray[ 'slug' ] . '-' . $variationSuffix;
        // $instanceArray[ 'color' ] = null;
        // $instanceArray[ 'size' ] = null;
        $name = $parent->getName();
        $sku = $parent->getSku();
        $price = $parent->getPrice();
        $image = $parent->getImage();
        $thumbnail = $parent->getThumbnail();
        $gallery = $parent->getGallery();
        $categories = $parent->getCategories();
        
        $instance->setName($name);
        $instance->setSku($sku);
        $instance->setPrice($price);
        $instance->setImage($image);
        $instance->setThumbnail($thumbnail);
        $instance->setGallery($gallery);
        $instance->setCategories($categories);

        // $instance = $this->serializer->deserialize( json_encode( $instanceArray ), get_class( $instance ), 'json' );
        $instance->setParent($parent);
        // dump($instance);die();
        
        return $instance;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
        	->addIdentifier('name')
        	->addIdentifier('sku', null, array('label' => 'SKU'))
        	->add('price', null, array())
        	->add('stock.value', null, array('label' => 'Stock'))
        ;

        $parent = $this->getParent()->getSubject();
        $productAttributes = $parent->getType()->getProductAttributes();
        if ( $productAttributes->count() > 0 ) {
            $productAttributesJson = $this->serializer->serialize( $productAttributes, 'json' );
            $productAttributeCodes = array_column( json_decode( $productAttributesJson, true ), 'code' );

            foreach ( $this->attributeProviders as $key => $provider ) {
                $keyArray = explode( '.', $key );
                // skip build of attribute form if not applicable to the product type
                if ( !in_array( $keyArray[ ( count( $keyArray ) - 1 ) ], $productAttributeCodes ) ) {
                    continue;
                }
                $provider->configureListFields( $listMapper );
            }
        }

        $listMapper
            ->add('position', null, array('editable' => true))
            ->add('enabled', null, array('editable' => true))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
                'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        parent::configureFormFields( $formMapper );

        // set parent and type
        // $subject->setParent( $this->getParent()->getSubject() );
        // $subject->setType( $this->getParent()->getSubject()->getType() );

        $productAttributes = $subject->getType()->getProductAttributes();
        if ( $productAttributes->count() > 0 ) {
            $productAttributesJson = $this->serializer->serialize( $productAttributes, 'json' );
            $productAttributeCodes = array_column( json_decode( $productAttributesJson, true ), 'code' );

            foreach ( $this->attributeProviders as $key => $provider ) {
                $keyArray = explode( '.', $key );
                // skip build of attribute form if not applicable to the product type
                if ( !in_array( $keyArray[ ( count( $keyArray ) - 1 ) ], $productAttributeCodes ) ) {
                    continue;
                }
                $provider->buildAttributeForm( $formMapper, $subject );
            }
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $subject = $this->getSubject();

        parent::configureShowFields( $showMapper );

        $productAttributes = $subject->getType()->getProductAttributes();
        if ( $productAttributes->count() > 0 ) {
            $productAttributesJson = $this->serializer->serialize( $productAttributes, 'json' );
            $productAttributeCodes = array_column( json_decode( $productAttributesJson, true ), 'code' );

            foreach ( $this->attributeProviders as $key => $provider ) {
                $keyArray = explode( '.', $key );
                // skip build of attribute form if not applicable to the product type
                if ( !in_array( $keyArray[ ( count( $keyArray ) - 1 ) ], $productAttributeCodes ) ) {
                    continue;
                }
                $provider->configureShowFields( $showMapper, $subject );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    /*protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id      = $admin->getRequest()->get('id');
        $product = $this->getObject($id);

        $menu->addChild(
            $this->trans('product.sidemenu.link_product_edit', array()),
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
        );

        if ($product->getParent() && $this->getCode() == 'app.store.admin.product') {
            $menu->addChild(
                $this->trans('product.sidemenu.link_add_variation', array()),
                array('uri' => $admin->generateUrl('app.store.admin.product_variation.create', array('id' => $id)))
            );

            $menu->addChild(
                $this->trans('product.sidemenu.view_variations'),
                array('uri' => $admin->generateUrl('app.store.admin.product_variation.list', array('id' => $id)))
            );
        }
    }*/

    public function setProductAttrbiteProviders( $attributeProviders )
    {
        $this->attributeProviders = $attributeProviders;
    }

    public function setSerializer( $serializer )
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        // $query = parent::createQuery($context);
        $query = $this->getModelManager()->createQuery($this->getClass());

        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        //$query->andWhere(
            /*$query->expr()->orX(
                $query->expr()->isNotNull($query->getRootAliases()[0] . '.parent'),
                $query->expr()->gt($query->getRootAliases()[0] . '.variationCount', 0)
            )*/
            // $query->expr()->eq($query->getRootAliases()[0] . '.parent', $this->getParent()->getSubject()->)
        //);
        // $query->setParameter('my_param', 'my_value');
        return $query;
    }
}