<?php

namespace AppBundle\Admin\Store;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Sonata\Form\Type\CollectionType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\AdminBundle\Form\Type\AdminType;

use AppBundle\Entity\Store\Product;
use AppBundle\Entity\Store\ProductAttributeColor;

class ProductAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'product';

    protected $baseRoutePattern = 'product';

    protected $productTypeManager;

    protected $productTypeAdmin;

    protected $productStockManager;

    public function getProductTypeManager()
    {
        return $this->productTypeManager;
    }

    public function getProductTypeAdmin()
    {
        return $this->productTypeAdmin;
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName, $productTypeManager, $productStockManager, $productTypeAdmin)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->productTypeManager = $productTypeManager;
        $this->productStockManager = $productStockManager;
        $this->productTypeAdmin = $productTypeAdmin;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // $collection->remove('history');
        $collection->add('select_type');
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();

        if ( $type = $this->getPersistentParameter( 'type' ) ) {
            $typeObject = $this->productTypeManager->findOneBy( array( 'code' => $type ) );

            if (!$typeObject) {
                $typeObject = $this->productTypeManager->getDefault();
                if (!$typeObject) {
                    throw new \Exception('No assigned default product type.');
                }
            }

            $instance->setType( $typeObject );
        }

        if ( !$instance->getStock() ) {
            $stock = $this->productStockManager->create();
            $stock->setProduct($instance);
            $instance->setStock($stock);
            $instance->setProductStock($stock->getValue());
        } else {
            $instance->setProductStock($instance->getStock()->getValue());
        }

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getObject($id)
    {
        $object = parent::getObject($id);

        if ( !$object->getStock() ) {
            $stock = $this->productStockManager->create();
            $stock->setProduct($object);
            $object->setStock($stock);
        } else {
            $object->setProductStock($object->getStock()->getValue());
        }

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $defaultType = $this->productTypeManager->getDefault();
        if (!$defaultType) {
            throw new \Exception('No assigned default product type.');
        }

        $parameters = array_merge(
            parent::getPersistentParameters(),
            [
                //'type' => $defaultType,
            ]
        );

        if ($this->getSubject()) {
            if ( $this->isChild() ) {
                $parameters['type'] = $this->getParent()->getSubject()->getType() ? $this->getParent()->getSubject()->getType()->getCode() : null;
            } else {
                $parameters['type'] = $this->getSubject()->getType() ? $this->getSubject()->getType()->getCode() : null;
            }
            
            return $parameters;
        }

        if ($this->hasRequest()) {
            $request = $this->getRequest();
            $fitlter = $request->query->get( 'filter' );
            $type = $request->get( 'type' );

            if ( !empty( $fitlter[ 'type' ][ 'value' ] ) ) {
                // $parameters[ 'type' ] = $fitlter[ 'type' ][ 'value' ];
            } elseif ( !empty( $type ) ) {
                $typeObject = $this->productTypeManager->findOneBy( array( 'code' => $type ) );
                if ( $typeObject ) {
                    $parameters[ 'type' ] = $request->get( 'type' );
                }
            }

            return $parameters;
        }

        return $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = [];

        if (\in_array($action, ['tree', 'show', 'edit', 'delete', 'list', 'batch'], true)
            && $this->hasAccess('create')
            && $this->hasRoute('create')
        ) {
            $list['create'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_create'),
//                'template' => $this->getTemplateRegistry()->getTemplate('button_create'),
            ];
        }

        if (\in_array($action, ['show', 'delete', 'acl', 'history'], true)
            && $this->canAccessObject('edit', $object)
            && $this->hasRoute('edit')
        ) {
            $list['edit'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_edit'),
                //'template' => $this->getTemplateRegistry()->getTemplate('button_edit'),
            ];
        }

        if (\in_array($action, ['show', 'edit', 'acl'], true)
            && $this->canAccessObject('history', $object)
            && $this->hasRoute('history')
        ) {
            $list['history'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_history'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_history'),
            ];
        }

        if (\in_array($action, ['edit', 'history'], true)
            && $this->isAclEnabled()
            && $this->canAccessObject('acl', $object)
            && $this->hasRoute('acl')
        ) {
            $list['acl'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_acl'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_acl'),
            ];
        }

        if (\in_array($action, ['edit', 'history', 'acl'], true)
            && $this->canAccessObject('show', $object)
            && \count($this->getShow()) > 0
            && $this->hasRoute('show')
        ) {
            $list['show'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_show'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_show'),
            ];
        }

        if (\in_array($action, ['show', 'edit', 'delete', 'acl', 'batch', 'select_type'], true)
            && $this->hasAccess('list')
            && $this->hasRoute('list')
        ) {
            $list['list'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_list'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_list'),
            ];
        }

        return $list;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        // if ( $this->isChild() ) {
        //     // set attributes
        //     if ( !$subject->getColors()->count() ) {
        //         $subject->addColor( new ProductAttributeColor() );
        //     }
        //     // set parent product
        //     $subject->setParent( $this->getParent()->getSubject() );
        // }

        $em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $categoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'product'")
                    ->orderBy('c.position, c.name', 'ASC');
        $brandCategoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'product_brand'")
                    ->orderBy('c.position, c.name', 'ASC');
        $themeCategoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'product_theme'")
                    ->orderBy('c.position, c.name', 'ASC');


        /*if ( $this->isChild() ) {
            $formMapper
                ->with('General', ['class' => 'col-md-8'])
                    ->add('parent', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false, 'admin_code' => 'app.store.admin.product'))
                ->end()
            ;
        }*/
        
        $formMapper
            ->with('General', ['class' => 'col-md-8'])
                //->add('type', ModelHiddenType::class)
                ->add('type', ModelListType::class, array('btn_add' => false, 'btn_edit' => false))
                ->add('name', TextType::class)
                ->add('sku', null, array('label' => 'SKU'))
                ->add('slug')
                ->add('abstract', TextareaType::class, array('label' => 'Abstract', 'attr' => array('rows' => 5)))
                ->add('description', SimpleFormatterType::class, array(
                    'required' => false,
                    'label' => 'Description',
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default', // optional
                    'format_options' => array(
                        'attr' => array(
                            'class' => 'span10 col-sm-10 col-md-10',
                            'rows' => 50,
                        ),
                    ),
                ))
                ->add('price', MoneyType::class, array('scale' => 4, 'currency' => 'PHP'))
                //->add('stock', AdminType::class, array('label' => false, 'required' => true), array('admin_code' => 'app.store.admin.product_stock'))
                ->add('productStock', IntegerType::class, array('label' => 'Stock', 'required' => true))
                /*->add('colors', CollectionType::class, array(
                        'label' => false,
                        'by_reference' => false,
                        'btn_add' => false,
                        'type_options' => array(
                            // Prevents the "Delete" option from being displayed
                            'delete' => false,
                            'delete_options' => array(
                                // You may otherwise choose to put the field but hide it
                                // 'type' => HiddenType::class,
                                // In that case, you need to fill in the options as well
                                'type_options' => array(
                                    'mapped'   => false,
                                    'required' => false,
                                )
                            )
                        )
                    ),
                    array(
                        //'edit' => 'inline',
                        'inline' => 'standard',
                        // 'sortable' => 'position',
                        'limit' => 1,
                        'edit' => 'standard',
                        //'inline' => 'table',
                        // 'sortable' => 'position',
                        // 'link_parameters' => ['context' => $context],
                        'allow_delete' => false,
                        'admin_code' => 'app.store.admin.product_attribute_color',
                    )
                )*/
                ->add('position', IntegerType::class)
                ->add('sale', CheckboxType::class, array('required' => false))
                ->add('featured', CheckboxType::class, array('required' => false))
                ->add('enabled', CheckboxType::class, array('required' => false))
            ->end()
            ->with('Publication', ['class' => 'col-md-4'])
                ->add('publicationStartDate', DateTimePickerType::class, array(
                    'label' => 'Publish Start Date',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'datepicker_use_button' => false,
                    'dp_side_by_side' => true,
                    //'dp_language' => 'en',
                    'required' => true,
                ))
                ->add('publicationEndDate', DateTimePickerType::class, array(
                    'label' => 'Publish End Date',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'datepicker_use_button' => false,
                    'dp_side_by_side' => true,
                    //'dp_language' => 'en',
                    'required' => false,
                ))
            ->end()
            ->with('Media', ['class' => 'col-md-4'])
                ->add('image', ModelListType::class, array('required' => false, 'btn_edit' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image', 'context' => 'product')))
                ->add('thumbnail', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image', 'context' => 'product')))
                ->add('gallery', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'product')))
            ->end()
            ->with('Classification', ['class' => 'col-md-4'])
                // ->add('categories', ModelListType::class, array('multiple' => true, 'btn_add' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'product')))
                ->add('categories', ModelType::class, array(
                    'query' => $categoriesQuery,
                    'required' => false,
                    'property' => 'name',
                    'multiple' => true,
                    'label' => 'Category',
                    // 'attr' => ['class' => 'show'],
                    /*'callback' => static function (AdminInterface $admin, $property, $searchText): void {
                        $datagrid = $admin->getDatagrid();
                        $datagrid->setValue($property, null, $searchText);
                        $datagrid->setValue('enabled', null, true);
                    },*/
                ), array('link_parameters' => array('context' => 'product')))
                //->add('brand', ModelListType::class, array('btn_add' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'product_brand')))
                ->add('brand', ModelType::class, array(
                    'query' => $brandCategoriesQuery,
                    'required' => false,
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'Brand',
                    'placeholder' => '',
                ), array('link_parameters' => array('context' => 'product_brand')))
                //->add('theme', ModelListType::class, array('btn_add' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'product_theme')))
                ->add('theme', ModelType::class, array(
                    'query' => $themeCategoriesQuery,
                    'required' => false,
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'Theme',
                    'placeholder' => '',
                ), array('link_parameters' => array('context' => 'product_theme')))
                // ->add('brand', ModelType::class, array(
                //     'required' => false,
                //     'property' => 'name',
                //     'multiple' => false,
                // ), array('link_parameters' => array('context' => 'product_brand')))
                // ->add('theme', ModelType::class, array(
                //     'required' => false,
                //     'property' => 'name',
                //     'multiple' => false,
                // ), array('link_parameters' => array('context' => 'product_theme')))
            ->end()
            ->with('Discounts', ['class' => 'col-md-4'])
                // ->add('categories', ModelListType::class, array('multiple' => true, 'btn_add' => false, 'btn_edit' => false), array('link_parameters' => array('context' => 'product')))
                ->add('discounts', ModelType::class, array(
                    'required' => false,
                    'property' => 'name',
                    'multiple' => true,
                    'label' => false,
                    'btn_add' => false,
                    // 'attr' => ['class' => 'show'],
                    /*'callback' => static function (AdminInterface $admin, $property, $searchText): void {
                        $datagrid = $admin->getDatagrid();
                        $datagrid->setValue($property, null, $searchText);
                        $datagrid->setValue('enabled', null, true);
                    },*/
                ), array())
                // ->add('discounts', CollectionType::class, array(
                //     'label' => false,
                //     'by_reference' => false,
                //     // 'btn_add' => false,
                //     /*'type_options' => array(
                //         // Prevents the "Delete" option from being displayed
                //         'delete' => false,
                //         'delete_options' => array(
                //             // You may otherwise choose to put the field but hide it
                //             // 'type' => HiddenType::class,
                //             // In that case, you need to fill in the options as well
                //             'type_options' => array(
                //                 'mapped'   => false,
                //                 'required' => false,
                //             )
                //         )
                //     )*/
                // ))
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
        	->add('id', null, array('label' => 'ID'))
            ->add('name')
            ->add('sku', null, array('label' => 'SKU'))
            ->add('slug')
        	->add('abstract')
            ->add('description', 'html')
            ->add('price')
            ->add('stock')
            ->add('position')
            ->add('sale')
            ->add('featured')
            ->add('enabled')
            ->add('image')
        	->add('thumbnail')
            ->add('gallery')
            ->add('categories')
            ->add('brand')
            ->add('theme')
            ->add('updatedAt', null, array( 'label' => 'Last Update' ))
            ->add('createdAt', null, array( 'label' => 'Date Created' ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $typeChoices = array();
        $productTypes = $this->productTypeManager->findBy( array( 'enabled' => true ), array( 'position' => 'ASC', 'name' => 'ASC' ) );

        foreach ( $productTypes as $productType ) {
            $typeChoices[ $productType->getCode() ] = $productType->getName();
        }

        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
        ;

        if ( !$this->isChild() ) {
            $datagridMapper->add('type.code', null, array( 'label' => 'Type', 'field_type' => ChoiceType::class ), null, [ 'choices' => array_flip( $typeChoices ) ]);
        }
        
        $datagridMapper
        	->add('name')
            ->add('sku', null, array('label' => 'SKU'))
            ->add('slug')
            ->add('price')
            ->add('stock')
            ->add('position')
            ->add('sale')
            ->add('featured')
            ->add('enabled')
            ->add('categories')
            ->add('brand')
            ->add('theme')
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
	                'field_options' => array(
	                    'dp_side_by_side'    => false,
	                    //'dp_min_view_mode'   => 'days',
	                    'dp_use_current'     => false,
	                    'format'             => 'yyyy-MM-dd HH:mm'
	                )
            	)
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
        	->addIdentifier('name')
        	->addIdentifier('sku', null, array('label' => 'SKU'))
        	->add('price', null, array())
        	->add('stock.value', null, array('label' => 'Stock'))
        	->add('position', null, array('editable' => true))
        	->add('enabled', null, array('editable' => true))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !\in_array($action, ['edit'], true)) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $menu->addChild('Edit',
            $admin->generateMenuUrl('edit', ['id' => $id])
        );

        // Display variations menu link only for product types that doesn't have assigned attributes
        /*if ( $childAdmin ) {
            $productAttributes = $childAdmin->getParent()->getSubject()->getType()->getProductAttributes();
            if ( $productAttributes->count() > 0 ) {
                $menu->addChild('Variations',
                    $admin->generateMenuUrl('app.store.admin.product_variation.list', ['id' => $id])
                );
            }
        }*/
        $menu->addChild('Variations',
            $admin->generateMenuUrl('app.store.admin.product_variation.list', ['id' => $id])
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Name' => 'name',
            'Position' => 'position',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name): ?string
    {
        switch($name) {
            case 'edit':
                return 'AppBundle:CRUD:Store\Product\edit.html.twig';
            case 'show':
                return 'AppBundle:CRUD:Store\Product\show.html.twig';
            case 'short_object_description':
                return 'AppBundle:CRUD:Store\Product\short-object-description.html.twig';
            default:
                return parent::getTemplate($name);
        }

        return parent::getTemplate($name);
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $queryAll = $this->getRequest()->query->get('all');

        if ($queryAll) {
            return $query;
        }

        $query->andWhere(
            $query->expr()->isNull($query->getRootAliases()[0] . '.parent')
        );
        // $query->setParameter('my_param', 'my_value');
        return $query;
    }

    public function prePersist($product)
    {
        // Update product stock
        if ($product->getProductStock()) {
            $stockUpdate = $product->getProductStock();
            $product->getStock()->setValue(abs($stockUpdate));
        }
    }

    public function postUpdate($product)
    {
        // Update product stock
        if ($product->getProductStock()) {
            $stockUpdate = $product->getStock()->getValue() - $product->getProductStock();
            $decrement = true;
            if ($stockUpdate < 0) {
                $decrement = false;
            }
            // dump($stockUpdate);
            // dump($product->getProductStock());
            // die();
            $this->productStockManager->updateProductStock($product, abs($stockUpdate), $decrement);
        }
    }
}