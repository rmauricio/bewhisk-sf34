<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;

class ColorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $categoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'color'")
                    ->andWhere("c.parent IS NOT NULL")
                    ->orderBy('c.position, c.name', 'ASC');

        $formMapper
            ->with('General', ['class' => 'col-md-12'])
            	->add('name', TextType::class)
            	// ->add('parent', ModelListType::class, array('required' => false))
                // ->add('hexCode', ColorType::class)
                ->add('hexCode', TextType::class)
                ->add('categories', ModelType::class, array(
                    'query' => $categoriesQuery,
                    'required' => false,
                    'property' => 'name',
                    'multiple' => true,
                    'label' => 'Category',
                    'btn_add' => false,
                    // 'attr' => ['class' => 'show'],
                    /*'callback' => static function (AdminInterface $admin, $property, $searchText): void {
                        $datagrid = $admin->getDatagrid();
                        $datagrid->setValue($property, null, $searchText);
                        $datagrid->setValue('enabled', null, true);
                    },*/
                ), array('link_parameters' => array()))
            	->add('position', IntegerType::class)
                ->add('enabled', CheckboxType::class, array( 'required' => false ))
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-12'])
                ->add('id', null, array('label' => 'ID'))
                ->add('name')
                // ->add('parent', null, array('required' => false))
                ->add('hexCode')
                ->add('categories')
                ->add('position')
                ->add('enabled')
                ->add('updatedAt', null, array( 'label' => 'Last Update' ))
                ->add('createdAt', null, array( 'label' => 'Date Created' ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $categoriesQuery = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'color'")
                    ->andWhere("c.parent IS NOT NULL")
                    ->orderBy('c.position, c.name', 'ASC');

        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('name')
            // ->add('parent')
            ->add('hexCode')
            ->add('categories', null, array('label' => 'Category'), null, array(
                    'query_builder' => $categoriesQuery
                )
            )
        	->add('position')
        	->add('enabled')
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
	                'field_options' => array(
	                    'dp_side_by_side'    => false,
	                    //'dp_min_view_mode'   => 'days',
	                    'dp_use_current'     => false,
	                    'format'             => 'yyyy-MM-dd HH:mm'
	                )
            	)
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('name')
        	// ->add('parent')
        	->add('hexCode')
            ->add('categories')
        	->add('position', null, array('editable' => true))
        	->add('enabled', null, array('editable' => true))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Name' => 'name',
            // 'Parent' => 'parent.name',
            'Hex Code' => 'hexCode',
            'Position' => 'position',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}