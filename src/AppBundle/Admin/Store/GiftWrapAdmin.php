<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Sonata\AdminBundle\Form\Type\ModelListType;

class GiftWrapAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-12'])
            	->add('name', TextType::class)
                ->add('price', MoneyType::class, array('scale' => 2, 'currency' => 'PHP'))
            	->add('description', TextareaType::class, array('required' => false))
                ->add('position', IntegerType::class, array( 'required' => false ))
                ->add('enabled', CheckboxType::class, array( 'required' => false ))
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-12'])
            	->add('id', null, array('label' => 'ID'))
            	->add('name')
            	->add('price', 'number', array('scale' => 2, 'template' => 'AppBundle:CRUD:show_number_format.html.twig'))
            	->add('description')
            	->add('position')
            	->add('enabled')
                ->add('updatedAt', null, array( 'label' => 'Last Update' ))
                ->add('createdAt', null, array( 'label' => 'Date Created' ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('name')
        	->add('price')
        	->add('position')
        	->add('enabled')
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
	                'field_options' => array(
	                    'dp_side_by_side'    => false,
	                    //'dp_min_view_mode'   => 'days',
	                    'dp_use_current'     => false,
	                    'format'             => 'yyyy-MM-dd HH:mm'
	                )
            	)
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('name')
        	->add('price', 'number', array('scale' => 2, 'row_align' => 'right', 'template' => 'AppBundle:CRUD:list_number_format.html.twig'))
        	->add('position', null, array('editable' => true))
        	->add('enabled', null, array('editable' => true))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Name' => 'name',
            'Price' => 'price',
            'Description' => 'description',
            'Position' => 'position',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}