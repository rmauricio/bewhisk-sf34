<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Store\Order;
use Hoa\Compiler\Llk\Rule\Choice;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class OrderAdmin extends AbstractAdmin
{
    // protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    // {
    //     if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
    //         return;
    //     }

    //     $admin = $this->isChild() ? $this->getParent() : $this;
    //     $id = $admin->getRequest()->get('id');

    //     $menu->addChild('View Order', [
    //         'uri' => $admin->generateUrl('show', ['id' => $id])
    //     ]);

    //     dump($this->isChild(), $this->getParent());

    //     if ($this->isGranted('EDIT')) {
    //         $menu->addChild('Edit Order', [
    //             'uri' => $admin->generateUrl('edit', ['id' => $id])
    //         ]);
    //     }

    //     if ($this->isGranted('LIST')) {
    //         $menu->addChild('Invoices', [
    //             'uri' => $admin->generateUrl('app.store.admin.order_invoice.list', ['id' => $id])
    //         ]);
    //         $menu->addChild('Payments', [
    //             'uri' => $admin->generateUrl('app.store.admin.order_payment.list', ['id' => $id])
    //         ]);
    //     }

    //     // if ($this->isGranted('LIST')) {
    //     //     $menu->addChild('Manage Videos', [
    //     //         'uri' => $admin->generateUrl('App\Admin\VideoAdmin.list', ['id' => $id])
    //     //     ]);
    //     // }
    // }

    protected $smFactory;

    protected $orderManager;

    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'createdAt'
    );

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName, $orderManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->orderManager = $orderManager;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !\in_array($action, ['edit'], true)) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $menu->addChild('Edit Order',
            $admin->generateMenuUrl('edit', ['id' => $id])
        );

        $order = $this->getSubject();
        $allowedStatusesForPayment = Order::getAllowedStatusesForPayment();
    
        if ($order->getId() && array_key_exists($order->getStatus(), $allowedStatusesForPayment)) {
            $menu->addChild('Invoices',
                $admin->generateMenuUrl('app.store.admin.order_invoice.list', ['id' => $id])
            );

            $menu->addChild('Payments',
                $admin->generateMenuUrl('app.store.admin.order_payment.list', ['id' => $id])
            );
        }

        // $page = $this->getSubject();
        // if (!$page->isHybrid() && !$page->isInternal()) {
        //     try {
        //         $path = $page->getUrl();
        //         $siteRelativePath = $page->getSite()->getRelativePath();
        //         if (!empty($siteRelativePath)) {
        //             $path = $siteRelativePath.$path;
        //         }
        //         $menu->addChild('view_page', [
        //             'uri' => $this->getRouteGenerator()->generate('page_slug', [
        //                 'path' => $path,
        //             ]),
        //         ]);
        //     } catch (\Exception $e) {
        //         // avoid crashing the admin if the route is not setup correctly
        //         // throw $e;
        //     }
        // }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId() && $subject->getOrderNumber()) {
            $formMapper
                ->tab('General')
                    ->with('General', ['class' => 'col-md-4'])
                        ->add('orderNumberData', TextType::class, array('mapped' => false, 'required' => false, 'attr' => array('label' => 'Order Number', 'readonly' => true), 'data' => $subject->getOrderNumber()))
                    ->end()
                ->end()
            ;
        }

        $statusOptions = Order::getStatusOptions();

        $formMapper
            ->tab('General')
                ->with('General', ['class' => 'col-md-4'])
                    ->add('user', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false))
                    ->add('paymentMethod', ChoiceType::class, [
                        // 'data' => Order::PAYMENT_METHOD_PAYNAMICS, 
                        'expanded' => true, 
                        'required' => true,
                        'choices' => array_flip(Order::getPaymentMethodOptions()),
                        // 'choice_label' => function ($choice, $key, $value) {
                        //     return $value;
                        // }
                        // 'choice_attr' => function($choice, $key, $value) {
                        //     // if ($value != Order::PAYMENT_METHOD_PAYNAMICS) {
                        //     //     return ['disabled' => true];
                        //     // }
                        //     return [];
                        // },
                    ]);
        
        if ($subject->getId()) {
            $orderSmFactory = $this->smFactory->get($subject, 'default');

            $transitionLabels = Order::getStatusTransitionLabels();
            $possibleTransitions = $orderSmFactory->getPossibleTransitions();
            $trasitionOptions = array();
            
            foreach ($possibleTransitions as $transition) {
                if (!isset($transitionLabels[$transition])) {
                    continue;
                }
                $trasitionOptions[$transition] = $transitionLabels[$transition];
            }
            
            $formMapper->add('statusData', TextType::class, array( 
                'mapped' => false, 
                'required' => false, 
                'label' => 'Status', 
                'data' => $statusOptions[$subject->getStatus()],
                'attr' => array('label' => 'Order Number', 'readonly' => true)
            ) );
            if ($possibleTransitions) {
                $formMapper->add('statusTransition', ChoiceType::class, array(
                    'label' => 'Status Transition', 
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => '',
                    'choices' => array_flip($trasitionOptions))
                );
            }
        }
        
        $formMapper
                ->end()
                ->with('Line Items', ['class' => 'col-md-8'])
                    ->add('lineItems', CollectionType::class, array(
                            'label' => false,
                            'by_reference' => false,
                            'constraints' => new \Symfony\Component\Validator\Constraints\Valid(),
                            // 'btn_add' => false,
                            'type_options' => array(
                                // Prevents the "Delete" option from being displayed
                                //'delete' => false,
                                /*'delete_options' => array(
                                    // You may otherwise choose to put the field but hide it
                                    // 'type' => HiddenType::class,
                                    // In that case, you need to fill in the options as well
                                    'type_options' => array(
                                        'mapped'   => false,
                                        'required' => false,
                                    )
                                )*/
                            ),
                        ),
                        array(
                            //'edit' => 'standard',
                            //'inline' => 'standard',
                            // 'sortable' => 'position',
                            // 'limit' => 1,
                            'edit' => 'inline',
                            'inline' => 'table',
                            // 'sortable' => 'position',
                            // 'link_parameters' => ['context' => $context],
                            'allow_delete' => true,
                            'admin_code' => 'app.store.admin.order_line_item',
                        )
                    )
                ->end()
            ->end()
            ->tab('Billing')
                ->with('Billing', ['class' => 'col-md-6'])
                    ->add('billingFirstName', TextType::class, array('label' => 'First Name'))
                    ->add('billingLastName', TextType::class, array('label' => 'Last Name'))
                    ->add('billingEmail', TextType::class, array('label' => 'E-mail address'))
                    ->add('billingAddress1', TextareaType::class, array('label' => 'Address', 'attr' => array('rows' => 5)))
                    ->add('billingAddress2', TextareaType::class, array('required' => false, 'attr' => array('rows' => 5), 'label' => 'Landmark'))
                    ->add('billingPostalCode', TextType::class, array('label' => 'Postal Code'))
                    ->add('billingCity', EntityType::class, array('label' => 'City', 'class' => 'AppBundle:Address\City', 'empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('r')
                            ->where('r.enabled = true')
                            ->orderBy('r.name', 'ASC');
                    }, 'choice_label' => function ($region) {
                        return $region->getName();
                    }))
                    ->add('billingContactNumber', TextType::class, ['label' => 'Contact Number'])
                ->end()
            ->end()
            ->tab('Shipping')
                ->with('Shipping', ['class' => 'col-md-6'])
                    ->add('shippingFirstName', TextType::class, array('label' => 'First Name'))
                    ->add('shippingLastName', TextType::class, array('label' => 'Last Name'))
                    ->add('shippingAddress1', TextareaType::class, array('label' => 'Address', 'attr' => array('rows' => 5)))
                    ->add('shippingAddress2', TextareaType::class, array('required' => false, 'attr' => array('rows' => 5), 'label' => 'Landmark'))
                    ->add('shippingPostalCode', TextType::class, array('label' => 'Postal Code'))
                    ->add('shippingCity', EntityType::class, array('label' => 'City', 'class' => 'AppBundle:Address\City', 'empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('r')
                            ->where('r.enabled = true')
                            ->orderBy('r.name', 'ASC');
                    }, 'choice_label' => function ($region) {
                        return $region->getName();
                    }))
                    ->add('shippingContactNumber', TextType::class, ['label' => 'Contact Number'])
                    ->add('shippingInstructions', TextareaType::class, ['label' => 'Shipping Instructions', 'required' => false, 'attr' => array('rows' => 5)])         
                ->end()
            ->end()
            // ->tab('Gift Wrap')
            //     ->with('Gift Wrap', ['class' => 'col-md-6'])
            //         ->add('giftWrap', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false))
            //         ->add('giftWrapDedication', TextareaType::class, array('required' => false, 'attr' => array('rows' => 5)))
            //     ->end()
            // ->end()
            ->tab('Promo Code')
                ->with('Promo Code', ['class' => 'col-md-6'])
                    ->add('promoCode', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false))
                ->end()
            ->end()
        ;

        // if ($subject->getId()) {
        //     $formMapper
        //         ->tab('General')
        //             ->with('General', ['class' => 'col-md-4'])
        //                 ->add('totalPriceData', TextType::class, array('mapped' => false, 'required' => false, 'label' => 'Total Price', 'attr' => array('readonly' => true), 'data' => $subject->getTotalPrice()))
        //             ->end()
        //         ->end()
        //     ;
        // }

        // if ($this->isSuperAdmin()) {
        //     $formMapper
        //         ->tab('General')
        //             ->with('General', ['class' => 'col-md-4'])
        //                 //->add('status', ChoiceType::class, array( 'label' => 'Status', 'choices' => array_flip( Order::getStatusOptions() ) ) )
        //                 ->add('sessionId', TextType::class, array('label' => 'Session ID', 'required' => false))
        //                 ->add('ipAddress')
        //                 // ->add('data', TextareaType::class, array())
        //                 // ->add('lineItems')
        //             ->end()
        //         ->end()
        //     ;
        // }
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('orderNumber')
            ->add('user', null, array('label' => 'Customer'))
            ->add('user.id', null, array('label' => 'Customer ID'))
            ->add('user.username', null, array('label' => 'Customer Username'))
            ->add('user.email', null, array('label' => 'Customer Email'))
            ->add('user.firstname', null, array('label' => 'Customer First Name'))
            ->add('user.middlename', null, array('label' => 'Customer Middle Name'))
        	->add('user.lastname', null, array('label' => 'Customer Last Name'))
        	->add('promoCodeValue', null, array('label' => 'Promo Code Discount'))
        	->add('finalTotalPrice', null, array('label' => 'Total'))
            // ->add('giftWrap')
            // ->add('giftWrap.id', null, array('label' => 'Gift Wrap ID'))
            // ->add('giftWrapName')
            // ->add('giftWrapPrice')
            // ->add('giftWrapDescription')
            ->add('status', 'choice', array( 'label' => 'Status', 'choices' => Order::getStatusOptions() ))
            ->add('lineItems')
            ->add('ipAddress')
        ;

        if ($this->isSuperAdmin()) {
            $formMapper
                ->add('sessionId', null, array('label' => 'Session ID'))
                ->add('data')
            ;
        }

        $formMapper
            ->add('updatedAt', null, array( 'label' => 'Last Update' ))
            ->add('createdAt', null, array( 'label' => 'Date Created' ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
            ->add('orderNumber')
            ->add('shippingFee', null, array('label' => "Shipping Fee"))
            ->add('finalTotalPrice', null, array('label' => "Total"))
            ->add('user.id', null, array('label' => 'Customer ID'))
            ->add('username', null, array('label' => 'Customer Username'))
            ->add('userEmail', null, array('label' => 'Customer Email'))
            ->add('userFirstName', null, array('label' => 'Customer First Name'))
        	->add('userLastName', null, array('label' => 'Customer Last Name'))
        	->add('promoCodeCode', null, array('label' => 'Promo Code'))
        	->add('promoCodeValue', null, array('label' => 'Promo Code Discount'))
            ->add('paymentMethod', null, array('label' => 'Payment Method', 'field_type' => ChoiceType::class), null, array('choices' => array_flip( Order::getPaymentMethodOptions() )))
            ->add('shippingLastName', null, array('label' => 'Shipping Last Name'))
            ->add('shippingFirstName', null, array('label' => 'Shipping First Name'))
            ->add('shippingEmail', null, array('label' => 'Shipping Email'))
            ->add('shippingAddress1', null, array('label' => 'Shipping Address'))
            ->add('shippingAddress2', null, array('label' => 'Shipping Landmark'))
            ->add('shippingCity.region', null, array('label' => 'Shipping Region'))
            ->add('shippingCity', null, array('label' => 'Shipping City'))
            ->add('shippingPostalCode', null, array('label' => 'Shipping Postal Code'))
            ->add('shippingContactNumber', null, array('label' => 'Shipping Contact Number'))
            ->add('billingLastName', null, array('label' => 'Billing Last Name'))
            ->add('billingFirstName', null, array('label' => 'Billing First Name'))
            ->add('billingEmail', null, array('label' => 'Billing Email'))
            ->add('billingAddress1', null, array('label' => 'Billing Address'))
            ->add('billingAddress2', null, array('label' => 'Billing Landmark'))
            ->add('billingCity.region', null, array('label' => 'Billing Region'))
            ->add('billingCity', null, array('label' => 'Billing City'))
            ->add('billingPostalCode', null, array('label' => 'Billing Postal Code'))
            ->add('billingContactNumber', null, array('label' => 'Billing Contact Number'))
            // ->add('giftWrap.id', null, array('label' => 'Gift Wrap ID'))
            // ->add('giftWrapName', null, array('label' => 'Gift Wrap Name'))
            // ->add('giftWrapPrice', null, array('label' => 'Gift Wrap Price'))
            ->add('status', null, array('label' => 'Status', 'field_type' => ChoiceType::class), null, array('choices' => array_flip( Order::getStatusOptions() )))
            ->add('ipAddress')
        ;

        if ($this->isSuperAdmin()) {
            $datagridMapper
                ->add('sessionId')
                ->add('uniqid')
                ->add('data')
            ;
        }

        $datagridMapper
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
                    'field_options' => array(
                        'dp_side_by_side'    => false,
                        //'dp_min_view_mode'   => 'days',
                        'dp_use_current'     => false,
                        'format'             => 'yyyy-MM-dd HH:mm'
                    )
                )
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('orderNumber')
            ->add('user.id', null, array('label' => 'Customer ID'))
            ->add('billingLastName', null, array('label' => 'Billing Last Name'))
            ->add('billingFirstName', null, array('label' => 'Billing First Name'))
            ->add('finalTotalPrice', null, array('label' => 'Total'))
            ->add('status', 'choice', array(
                'label' => 'Status',
                'choices' => Order::getStatusOptions(),
            ))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        if ($this->isSuperAdmin()) {
            return array(
                'ID' => 'id',
                'Order Number' => 'orderNumber',
                'Total Price' => 'totalPrice',
                'Customer ID' => 'user.id',
                'Customer Username' => 'user.username',
                'Customer Email' => 'user.email',
                'Customer First Name' => 'userFirstName',
                'Customer Midddle Name' => 'userMiddleName',
                'Customer Last Name' => 'userLastName',
                'Gift Wrap ID' => 'giftWrap.id',
                'Gift Wrap Name' => 'giftWrapName',
                'Gift Wrap Price' => 'giftWrapPrice',
                'Gift Wrap Description' => 'giftWrapDescription',
                'Status' => 'getStatusLabel',
                'Session ID' => 'sessionId',
                'IP Address' => 'ipAddress',
                'Data' => 'data',
                'Last Update' => 'updatedAt',
                'Date Created' => 'createdAt',
            );
        }

        return array(
            'ID' => 'id',
            'Order Number' => 'orderNumber',
            'Total Price' => 'totalPrice',
            'Customer ID' => 'user.id',
            'Customer Username' => 'user.username',
            'Customer Email' => 'user.email',
            'Customer First Name' => 'userFirstName',
            'Customer Midddle Name' => 'userMiddleName',
            'Customer Last Name' => 'userLastName',
            'Gift Wrap ID' => 'giftWrap.id',
            'Gift Wrap Name' => 'giftWrapName',
            'Gift Wrap Price' => 'giftWrapPrice',
            'Gift Wrap Description' => 'giftWrapDescription',
            'Status' => 'getStatusLabel',
            'IP Address' => 'ipAddress',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }

    // public function preUpdate($object)
    // {
    //     $orderSmFactory = $this->smFactory->get($object, 'default');

    //     $statusTransition = $object->getStatusTransition();
    //     if ($orderSmFactory->can($statusTransition)) {
    //         $orderSmFactory->apply($statusTransition);
    //     }
    // }

    // public function postUpdate($object)
    // {
    //     $this->orderManager->updateStocksWithOrder($object);
    // }

    public function isSuperAdmin()
    {
        $userAdmin = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $userRoles = $userAdmin->getRoles();

        if ($userRoles && in_array('ROLE_SUPER_ADMIN', $userRoles)) {
            return true;
        }

        return false;
    }

    public function setSmFactory($smFactory)
    {
        $this->smFactory = $smFactory;
    }
}