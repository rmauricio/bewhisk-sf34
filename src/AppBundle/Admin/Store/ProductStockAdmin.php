<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use AppBundle\Entity\Store\ProductStock;

class ProductStockAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-12'])
            	//->add('product', null, array(), array('admin_code' => 'app.store.admin.product'))
            	->add('value', IntegerType::class, array('required' => true, 'label' => 'Stock'))
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-12'])
                ->add('id', null, array('label' => 'ID'))
            	->add('value', 'number', array())
                ->add('updatedAt', null, array( 'label' => 'Last Update' ))
                ->add('createdAt', null, array( 'label' => 'Date Created' ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('value')
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
	                'field_options' => array(
	                    'dp_side_by_side'    => false,
	                    //'dp_min_view_mode'   => 'days',
	                    'dp_use_current'     => false,
	                    'format'             => 'yyyy-MM-dd HH:mm'
	                )
            	)
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->add('value', 'number', array('row_align' => 'right'))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Value' => 'value',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}