<?php

namespace AppBundle\Admin\Store;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use AppBundle\Entity\Store\Order;

class OrderLineItemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*if (!$this->isChild()) {
            $formMapper
                ->with('General', ['class' => 'col-md-8'])
                    ->add('order', ModelListType::class, array('required' => true, 'btn_add' => false, 'btn_edit' => false))
                ->end()
            ;
        }*/

        $formMapper
            ->with('General', ['class' => 'col-md-8'])
            	->add('product', ModelListType::class, array('required' => false, 'btn_add' => false, 'btn_edit' => false), array('admin_code' => 'app.store.admin.product', 'link_parameters' => array('all' => 1)))
                ->add('quantity', IntegerType::class, array())
            ->end()
        ;

        /*if ($this->isSuperAdmin()) {
            $formMapper
                ->with('General', ['class' => 'col-md-8'])
                    ->add('data', TextareaType::class, array())
                ->end()
            ;
        }*/
    }

    protected function configureShowFields(ShowMapper $formMapper)
    {
        $formMapper
        	->add('id', null, array('label' => 'ID'))
        	->add('order')
            ->add('product', null, array('admin_code' => 'app.store.admin.product'))
            ->add('productName', null, array('label' => 'Product Name'))
            ->add('productTypeId', null, array('label' => 'Product Type ID'))
            ->add('productTypeCode', null, array('label' => 'Product Type Code'))
            ->add('productTypeName', null, array('label' => 'Product Type Name'))
            ->add('price')
            ->add('quantity')
        ;

        if ($this->isSuperAdmin()) {
            $formMapper
                ->add('data')
            ;
        }

        $formMapper
            ->add('updatedAt', null, array( 'label' => 'Last Update' ))
            ->add('createdAt', null, array( 'label' => 'Date Created' ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
        ;

        if (!$this->isChild()) {
            $datagridMapper
                ->add('order.id', null, array('label' => 'Order ID'))
                ->add('order.orderNumber', null, array('label' => 'Order Number'))
            ;
        }

        $datagridMapper
            ->add('productName')
            ->add('productTypeId')
            ->add('productTypeCode')
            ->add('productTypeName')
            ->add('price')
            ->add('quantity')
        ;

        if ($this->isSuperAdmin()) {
            $datagridMapper
                ->add('data')
            ;
        }

        $datagridMapper
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
                    'field_options' => array(
                        'dp_side_by_side'    => false,
                        //'dp_min_view_mode'   => 'days',
                        'dp_use_current'     => false,
                        'format'             => 'yyyy-MM-dd HH:mm'
                    )
                )
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
        ;

        if (!$this->isChild()) {
            $listMapper
                ->addIdentifier('ordder.orderNumber', null, array('label' => 'Order'))
            ;
        }

        $listMapper
            ->add('productName')
            ->add('productTypeName', null, array('label' => 'Product Type'))
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        if ($this->isSuperAdmin()) {
            return array(
                'ID' => 'id',
                'Order ID' => 'order.id',
                'Order Number' => 'order.orderNumber',
                'Product ID' => 'product.id',
                'Product Name' => 'productName',
                'Product Type ID' => 'productTypeId',
                'Product Type Code' => 'productTypeCode',
                'Product Type Name' => 'productTypeName',
                'Price' => 'price',
                'Quantity' => 'quantity',
                'Data' => 'data',
                'Last Update' => 'updatedAt',
                'Date Created' => 'createdAt',
            );
        }

        return array(
            'ID' => 'id',
            'Order ID' => 'order.id',
            'Order Number' => 'order.orderNumber',
            'Product ID' => 'product.id',
            'Product Name' => 'productName',
            'Product Type ID' => 'productTypeId',
            'Product Type Code' => 'productTypeCode',
            'Product Type Name' => 'productTypeName',
            'Price' => 'price',
            'Quantity' => 'quantity',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }

    public function isSuperAdmin()
    {
        $userAdmin = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $userRoles = $userAdmin->getRoles();

        if ($userRoles && in_array('ROLE_SUPER_ADMIN', $userRoles)) {
            return true;
        }

        return false;
    }
}