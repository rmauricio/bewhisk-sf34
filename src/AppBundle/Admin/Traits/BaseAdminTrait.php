<?php

namespace AppBundle\Admin\Traits;

/**
 * BaseAdminnTrait
 */
trait BaseAdminTrait
{
    public function isSuperAdmin()
    {
        $userAdmin = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $userRoles = $userAdmin->getRoles();

        if ($userRoles && in_array('ROLE_SUPER_ADMIN', $userRoles)) {
            return true;
        }

        return false;
    }

    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }
}