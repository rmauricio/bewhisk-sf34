<?php

namespace AppBundle\Admin\User;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use AppBundle\Admin\User\BillingInformationAdmin as BaseBillingInformationAdmin;

class UserBillingInformationAdmin extends BaseBillingInformationAdmin
{
    protected $baseRouteName = 'user_billing_information';

    protected $baseRoutePattern = 'user-billing-information';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $userFieldType = ModelListType::class;
        $userFieldOptions = array('btn_edit' => false, 'btn_add' => false);

        $formMapper
            //->with('General', ['class' => 'col-md-12'])
                ->add('user', ModelHiddenType::class)
                ->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('address1', TextareaType::class, array('label' => 'Address 1', 'attr' => array('rows' => 5)))
                ->add('address2', TextareaType::class, array('required' => false, 'label' => 'Address 2', 'attr' => array('rows' => 5)))
                ->add('postalCode', TextType::class)
                ->add('city', ModelListType::class, array('required' => false, 'btn_edit' => false))
                ->add('contactNumber', TextType::class)
            //->end()
        ;
    }
}