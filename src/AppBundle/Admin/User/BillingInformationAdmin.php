<?php

namespace AppBundle\Admin\User;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelHiddenType;

class BillingInformationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $userFieldType = ModelListType::class;
        $userFieldOptions = array('btn_edit' => false, 'btn_add' => false);

        $formMapper
            ->with('General', ['class' => 'col-md-12'])
                ->add('user', $userFieldType, $userFieldOptions)
                ->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('email', TextType::class, array('label' => 'E-mail Address'))
                ->add('address1', TextareaType::class, array('label' => 'Address 1', 'attr' => array('rows' => 5)))
                ->add('address2', TextareaType::class, array('required' => false, 'label' => 'Address 2', 'attr' => array('rows' => 5)))
                ->add('postalCode', TextType::class)
                ->add('city', ModelListType::class, array('btn_edit' => false))
                ->add('contactNumber', TextType::class)
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General', ['class' => 'col-md-12'])
                ->add('id', null, array('label' => 'ID'))
                ->add('user')
                ->add('user.firstname', null, array('label' => 'User First Name'))
                ->add('user.lastname', null, array('label' => 'User Last Name'))
                ->add('user.email', null, array('label' => 'User E-mail Address'))
                ->add('user.phone', null, array('label' => 'User Phone'))
                ->add('firstName')
                ->add('lastName')
                ->add('email', null, array('label' => 'E-mail Address'))
                ->add('address1', null, array('label' => 'Address 1'))
                ->add('address2', null, array('label' => 'Address 2'))
                ->add('postalCode')
                ->add('city', null, array('label' => 'City'))
                ->add('city.region', null, array('label' => 'Region'))
                ->add('contactNumber')
                ->add('updatedAt', null, array( 'label' => 'Last Update' ))
                ->add('createdAt', null, array( 'label' => 'Date Created' ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('id', null, array('label' => 'ID'))
            ->add('user.username', null, array('label' => 'User Username'))
            ->add('user.firstname', null, array('label' => 'User First Name'))
            ->add('user.lastname', null, array('label' => 'User Last Name'))
            ->add('user.email', null, array('label' => 'User E-mail Address'))
            ->add('user.phone', null, array('label' => 'User Phone'))
            ->add('firstName')
            ->add('lastName')
            ->add('email', null, array('label' => 'E-mail Address'))
        	->add('address1', null, array('label' => 'Address 1'))
        	->add('address2', null, array('label' => 'Address 2'))
        	->add('postalCode')
        	->add('city')
        	->add('city.region', null, array('label' => 'Region'))
        	->add('contactNumber')
            ->add('createdAt', 'doctrine_orm_datetime_range', array('label' => 'Date Created'), 'sonata_type_datetime_range_picker', array(
	                'field_options' => array(
	                    'dp_side_by_side'    => false,
	                    //'dp_min_view_mode'   => 'days',
	                    'dp_use_current'     => false,
	                    'format'             => 'yyyy-MM-dd HH:mm'
	                )
            	)
            )
            ->add('updatedAt', 'doctrine_orm_datetime_range', array('label' => 'Last Update'), 'sonata_type_datetime_range_picker', array(
                'field_options' => array(
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                )
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->addIdentifier('id', null, array('label' => 'ID'))
            ->add('user.username', null, array('label' => 'Username'))
            ->add('firstName')
        	->add('lastName')
        	->add('contactNumber')
            ->add('createdAt', 'datetime', array('label' => 'Date Created'))
            ->add('_action', null, array(
				'header_style' => 'width: 200px',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'User Username' => 'user.username',
            'User First Name' => 'user.firstname',
            'User Last Name' => 'user.lastname',
            'User E-mail Address' => 'user.email',
            'First Name' => 'firstName',
            'Last Name' => 'lastName',
            'E-mail Address' => 'email',
            'Address 1' => 'address1',
            'Address 2' => 'address2',
            'Postal Code' => 'postalCode',
            'City' => 'city',
            'Region' => 'city.region',
            'Contact Number' => 'contactNumber',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}