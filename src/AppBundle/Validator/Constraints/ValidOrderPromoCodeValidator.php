<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidOrderPromoCodeValidator extends ConstraintValidator
{
    protected $orderService;

    protected $storeSettings;
    
    public function validate($protocol, Constraint $constraint)
    {
        if (!$protocol->getPromoCode()) {
            return;
        }

        $promoCode = $protocol->getPromoCode();
        $currentDateTime = new \DateTime();

        if (!$protocol->isOnInitialState()) {
            return;
        }

        if (!$promoCode->getEnabled()) {
            $this->context->buildViolation($constraint->messagePromoCodeInvalid)
                ->atPath('promoCode')
                ->addViolation();
        } elseif ($promoCode->getStartDate()->format('U') > $currentDateTime->format('U')) {
            $this->context->buildViolation($constraint->messagePromoCodeNotAvailable)
                ->atPath('promoCode')
                ->addViolation();
        } elseif ($promoCode->getEndDate() && $promoCode->getEndDate()->format('U') < $currentDateTime->format('U')) {
            $this->context->buildViolation($constraint->messagePromoCodeExpired)
                ->atPath('promoCode')
                ->addViolation();
        } else {
            $totals = $this->orderService->getTotals($protocol);
            if ($totals['totalPrice'] < $promoCode->getMinimumOrderTotal()) {
                $this->context->buildViolation($constraint->messagePromoCodeLTTotalOrder)
                ->setParameter('{{ amount }}', $this->storeSettings['default_currency'] . ' ' . number_format($promoCode->getMinimumOrderTotal(), 2))
                ->atPath('promoCode')
                ->addViolation();
            }
        }
    }

    public function setOrderService($orderService)
    {
        $this->orderService = $orderService;
    }

    public function setStoreSettings($storeSettings)
    {
        $this->storeSettings = $storeSettings;
    }
}