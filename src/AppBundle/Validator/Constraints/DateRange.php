<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateRange extends Constraint
{
    public $messageStartDateLessThan = 'This value should be less than end date.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}