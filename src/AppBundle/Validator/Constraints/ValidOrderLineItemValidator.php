<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidOrderLineItemValidator extends ConstraintValidator
{
    private $session;
    
    private $authorizationChecker;

    private $tokenStorage;
    
    private $orderManager;

    private $orderLineItemManager;

    private $productManager;

    public function validate($protocol, Constraint $constraint)
    {
        // if ($protocol->getProduct() && !$protocol->getProduct()->getEnabled()) {
        //     // $this->context->buildViolation($constraint->messageProductNotAvailable)
        //     //     ->setParameter('{{ productName }}', $protocol->getProduct()->getName())
        //     //     ->atPath('product')
        //     //     ->addViolation();
        // }
        
        // $activeOrder = $this->orderManager->getUserActiveOrder();
        $order = $protocol->getOrder();
        $remainingStocks = $this->productManager->countRemainingStocks($protocol->getProduct(), $order);

        $stock = $remainingStocks[$protocol->getProduct()->getId()];
        $hasReservedStock = false;
        if (is_array($stock)) {
            if ($stock['reserved_stocks'] > 0) {
                $hasReservedStock = true;
            }
            $stock = $stock['less_reserved'];
        }
        if (!$hasReservedStock && $stock <= 0) {
            $this->context->buildViolation($constraint->messageOutOfStock)
                ->setParameter('{{ productName }}', $protocol->getProduct()->getName())
                ->atPath('quantity')
                ->addViolation();
        } elseif ($stock < $protocol->getQuantity()) {
            $this->context->buildViolation($constraint->messageInsufficientStocks)
                ->setParameter('{{ productName }}', $protocol->getProduct()->getName())
                ->atPath('quantity')
                ->addViolation();
        }
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function setTokenStorage($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function setOrderManager($orderManager)
    {
        $this->orderManager = $orderManager;
    }

    public function setOrderLineItemManager($orderLineItemManager)
    {
        $this->orderLineItemManager = $orderLineItemManager;
    }

    public function setProductManager($productManager)
    {
        $this->productManager = $productManager;
    }
}