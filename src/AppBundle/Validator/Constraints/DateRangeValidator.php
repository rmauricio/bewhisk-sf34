<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateRangeValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
    	if ($protocol->getStartDate() instanceOf \DateTime && $protocol->getEndDate() instanceOf \DateTime) {
        	if ($protocol->getStartDate()->format('U') > $protocol->getEndDate()->format('U')) {
    			$this->context->buildViolation($constraint->messageStartDateLessThan)
                    ->atPath('startDate')
                    ->addViolation();
    		}
        }
    }
}