<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidOrderPromoCode extends Constraint
{
	public $messagePromoCodeInvalid = 'Invalid promo code.';
	public $messagePromoCodeNotAvailable = 'Promo code currently not available.';
	public $messagePromoCodeExpired = 'Promo code no longer available.';
	public $messagePromoCodeLTTotalOrder = 'Minimum purchase for this promo code should be {{ amount }}.';
	public $messagePromoCodeUsed = 'Promo code no longer available.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}