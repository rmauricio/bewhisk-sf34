<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidOrderLineItem extends Constraint
{
    // public $messageInsufficientStocks = 'Product "{{ productName }}" current stock not sufficient.';
	// public $messageOutOfStock = 'Product "{{ productName }}" is out of stock.';
	public $messageInsufficientStocks = 'Current stock is not sufficient for product "{{ productName }}".';
	public $messageOutOfStock = 'Product "{{ productName }}" is out of stock.';
	public $messageProductNotAvailable = 'Product "{{ productName }}" is not available.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}