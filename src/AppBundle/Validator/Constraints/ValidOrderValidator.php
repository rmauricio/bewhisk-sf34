<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidOrderValidator extends ConstraintValidator
{
    protected $orderService;
    
    public function validate($protocol, Constraint $constraint)
    {
    }

    public function setOrderService($orderService)
    {
        $this->orderService = $orderService;
    }
}