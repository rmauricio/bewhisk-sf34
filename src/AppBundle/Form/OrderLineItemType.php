<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Store\OrderLineItem;
use AppBundle\DataTransformer\ProductToNumberTransformer;

class OrderLineItemType extends AbstractType
{
    private $productToNumberTransformer;

    public function __construct(ProductToNumberTransformer $productToNumberTransformer)
    {
        $this->productToNumberTransformer = $productToNumberTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->add('productVariations', CollectionType::class, [
        //     'entry_type' => LineItemType::class,
        //     'entry_options' => ['label' => false],
        // ]);

        $builder->add('product', HiddenType::class, array(
            'invalid_message' => 'You have selected an invalid product.',
        ));

        $builder->get('product')->addModelTransformer($this->productToNumberTransformer);

        $builder
            //->add('color', ChoiceType::class, array(), array('placeholder' => ''))
            //->add('size', ChoiceType::class, array(), array('placeholder' => ''))
            ->add('quantity', IntegerType::class, array())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderLineItem::class,
        ]);
    }
}