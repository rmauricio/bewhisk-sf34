<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User\ShippingInformation;

class ShippingInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('email', TextType::class)
            ->add('address1', TextareaType::class, array('label' => 'Address'))
            ->add('address2', TextareaType::class, array('required' => false, 'label' => 'Landmark'))
            ->add('postalCode', TextType::class)
            ->add('region', EntityType::class, array('class' => 'AppBundle:Address\Region', 'empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('r')
                    ->where('r.enabled = true')
                    ->orderBy('r.name', 'ASC');
            }, 'choice_label' => function ($region) {
                return $region->getName();
            }))
            ->add('contactNumber', TextType::class)
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->addElements($form, $data);
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $this->addElements($form, $data);
    }

    protected function addElements(FormInterface $form, $data) 
    {
        // Build city
        $region = null;
        if (is_array($data) && isset($data['region'])) {
            $region = $data['region'];
        } elseif ($data instanceof ShippingInformation) {
            $region = $data->getRegion();
        }

        if (null === $region) {
            $form->add('city', null, array('empty_data' => null, 'required' => true, 'placeholder' => '', 'choices' => array()));
        } else {
            $form->add('city', null, array('empty_data' => null, 'required' => true, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) use ($region) {
                return $er->createQueryBuilder('c')
                    ->where('c.enabled = true')
                    ->andWhere('c.region = :region')
                    ->orderBy('c.name', 'ASC')
                    ->setParameter('region', $region);
            }));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ShippingInformation::class,
        ]);
    }
}