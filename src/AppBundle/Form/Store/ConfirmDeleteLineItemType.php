<?php

namespace AppBundle\Form\Store;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Store\OrderLineItem;
use AppBundle\DataTransformer\OrderLineItemToNumberTransformer;

class ConfirmDeleteLineItemType extends AbstractType
{
    private $orderLineItemToNumberTransformer;

    public function __construct(OrderLineItemToNumberTransformer $orderLineItemToNumberTransformer)
    {
        $this->orderLineItemToNumberTransformer = $orderLineItemToNumberTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('item', HiddenType::class);
        
        $builder->get('item')
            ->addModelTransformer($this->orderLineItemToNumberTransformer);
    }
}