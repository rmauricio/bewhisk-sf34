<?php

namespace AppBundle\Form\Store;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Entity\Store\Order;
use AppBundle\Entity\Store\GiftWrap;
use AppBundle\Form\User\BillingInfoType;
use AppBundle\Form\User\ShippingInfoType;
use AppBundle\DataTransformer\PromoCodeToNumberTransformer;
use AppBundle\Validation\OrderCheckoutValidationGroupResolver;

class OrderCheckoutType extends AbstractType
{
    private $promoCodeToNumberTransformer;

    private $orderCheckoutValidationGroupResolver;

    public function __construct(PromoCodeToNumberTransformer $promoCodeToNumberTransformer, OrderCheckoutValidationGroupResolver $orderCheckoutValidationGroupResolver)
    {
        $this->promoCodeToNumberTransformer = $promoCodeToNumberTransformer;
        $this->orderCheckoutValidationGroupResolver = $orderCheckoutValidationGroupResolver;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
        <!-- Shipping -->
        <many-to-one field="shippingCity" target-entity="AppBundle\Entity\Address\City" nullable="true">
            <join-column name="shipping_city_id" referenced-column-name="id"/>
        </many-to-one>
		<field name="shippingFirstName" type="string" column="shipping_first_name" length="255" nullable="true"/>
		<field name="shippingLastName" type="string" column="shipping_last_name" length="255" nullable="true"/>
		<field name="shippingAddress1" type="text" column="shipping_address1" nullable="true"/>
		<field name="shippingAddress2" type="text" column="shipping_address2" nullable="true"/>
		<field name="shippingPostalCode" type="string" column="shipping_postal_code" length="10" nullable="true"/>
		<field name="shippingContactNumber" type="string" column="shipping_contact_number" length="100" nullable="true"/>
		<field name="shippingEmail" type="string" column="shipping_email" length="255" nullable="true"/>
		<field name="shippingInstructions" type="text" column="shipping_instructions" nullable="true"/>
         */
        $builder
            ->add('billingFirstName', TextType::class, array('label' => 'First Name'))
            ->add('billingLastName', TextType::class, array('label' => 'Last Name'))
            ->add('billingEmail', TextType::class, array('label' => 'E-mail Address'))
            ->add('billingAddress1', TextareaType::class, array('label' => 'Address'))
            ->add('billingAddress2', TextareaType::class, array('required' => false, 'label' => 'Landmark'))
            ->add('billingPostalCode', TextType::class, array('label' => 'Postal Code'))
            ->add('billingRegion', EntityType::class, array(
                'class' => 'AppBundle:Address\Region', 
                'empty_data' => null, 
                'placeholder' => '', 
                'label' => 'Region',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->where('r.enabled = true')
                        ->orderBy('r.name', 'ASC');
                }, 'choice_label' => function ($region) {
                return $region->getName();
            }))
            ->add('billingContactNumber', TextType::class, array('label' => 'Contact Number'))
            // ->add('billing', BillingInfoType::class, ['label' => false, 'constraints' => new \Symfony\Component\Validator\Constraints\Valid()])
            ->add('shippingFirstName', TextType::class, array('label' => 'First Name'))
            ->add('shippingLastName', TextType::class, array('label' => 'Last Name'))
            ->add('shippingEmail', TextType::class, array('label' => 'E-mail Address'))
            ->add('shippingAddress1', TextareaType::class, array('label' => 'Address'))
            ->add('shippingAddress2', TextareaType::class, array('required' => false, 'label' => 'Landmark'))
            ->add('shippingPostalCode', TextType::class, array('label' => 'Postal Code'))
            ->add('shippingRegion', EntityType::class, array(
                'class' => 'AppBundle:Address\Region', 
                'empty_data' => null, 
                'placeholder' => '', 
                'label' => 'Region',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->where('r.enabled = true')
                        ->orderBy('r.name', 'ASC');
                }, 'choice_label' => function ($region) {
                return $region->getName();
            }))
            ->add('shippingContactNumber', TextType::class, array('label' => 'Contact Number'))
            // ->add('shipping', ShippingInfoType::class, ['label' => false, 'constraints' => new \Symfony\Component\Validator\Constraints\Valid()])
            ->add('promoCode', TextType::class, ['required' => false, 'label' => 'Promo Code / E-Voucher', 'invalid_message' => 'Invalid promo code'])
            // ->add('giftWrap', EntityType::class, [
            //     'class' => 'AppBundle:Store\GiftWrap',
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('g')
            //             ->where('g.enabled = true')
            //             ->orderBy('g.position', 'ASC')
            //             ->addOrderBy('g.name', 'ASC');
            //     },
            //     'choice_label' => 'name',
            //     'empty_data' => null, 
            //     'placeholder' => ''
            // ])
            ->add('paymentMethod', ChoiceType::class, [
                // 'data' => Order::PAYMENT_METHOD_PAYNAMICS, 
                'expanded' => true, 
                'choices' => array_flip(Order::getPaymentMethodOptions()),
                // 'choice_attr' => function($choice, $key, $value) {
                //     if ($value != Order::PAYMENT_METHOD_PAYNAMICS) {
                //         return ['disabled' => true];
                //     }
                //     return [];
                // },
            ])
            // ->add('giftWrapDedication', TextareaType::class, ['label' => 'Delivery Instructions or Comments'])
            ->add('shippingInstructions', TextareaType::class, ['required' => false])
            ->add('shippingSameAsBilling', CheckboxType::class, ['label' => 'My shipping information is the same as my billing information.'])
            ->add('lineItems', CollectionType::class, [
                'entry_type' => OrderCheckoutLineItemType::class,
                'label' => false,
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
                'constraints' => new \Symfony\Component\Validator\Constraints\Valid()
            ])
        ;

        $builder->get('promoCode')->addModelTransformer($this->promoCodeToNumberTransformer);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'validation_groups' => $this->orderCheckoutValidationGroupResolver
        ]);
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->addElements($form, $data);
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $this->addElements($form, $data);
    }

    protected function addElements(FormInterface $form, $data) 
    {
        // Build city
        $region = null;
        if (is_array($data) && isset($data['billingRegion'])) {
            $region = $data['billingRegion'];
        } elseif ($data instanceof Order) {
            $region = $data->getBillingRegion(); 
        }

        if (null === $region) {
            $form->add('billingCity', null, array('label' => 'City', 'empty_data' => null, 'required' => true, 'placeholder' => '', 'choices' => array()));
        } else {
            $form->add('billingCity', null, array('label' => 'City', 'empty_data' => null, 'required' => true, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) use ($region) {
                return $er->createQueryBuilder('c')
                    ->where('c.enabled = true')
                    ->andWhere('c.region = :region')
                    ->orderBy('c.name', 'ASC')
                    ->setParameter('region', $region);
            }));
        }

        $region = null;
        if (is_array($data) && isset($data['shippingRegion'])) {
            $region = $data['shippingRegion'];
        } elseif ($data instanceof Order) {
            $region = $data->getShippingRegion(); 
        }

        if (null === $region) {
            $form->add('shippingCity', null, array('label' => 'City', 'empty_data' => null, 'required' => true, 'placeholder' => '', 'choices' => array()));
        } else {
            $form->add('shippingCity', null, array('label' => 'City', 'empty_data' => null, 'required' => true, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) use ($region) {
                return $er->createQueryBuilder('c')
                    ->where('c.enabled = true')
                    ->andWhere('c.region = :region')
                    ->orderBy('c.name', 'ASC')
                    ->setParameter('region', $region);
            }));
        }
    }
}