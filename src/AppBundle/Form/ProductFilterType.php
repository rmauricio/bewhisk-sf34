<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Model\ProductFilter;

class ProductFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'sort', ChoiceType::class, array( 'choices' => array_flip( ProductFilter::getSortOptions() ), 'placeholder' => '' ) )
            // ->add( 'brand', EntityType::class, array(
            //    'class' => 'ApplicationSonataClassificationBundle:Category',
            //    'query_builder' => function (EntityRepository $er) {
            //        return $er->createQueryBuilder('c')
            //            ->where("c.context = 'product_brand'")
            //            ->andWhere("c.enabled = true")
            //            ->orderBy('c.position', 'ASC')
            //            ->addOrderBy('c.name', 'ASC');
            //     }, 
            //     'empty_data' => null,
            //     'placeholder' => '',
            // ) )
            ->add( 'price', ChoiceType::class, array( 'choices' => array_flip( ProductFilter::getPriceOptions() ), 'placeholder' => '' ) )
            ->add( 'color', EntityType::class, array(
                'class' => 'AppBundle:Store\Color',
                'multiple' => true,
                'expanded' => false,
                'attr' => array(
                    'multiple' => true,
                ),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where("c.parent IS NULL")
                        ->andWhere("c.enabled = true")
                        ->orderBy('c.position', 'ASC')
                        ->addOrderBy('c.name', 'ASC');
                },
            ) )
            ->add( 'size', EntityType::class, array(
                'class' => 'AppBundle:Store\Size',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where("s.enabled = true")
                        ->orderBy('s.position', 'ASC')
                        ->addOrderBy('s.name', 'ASC');
                },
                'empty_data' => null,
                'placeholder' => '',
            ) )
            // ->add( 'theme', EntityType::class, array(
            //     'class' => 'ApplicationSonataClassificationBundle:Category',
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('c')
            //             ->where("c.context = 'product_theme'")
            //             ->andWhere("c.enabled = true")
            //             ->orderBy('c.position', 'ASC')
            //             ->addOrderBy('c.name', 'ASC');
            //     },
            //     'empty_data' => null,
            //     'placeholder' => '',
            // ) )
            ->add( 'sortField', HiddenType::class, array() )
            ->add( 'sortOrder', HiddenType::class, array() )
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                // $form = $event->getForm();
                $data = $event->getData();
                $sort = $data->getSort();
                // dump($data);
                $sortBy = null;
                $order = null;
                if ( $sort ) {
                    list($sortBy, $order) = explode('__', $sort);   
                }
                $data->setSortField($sortBy);
                $data->setSortOrder($order);
                $event->setData($data);
            }
        );

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                // $form = $event->getForm();
                $data = $event->getData();
                if ( $data['sort'] ) {
                    list($sortBy, $order) = explode('__', $data['sort']);
                    $data['sortField'] = $sortBy;
                    $data['sortOrder'] = $order;
                } else {
                    $data['sortField'] = null;
                    $data['sortOrder'] = null;
                }
                $event->setData($data);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductFilter::class,
        ]);
    }
}