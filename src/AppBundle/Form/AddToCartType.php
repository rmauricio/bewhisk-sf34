<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Model\AddToCart;
use AppBundle\Entity\Store\OrderLineItem;
use AppBundle\DataTransformer\ProductToNumberTransformer;
use AppBundle\DataTransformer\ColorToNumberTransformer;
use AppBundle\DataTransformer\SizeToNumberTransformer;

class AddToCartType extends AbstractType
{
    private $productToNumberTransformer;

    private $colorToNumberTransformer;

    private $sizeToNumberTransformer;

    public function __construct(ProductToNumberTransformer $productToNumberTransformer, ColorToNumberTransformer $colorToNumberTransformer, SizeToNumberTransformer $sizeToNumberTransformer)
    {
        $this->productToNumberTransformer = $productToNumberTransformer;
        $this->colorToNumberTransformer = $colorToNumberTransformer;
        $this->sizeToNumberTransformer = $sizeToNumberTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', HiddenType::class, array(
                'invalid_message' => 'You have selected an invalid product.',
            ))
            // ->add('quantity', HiddenType::class, array())
        ;

        $builder->get('product')->addModelTransformer($this->productToNumberTransformer);

        if ($options['simple'] === false) {
            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                
                $parent = $data->getParentProduct();
                // if ($parent->getParentProduct()) {
                //     $parent = $parent->getParentProduct();
                // }

                if ($parent && count($parent->getType()->getProductAttributes())) {
                    foreach ($parent->getType()->getProductAttributes() as $productAttribute) {
                        /*if (!$productAttribute->getEnabled()) {
                            continue;
                        }*/
                        if ($productAttribute->getCode() == 'color') {
                            $colorIds = array();
                            $colorVariations = $parent->getEnabledColorVariations();
                            if (!empty($colorVariations)) {
                                foreach ($colorVariations as $colorVariation) {
                                    $colorIds[] = $colorVariation->getColor()->getId();
                                }
                                if (!empty($colorIds)) {
                                    $form->add($productAttribute->getCode(), EntityType::class, array(
                                        'class' => 'AppBundle:Store\Color',
                                        'query_builder' => function (EntityRepository $er) use ($colorIds) {
                                            return $er->createQueryBuilder('c')
                                                ->andWhere('c.id IN (' . implode(', ', $colorIds) . ')')
                                                ->orderBy('c.position', 'ASC')
                                                ->addOrderBy('c.name', 'ASC');
                                        },
                                        'choice_label' => 'name',
                                    ));
                                }
                            }
                        } elseif ($productAttribute->getCode() == 'size') {
                            $sizeIds = array();
                            $sizeVariations = $parent->getEnabledSizeVariations();
                            if (!empty($sizeVariations)) {
                                foreach ($sizeVariations as $sizeVariation) {
                                    $sizeIds[] = $sizeVariation->getSize()->getId();
                                }
                                $form->add($productAttribute->getCode(), EntityType::class, array(
                                    'class' => 'AppBundle:Store\Size',
                                    'query_builder' => function (EntityRepository $er) use ($sizeIds) {
                                        return $er->createQueryBuilder('s')
                                            ->andWhere('s.id IN (' . implode(', ', $sizeIds) . ')')
                                            ->orderBy('s.position', 'ASC')
                                            ->addOrderBy('s.name', 'ASC');
                                    },
                                    'choice_label' => 'name',
                                ));
                            } 
                        }
                    }
                }
            });

            $builder
                //->add('color', ChoiceType::class, array(), array('placeholder' => ''))
                //->add('size', ChoiceType::class, array(), array('placeholder' => ''))
                ->add('quantity', IntegerType::class, array())
            ;
        } else {

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
            
                $parent = $data->getParentProduct();
                // if ($parent->getParentProduct()) {
                //     $parent = $parent->getParentProduct();
                // }

                if ($parent && count($parent->getType()->getProductAttributes())) {
                    foreach ($parent->getType()->getProductAttributes() as $productAttribute) {
                        /*if (!$productAttribute->getEnabled()) {
                            continue;
                        }*/
                        if ($productAttribute->getCode() == 'color') {
                            $colorIds = array();
                            $colorVariations = $parent->getEnabledColorVariations();
                            if (!empty($colorVariations)) {
                                foreach ($colorVariations as $colorVariation) {
                                    $colorIds[] = $colorVariation->getColor()->getId();
                                }
                                if (!empty($colorIds)) {
                                    $form->add($productAttribute->getCode(), HiddenType::class, array(
                                        'data' => $data->getColor()->getId(),
                                        'invalid_message' => 'You have selected an invalid color.'
                                    ));
                                }
                            }
                        } elseif ($productAttribute->getCode() == 'size') {
                            $sizeIds = array();
                            $sizeVariations = $parent->getEnabledSizeVariations();
                            if (!empty($sizeVariations)) {
                                foreach ($sizeVariations as $sizeVariation) {
                                    $sizeIds[] = $sizeVariation->getSize()->getId();
                                }
                                $form->add($productAttribute->getCode(), HiddenType::class, array(
                                    'data' => $data->getSize()->getId(),
                                    'invalid_message' => 'You have selected an invalid size.',
                                ));
                            } 
                        }
                    }
                }
            });

            if ($builder->has('color')) {
                $builder->get('color')->addModelTransformer($this->colorToNumberTransformer);
            }
            if ($builder->has('size')) {
                $builder->get('size')->addModelTransformer($this->sizeToNumberTransformer);
            }
            
            $builder
                //->add('color', ChoiceType::class, array(), array('placeholder' => ''))
                //->add('size', ChoiceType::class, array(), array('placeholder' => ''))
                ->add('quantity', HiddenType::class, array())
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderLineItem::class,
            'simple' => false,
            'validation_groups' => array('AppStoreAddToCart')
        ]);
    }
}