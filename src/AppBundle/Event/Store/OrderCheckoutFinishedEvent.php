<?php

namespace AppBundle\Event\Store;

use AppBundle\Entity\Store\Order;
use Symfony\Component\EventDispatcher\Event;

class OrderCheckoutFinishedEvent extends Event
{
    const NAME = 'store_order.checkout_finished';

    protected $order;

    protected $exception;

    protected $response;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setException($exception)
    {
        $this->exception = $exception;
    }

    public function getException()
    {
        return $this->exception;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}