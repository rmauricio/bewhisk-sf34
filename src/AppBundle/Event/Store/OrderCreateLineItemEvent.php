<?php

namespace AppBundle\Event\Store;

use AppBundle\Entity\Store\OrderLineItem;
use Symfony\Component\EventDispatcher\Event;

class OrderCreateLineItemEvent extends Event
{
    const NAME = 'store_order.create_line_item';

    protected $orderLineItem;

    protected $user;

    protected $exception;

    protected $response;

    public function __construct(OrderLineItem $orderLineItem, $user)
    {
        $this->orderLineItem = $orderLineItem;
        $this->user = $user;
    }

    public function getOrderLineItem()
    {
        return $this->orderLineItem;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setException($exception)
    {
        $this->exception = $exception;
    }

    public function getException()
    {
        return $this->exception;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}