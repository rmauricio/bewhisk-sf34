<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder; 
use Symfony\Component\DependencyInjection\Reference;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
	{
        $definition = $container->getDefinition('sonata.formatter.form.type.selector');
        $definition->setClass('Application\Sonata\FormatterBundle\Form\Type\FormatterType');

        $definition = $container->getDefinition('sonata.classification.manager.category');
        $definition->setClass('Application\Sonata\ClassificationBundle\Entity\CategoryManager');

        // if ($container->hasDefinition('fos_user.security.controller')) {
        //     $definition = $container->getDefinition('fos_user.security.controller');
        //     $definition->setClass('AppBundle\Controller\User\SecurityController');
        // }

        // if ($container->hasDefinition('fos_user.registration.controller')) {
        //     $definition = $container->getDefinition('fos_user.registration.controller');
        //     $definition->setClass('AppBundle\Controller\User\RegistrationController');
        // }

        if ($container->hasDefinition('hwi_oauth.user.provider.fosub_bridge')) {
            $definition = $container->getDefinition('hwi_oauth.user.provider.fosub_bridge');
            $definition->setClass('AppBundle\Security\Core\User\FOSUBUserProvider');
            $definition->addMethodCall('setTokenGenerator', [$container->getDefinition('fos_user.util.token_generator.default')]);
            $definition->addMethodCall('setValidator', [$container->getDefinition('validator')]);
        }

        // $definition = $container->getDefinition('sonata.page.transformer');
        // $definition->setClass('Application\Sonata\PageBundle\Entity\Transformer');

        if ($container->hasDefinition('sonata.article.admin.article')) {
            $definition = $container->getDefinition('sonata.article.admin.article');
            // Fix missing child admin on admin getChild method call
            $definition->addMethodCall('addChild', [$container->getDefinition('sonata.article.admin.fragment')]);
        }

        // Move this to SonataUserBundle extension
        if ($container->hasDefinition('sonata.user.admin.user')) {
            $definition = $container->getDefinition('sonata.user.admin.user');
            $definition->addMethodCall('setValidationGroupResolver', [$container->getDefinition('sonata.user.validation_group_resolver.user')]);
        }

        $this->processProductAttributeProviders($container);
    }

    public function processProductAttributeProviders(ContainerBuilder $container)
    {
        // @todo: Move this on a separate class
        $serviceIds = $container->findTaggedServiceIds('app.product_attribute.provider');
        $services = array();

        foreach ($serviceIds as $id => $attributes) {
            if (!isset($attributes[0]) || !isset($attributes[0]['key'])) {
                throw new \RuntimeException('You need to specify the `key` argument to your tag.');
            }

            $services[$attributes[0]['key']] = new Reference($id);

            if ($container->hasDefinition('app.entity_manager.store.product_attribute')) {
                $productAttributeProviderDef = $container->getDefinition($id);
                $productAttributeProviderDef->addMethodCall('setProductAttributesManager', array( $container->getDefinition('app.entity_manager.store.product_attribute') ));
            }

            if ($container->hasDefinition('doctrine')) {
                $productAttributeProviderDef = $container->getDefinition($id);
                $productAttributeProviderDef->addMethodCall('setDoctrine', array( $container->getDefinition('doctrine') ));
            }
        }

        if ($container->hasDefinition('app.store.admin.product_variation')) {
            $productVariationAdminDef = $container->getDefinition('app.store.admin.product_variation');
            $productVariationAdminDef->addMethodCall('setProductAttrbiteProviders', array( $services ));
        }
        
        if ($container->hasDefinition('jms_serializer.serializer')) {
            $productVariationAdminDef->addMethodCall('setSerializer', array( $container->getDefinition('jms_serializer.serializer') ));
        }
    }
}
