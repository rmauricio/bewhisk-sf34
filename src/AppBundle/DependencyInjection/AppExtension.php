<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class AppExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @param array            $config    An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     *
     * @api
     */
    public function load(array $config, ContainerBuilder $container)
    {
		$configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $config);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('admin.xml');
        $loader->load('orm.xml');
        $loader->load('providers.xml');
        $loader->load('event_listener.xml');
		$loader->load('services.xml');
		$loader->load('security.xml');
		// $loader->load('registration.xml');
		// $loader->load('block.xml');
        //dump($config);die();
        $this->configureMenu($container, $config);
        $this->configureStoreSettingsParameters($container, $config);
    }
    
    /**
     * @param ContainerBuilder $container
     * @param array            $config
     */
    public function configureMenu(ContainerBuilder $container, array $config)
    {
        $container->getDefinition('app.menu_builder')->replaceArgument(6, $config['menu']);
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $config
     */
    public function configureStoreSettingsParameters(ContainerBuilder $container, array $config)
    {
		if (!empty($config['store']['settings'])) {
            $container->setParameter('app_store_settings', $config['store']['settings']);
            foreach ($config['store']['settings'] as $key => $setting) {
				$container->setParameter('app_store_settings.' . $key, $setting);
			}
		}
	}
}