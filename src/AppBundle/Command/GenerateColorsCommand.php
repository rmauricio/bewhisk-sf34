<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
# use Sonata\ClassificationBundle\Entity\CategoryManager;
use Application\Sonata\ClassificationBundle\Entity\CategoryManager;
use AppBundle\EntityManager\Store\ColorManager;
use Cocur\Slugify\Slugify;

class GenerateColorsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:generate-colors';
    
    private $contextManager;

    private $categoryManager;

    private $colorManager;

    public function __construct($contextManager, CategoryManager $categoryManager, ColorManager $colorManager)
    {
        $this->contextManager = $contextManager;
        $this->categoryManager = $categoryManager;
        $this->colorManager = $colorManager;

        parent::__construct();
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ...
        $output->writeln('Starting...');

        $dataFileRef = './web/colors.txt';
        
        $file = fopen($dataFileRef, "r");

        $fileContents = fgets($file);

        $colorGroups = array();
        $colors = array();

        while (!feof($file)) {
            $colorRow = fgets($file);
            $colorRowArr = explode('-', $colorRow);

            $colorGroupName = trim($colorRowArr[1]);
            $colorGroups[$colorGroupName] = $colorGroupName;

            $colorName = trim($colorRowArr[0]);

            if (!isset($colors[$colorName])) {
                $colors[$colorName] = array(
                    'name' => $colorName,
                    'hexCode' => trim($colorRowArr[2]),
                );
            }

            if (empty($colors[$colorName]['groups'])) {
                $colors[$colorName]['groups'] = array();
            }

            $colors[$colorName]['groups'][] = trim($colorRowArr[1]);
        }

        $colorContext = $this->contextManager->findOneBy(array('id' => 'color'));
        $colorParent = $this->categoryManager->findOneBy(array('id' => 26));

        foreach ($colorGroups as $key => $colorGroup) {
            $slugify = new Slugify();
            $categorySlug = $slugify->slugify($colorGroup);

            /*$colorGroupCategory = $this->categoryManager->create();
            $colorGroupCategory->setName($colorGroup);
            $colorGroupCategory->setContext($colorContext);
            $colorGroupCategory->setParent($colorParent);
            $colorGroupCategory->setSlug($categorySlug);
            $colorGroupCategory->setEnabled(true);*/

            $colorGroups[$key] = $this->categoryManager->findOneBy(array('slug' => $categorySlug));
        }

        foreach ($colors as $key => $color) {
            $colorObject = $this->colorManager->findOneBy(array('name' => $color['name']));

            $categories = array();
            foreach ($color['groups'] as $group) {
                $categories[] = $colorGroups[$group];
            }

            //$colorObject->setName($color['name']);
            //$colorObject->setHexCode($color['hexCode']);
            $colorObject->setCategories($categories);
            //$colorObject->setEnabled(true);
            $this->colorManager->save($colorObject);
        }
        
        dump($colorGroups);
        dump($colors);

        fclose($file);

        /*$output->writeln([
            'Creating Color',
            '============',
            'Dobe',
        ]);*/

        $output->writeln('Done!');
    }
}