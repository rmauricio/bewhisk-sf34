<?php

namespace AppBundle\Block\Store;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\Pool;
use Sonata\FormatterBundle\Form\Type\FormatterType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use AppBundle\Form\AddToCartType;

/**
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class ProductsBlockService extends AbstractAdminBlockService 
{
    /**
     * @var ManagerInterface
     */
    protected $productsManager;

    protected $requestStack;

    protected $session;

    protected $authorizationChecker;

    protected $tokenStorage;

    protected $formFactory;

    /**
     * @var ManagerInterface
     */
    protected $categoryManager;

    protected $categoryAdmin;

    protected $orderManager;

    protected $orderLineItemManager;

    protected $blockUniqueID;

    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ManagerInterface   $mediaManager
     */
    public function __construct($name, EngineInterface $templating, $requestStack, $session, AuthorizationCheckerInterface $authorizationChecker, $tokenStorage, $formFactory, ManagerInterface $productsManager, ManagerInterface $categoryManager, $categoryAdmin, ManagerInterface $orderManager, ManagerInterface $orderLineItemManager)
    {
        parent::__construct($name, $templating);

        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->productsManager = $productsManager;
        $this->categoryManager = $categoryManager;
        $this->categoryAdmin = $categoryAdmin;
        $this->orderManager = $orderManager;
        $this->orderLineItemManager = $orderLineItemManager;
    }

	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
    	$settings = $blockContext->getSettings();

        $templateLayouts = self::getLayoutTemplates();
        $templateLayout = $templateLayouts[$settings['layout']];

        $offset = null;
        if (!empty($settings['offset'])) {
            $offset = $settings['offset'];
        }

        $limit = null;
        if (!empty($settings['limit'])) {
            $limit = $settings['limit'];
        }

        $categories = array();
        if (!empty($settings['categories'])) {
            $categories = $settings['categories'];
        }

        $excludedIds = array();
        if (!empty($settings['excludedIds'])) {
            $excludedIds = $settings['excludedIds'];
            if (!is_array($excludedIds)) {
                $excludedIds = explode(',', $excludedIds);
            }
        }

        $sortBy = null;
        if (!empty($settings['sortBy'])) {
            if ($settings['sortBy'] == 'featured') {
                $sortBy = array(
                    'p.featured' => 'DESC',
                );
            } elseif ($settings['sortBy'] == 'new_arrivals') {
                $sortBy = array(
                    'p.publicationStartDate' => 'DESC',
                );
            }
        }

        $user = null;
        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->tokenStorage->getToken()->getUser();
        }

        $activeOrder = $this->orderManager->getUserActiveOrder(array('user' => $user));
        $activeOrderLineItems = array();
        if ($activeOrder) {
            $activeOrderLineItems = $activeOrder->getProductIndexedLineItems();
        }

        $stocks = array();
        $products = $this->productsManager->findProductBy(array('categories' => $categories), $sortBy, $excludedIds, $limit, $offset, $stocks, $activeOrder);

        $addToCartForms = array();

        foreach ($products as $product) {
            $productParent = $product;
            $variations = $product->getEnabledVariations();
            if ($product->getParent()) {
                $productParent = $product->getParent();
                $variations = $productParent->getEnabledVariations(); 
            }

            $lineItem1 = $this->orderLineItemManager->create();
            $lineItem1->setProduct($product);
            $lineItem1->setParentProduct($product->getParent());
            $lineItem1->setColor($product->getColor());
            $lineItem1->setSize($product->getSize());
            
            $itemProductId = $product->getId();
            if (isset($activeOrderLineItems[$itemProductId])) {
                $quantity = $activeOrderLineItems[$itemProductId]->getQuantity();
                $lineItem1->setQuantity($quantity);
            }

            $addToCartForms[$product->getId()] = $this->formFactory->create(AddToCartType::class, $lineItem1, array(
                'method' => 'POST',
                'attr' => array('novalidate' => true),
                'simple' => true,
            ))->createView();

            foreach ($variations as $variation) {
                $lineItem2 = $this->orderLineItemManager->create();
                $lineItem2->setProduct($variation);
                $lineItem2->setParentProduct($variation->getParent());
                $lineItem2->setColor($variation->getColor());
                $lineItem2->setSize($variation->getSize());

                $itemProductId = $variation->getId();
                if (isset($activeOrderLineItems[$itemProductId])) {
                    $quantity = $activeOrderLineItems[$itemProductId]->getQuantity();
                    $lineItem2->setQuantity($quantity);
                }

                $addToCartForms[$variation->getId()] = $this->formFactory->create(AddToCartType::class, $lineItem2, array(
                    'method' => 'POST',
                    'attr' => array('novalidate' => true),
                    'simple' => true,
                ))->createView();
            }

            $addToCartForms[$product->getId()] = $this->formFactory->create(AddToCartType::class, $lineItem1, array(
                'method' => 'POST',
                'attr' => array('novalidate' => true),
                'simple' => true,
            ))->createView();
        }

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
            'products' => $products,
            'stocks' => $stocks,
            'addToCartForms' => $addToCartForms,
            'templateLayout' => $templateLayout,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $immutableArrayType = 'Sonata\CoreBundle\Form\Type\ImmutableArrayType';
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $integerType = 'Symfony\Component\Form\Extension\Core\Type\IntegerType';
            $choiceType = 'Symfony\Component\Form\Extension\Core\Type\ChoiceType';
            $checkboxType = 'Symfony\Component\Form\Extension\Core\Type\CheckboxType';
        } else {
            $immutableArrayType = 'sonata_type_immutable_array';
            $textType = 'text';
            $integerType = 'integer';
            $checkboxType = 'checkbox';
            $choiceType = 'choice';
        }

        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                // array(
                //     'title',
                //     $textType,
                //     array(
                //         'required' => false,
                //         'label' => 'Title',
                //     )
                // ),
                array($this->getCategoryBuilder('categories', $formMapper, array('label' => 'Categories', 'required' => false), true), null, array()),
                array(
                    'limit',
                    $integerType,
                    array(
                        'required' => false,
                        'label' => 'Limit',
                    )
                ),
                array(
                    'offset',
                    $integerType,
                    array(
                        'required' => false,
                        'label' => 'Offset',
                    )
                ),
                array(
                    'enablePager',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Enable Pager',
                    )
                ),
                array(
                    'sortBy', $choiceType, array(
                        'required' => false,
                        'placeholder' => '',
                        'empty_data' => null,
	                    'choices' => array_flip(self::getSortByOptions()),
	                    'label' => 'Sort By',
	                )
                ),
                array(
                    'layout', $choiceType, array(
	                    'required' => true,
	                    'choices' => array_flip(self::getLayoutOptions()),
	                    'label' => 'Layout',
	                )
                ),
                array(
                    'class',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Class',
                    )
                ),
                array(
                    'headerTitle',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title',
                    )
                ),
                array(
                    'headerTitleWrapperClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title Wrapper Class',
                    )
                ),
                array(
                    'headerTitleClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title Class',
                    )
                ),
                array(
                    'showNoResultsMessage',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show No Results Message',
                    )
                ),
                array(
                    'excludedIds',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Excluded ID(s)',
                    )
                ),
            ),
        ));

        $categoryViewCallbackTransformer = $this->categoryViewCallbackTransformer();
        $formMapper->get('settings')->get('categories')->addViewTransformer($categoryViewCallbackTransformer);

        if (($blockUniqueID = $formMapper->getAdmin()->getUniqid())) {
            $this->blockUniqueId = $blockUniqueID;
        }

        parent::buildEditForm($formMapper, $block);
    }

    /**
     * @return CallbackTransformer
     */
    protected function categoryViewCallbackTransformer()
    {
        return new CallbackTransformer(
            function ($categories) {
                return $categories;
            },
            function ($categories) {
                if (is_array($categories)) {
                    $categories = array_filter($categories, function($val){
                        if (empty($val)) {
                            return false;
                        }
                        return true;
                    });
                    return $categories;
                }
                return $categories;
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'layout' => 'default',
            'categories' => null,
            'limit' => 4,
            'offset' => 0,
            'enablePager' => false,
            'headerTitle' => null,
            'headerTitleWrapperClass' => null,
            'headerTitleClass' => null,
            'class' => null,
            'showNoResultsMessage' => false,
            'excludedIds' => null,
            'sortBy' => null,
            'template' => 'AppBundle:Block:Store/products.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function load(BlockInterface $block)
    {
        $this->initCategories($block, 'categories');
    }

    /**
     * Generic collections initialization
     * 
     * @param BlockInterface $block
     * @param string $fieldNames
     */
    protected function initCategories(BlockInterface $block, $fieldName = '')
    {
        $categoryArray = $block->getSetting($fieldName, array());
        
        $categoryArrayCollection = new ArrayCollection();
        
        if (!empty($categoryArray)) {
            if ($categoryArray && count($categoryArray)) {
                foreach($categoryArray as $categoryId) {
                    if (($category = $this->categoryManager->findOneBy(array('id' => $categoryId)))) {
                        $categoryArrayCollection->add($category);
                    }
                }
            }
        }
        $block->setSetting($fieldName, $categoryArrayCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(BlockInterface $block)
    {
        if (($categories = $block->getSetting('categories')) && ($categories instanceof ArrayCollection && $categories->count() > 0)) {
            $block->setSetting('categories', $this->getPostedCategories('categories'));
        } else {
            $block->setSetting('categories', null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(BlockInterface $block)
    {
        if (($categories = $block->getSetting('categories')) && ($categories instanceof ArrayCollection && $categories->count() > 0)) {
            $block->setSetting('categories', $this->getPostedCategories('categories'));
        } else {
            $block->setSetting('categories', null);
        }
    }

    /**
     * Get Posted collections in order to preserved the collection order
     * 
     * @param type $fieldName
     * @return array
     */
    protected function getPostedCategories($fieldName)
    {
        if (empty($this->blockUniqueId)) {
            return null;
        }
        
        if (($requestStack = $this->requestStack)) {
            $currentRequest = $requestStack->getCurrentRequest();
            if (($blockInfo = $currentRequest->request->get($this->blockUniqueId))) {
                if (isset($blockInfo['settings']) && isset($blockInfo['settings'][$fieldName])) {
                    // just to make sure values are returned as integer
                    return array_map(function($value){
                        return (int) $value;
                    }, $blockInfo['settings'][$fieldName]);
                }
            }
        }
        return null;
    }

    public function getBlockMetadata($code = null): Metadata
    {
        return new Metadata(
            $this->getName(),
            null !== $code ? $code : $this->getName(),
            false,
            'AppBundle',
            ['class' => 'fa fa-shopping-cart']
        );
    }
    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getCategoryBuilder($fieldName, FormMapper $formMapper, $options = array(), $multiple = false, $label = null, $help = null)
    {
        // simulate an association ...
        $fieldDescription = $this->categoryAdmin->getModelManager()->getNewFieldDescriptionInstance($this->categoryAdmin->getClass(), $fieldName, array());
        $fieldDescription->setAssociationAdmin($this->categoryAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        if ($multiple) {
            $fieldDescription->setOption('edit', 'standard');
            $fieldDescription->setAssociationMapping(array(
                'fieldName' => $fieldName,
                'type'      => ClassMetadataInfo::MANY_TO_MANY
            ));

            $entityManager = $this->categoryAdmin->getModelManager()->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');

            $query = $entityManager->createQueryBuilder('c')
                        ->select('c')
                        ->from('ApplicationSonataClassificationBundle:Category', 'c')
                        ->where("c.context = 'product'")
                        ->orderBy('c.position', 'ASC')
                        ->addOrderBy('c.name', 'ASC');
            
            return $formMapper->create($fieldName, 'sonata_type_model', array(
                'sonata_field_description' => $fieldDescription,
                'class'                    => $this->categoryAdmin->getClass(),
                'model_manager'            => $this->categoryAdmin->getModelManager(),
                'label'                    => $label,
                'sonata_help'              => $help,
                'property'                 => 'name',
                'multiple'                 => true,
                'required'                 => false,
                'expanded'                 => false,
                'sortable'                 => true,
                'btn_add'                  => false,
                'query'                    => $query,
                'attr'                     => array('data-sonata-select2' => false)
            ));
        } else {
            $fieldDescription->setOption('edit', 'list');
            $fieldDescription->setAssociationMapping(array(
                'fieldName' => 'category',
                'type' => ClassMetadataInfo::MANY_TO_ONE,
            ));
            // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
            $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
                ? 'Sonata\AdminBundle\Form\Type\ModelListType'
                : 'sonata_type_model_list';

            $fieldDescription->setOption('link_parameters', array('context' => 'promo', 'hide_context' => true));

            $fieldOptions = array_merge(
                array(
                    'sonata_field_description' => $fieldDescription,
                    'class' => $this->categoryAdmin->getClass(),
                    'model_manager' => $this->categoryAdmin->getModelManager()
                )
            , $options);

            return $formMapper->create($fieldName, $modelListType, $fieldOptions);
        }
    }

    public static function getLayoutOptions()
    {
        return array(
            'default' => 'Default',
            // 'layout1' => 'Layout 1',
            // 'layout2' => 'Layout 2',
        );
    }

    public static function getSortByOptions()
    {
        return array(
            'featured' => 'Featured',
            'new_arrivals' => 'New Arrivals',
            // 'layout1' => 'Layout 1',
            // 'layout2' => 'Layout 2',
        );
    }

    public static function getLayoutTemplates()
    {
    	return array(
    		'default' => 'AppBundle:Block:Store/products_layouts/default.html.twig',
    		// 'layout1' => 'AppBundle:Block:Product/products_layouts/layout1.html.twig',
    		// 'layout2' => 'AppBundle:Block:Product/products_layouts/layout2.html.twig',
    	);
    }
}