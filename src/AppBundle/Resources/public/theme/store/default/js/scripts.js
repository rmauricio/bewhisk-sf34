(function ($) {

	$(document).ready(function(){

		// scroll to top
		$('.scrolltop').click(function(e){
			e.preventDefault();
			$('html, body').stop().animate({ 'scrollTop': 0 }, 500, "swing", function () { });
		});

		// icheck
		$('input[type="radio"]:not(.icheck-exclude, .icheck-exclude input),input[type="checkbox"]:not(.icheck-exclude, .icheck-exclude input)').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });

        // select2
        $('select:not(.select2-exclude select, .select2-exclude)').select2();

        // file upload
        $('.file-upload').each(function(i, o){
        	var upload_btn = $(o).find('.input-group-addon .fa-upload');
        	upload_btn.click(function(){
	            $(this).closest('.file-upload').find('input').trigger('click.bs.fileinput');
	        });
		});

		// navbar dropdown hover and activate click event on parent item

        $('.navbar .dropdown > a').click(function(){
            location.href = this.href;
        });

	});

	$.fn.handleFormMultipleSubmission = function() {
		if ($(this).hasClass('processing')) {
			return false;
		}
		$.fn.showLoader();
		$(this).addClass('processing');
	}

	$.fn.showLoader = function() {
		$.blockUI({ 
			// message: '<div class="messages messages--info messages--loader"><ul class="messages__list"><li class="messages__item"><small>Please wait while we are processing<br />your request.</small></li></ul></div>', 
			message: '<div><img src="/bundles/app/theme/store/default/images/loader-spin-1s-100.svg" /><br /><small>Please wait while we are processing<br />your request.</small></div>', 
	        css: { 
				border: 'none', 
		 		backgroundColor: 'transparent',
                left: '50%',
                marginLeft: '-150px',
                width: '300px',
			},
			overlayCSS:  { 
				backgroundColor: '#ffffff', 
				opacity:         0.6, 
				cursor:          'wait'
			},
		});
	}

})(jQuery);
